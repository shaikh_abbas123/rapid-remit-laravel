<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name'      => 'Admin',
            'email'     => 'admin@admin.com',
            'password'  => Hash::make('12345678'),
        ]);
        DB::table('admins')->insert([
            'name'      => 'Ammar',
            'email'     => 'ammar@admin.com',
            'password'  => Hash::make('12345678'),
        ]);
        DB::table('users')->insert([
            'firstname' => 'Customer',
            'lastname'  => 'Test',
            'email'     => 'customer@customer.com',
            'password'  => Hash::make('12345678'),
        ]);
    }
}
