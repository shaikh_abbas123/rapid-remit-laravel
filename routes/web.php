<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"website\HomeController@index")->name('home');

Route::get('/login',"website\AuthController@loginShow")->name('login')->middleware('guest:web');
Route::post('/login',"website\AuthController@loginPost")->name('login.store');
Route::get('/logout',"website\AuthController@logout")->name('logout')->middleware('auth:web');

Route::middleware('auth:web')->group(function () {
    Route::get('home',"website\HomeController@index")->name('home');
});


Route::get('/admin/login',"Admin\AuthController@loginShow")->name('admin.login')->middleware('guest:admin');
Route::post('/admin/login',"Admin\AuthController@loginPost")->name('admin.login.store');
Route::get('/admin/logout',"Admin\AuthController@logout")->name('admin.logout')->middleware('auth:admin');

Route::get('/forgotpassword/{type}',            "ForgetPasswordResetController@viewForgotpassword")                 ->name('forgotpassword');
Route::get('/resetPassword/{email_encode}',     "ForgetPasswordResetController@ResetPassword")                      ->name('resetPassword'); // Forgot Password send email 
Route::post('/SetNewPassword',                  "ForgetPasswordResetController@SetNewPassword")                     ->name('SetNewPassword'); // Set New Password 
Route::post('/AjaxCallForForgotPasswordEmail',  "ForgetPasswordResetController@AjaxCallForForgotPasswordEmail")     ->name('AjaxCallForForgotPasswordEmail'); // Forgot Password send email 

Route::get('register',  "website\RegisterController@viewRegister")->name("register");
Route::post('register', "website\RegisterController@registerUser")->name("registerUser");

Route::middleware('auth:admin')->group(function () {
    Route::get('admin/home',"Admin\HomeController@index")->name('admin.home');
    Route::post('admin/home/store',      'Admin\HomeController@store')    ->name('home.store');



    Route::get( 'admin/service/view',       'Admin\ServiceController@index')    ->name('service.view');
    Route::get( 'admin/service/create',     'Admin\ServiceController@create')   ->name('service.create');
    Route::get( 'admin/service/edit/{id}',  'Admin\ServiceController@edit')     ->name('service.edit');
    Route::post('admin/service/store',      'Admin\ServiceController@store')    ->name('service.store');
    Route::post('admin/service/delete/{id}','Admin\ServiceController@delete')   ->name('service.delete');

    Route::get( 'admin/article/view',       'Admin\ArticleController@index')    ->name('article.view');
    Route::get( 'admin/article/create',     'Admin\ArticleController@create')   ->name('article.create');
    Route::get( 'admin/article/edit/{id}',  'Admin\ArticleController@edit')     ->name('article.edit');
    Route::post('admin/article/store',      'Admin\ArticleController@store')    ->name('article.store');
    Route::post('admin/article/delete/{id}','Admin\ArticleController@delete')   ->name('article.delete');

    Route::get( 'admin/blog/view',       'Admin\BlogController@index')    ->name('blog.view');
    Route::get( 'admin/blog/create',     'Admin\BlogController@create')   ->name('blog.create');
    Route::get( 'admin/blog/edit/{id}',  'Admin\BlogController@edit')     ->name('blog.edit');
    Route::post('admin/blog/store',      'Admin\BlogController@store')    ->name('blog.store');
    Route::post('admin/blog/delete/{id}','Admin\BlogController@delete')   ->name('blog.delete');

    Route::get( 'admin/news/view',       'Admin\NEWSController@index')    ->name('news.view');
    Route::get( 'admin/news/create',     'Admin\NEWSController@create')   ->name('news.create');
    Route::get( 'admin/news/edit/{id}',  'Admin\NEWSController@edit')     ->name('news.edit');
    Route::post('admin/news/store',      'Admin\NEWSController@store')    ->name('news.store');
    Route::post('admin/news/delete/{id}','Admin\NEWSController@delete')   ->name('news.delete');

    Route::get( 'admin/offer/view',       'Admin\OfferController@index')    ->name('offer.view');
    Route::get( 'admin/offer/create',     'Admin\OfferController@create')   ->name('offer.create');
    Route::get( 'admin/offer/edit/{id}',  'Admin\OfferController@edit')     ->name('offer.edit');
    Route::post('admin/offer/store',      'Admin\OfferController@store')    ->name('offer.store');
    Route::post('admin/offer/delete/{id}','Admin\OfferController@delete')   ->name('offer.delete');

    Route::get( 'admin/setting/create',     'Admin\SettingController@create')   ->name('setting.create');
    Route::post('admin/setting/store',      'Admin\SettingController@store')    ->name('setting.store');
    Route::get('admin/setting/delete/{id}',      'Admin\SettingController@delete')    ->name('setting.delete');


    Route::get( 'admin/faq/view',        'Admin\FaqController@index')       ->name('faq.view');
    Route::get( 'admin/faq/create',      'Admin\FaqController@create')      ->name('faq.create');
    Route::get( 'admin/faq/edit/{id}',   'Admin\FaqController@edit')        ->name('faq.edit');
    Route::post('admin/faq/store',       'Admin\FaqController@store')       ->name('faq.store');
    Route::get( 'admin/faq/delete/{id}', 'Admin\FaqController@delete')      ->name('faq.delete');

    Route::get( 'admin/policies/create',     'Admin\PolicyController@create')   ->name('policy.create');
    Route::post('admin/policies/privacyPolicy',      'Admin\PolicyController@store_privacy_policy')             ->name('privacyPolicy.store');
    Route::post('admin/policies/termsAndConditions',      'Admin\PolicyController@store_terms_and_conditions')       ->name('termsAndConditions.store');
    Route::post('admin/policies/cookiesPolicy',      'Admin\PolicyController@store_cookies_policy')             ->name('cookiesPolicy.store');

});

Route::get('/comparison',"website\HomeController@comparison")->name('website.comparison');
Route::get('/ournetwork',"website\HomeController@ournetwork")->name('website.ournetwork');
Route::get('/SpecialOffer',"website\HomeController@SpecialOffer")->name('website.SpecialOffer');
Route::get('/AboutUs',"website\HomeController@AboutUs")->name('website.AboutUs');
Route::get('/knowledgeBase',"website\HomeController@knowledgeBase")->name('website.knowledgeBase');
Route::get('/faqs',"website\HomeController@faqs")->name('website.faqs');
Route::get('/contact',"website\HomeController@contact")->name('website.contact');
Route::get('/privacypolicy',"website\HomeController@privacypolicy")->name('website.privacypolicy');
Route::get('/termsandconditions',"website\HomeController@termsandconditions")->name('website.termsandconditions');
Route::get('/cookiespolicy',"website\HomeController@cookiespolicy")->name('website.cookiespolicy');
Route::get('/News',"website\HomeController@News")->name('website.News');
Route::get('/xpressmoney',"website\HomeController@xpressmoney")->name('website.xpressmoney');
Route::get('/forexcalculator',"website\HomeController@forexcalculator")->name('website.forexcalculator');
Route::get('/Articles',"website\HomeController@Articles")->name('website.Articles');
Route::get('/blogs',"website\HomeController@blogs")->name('website.blogs');