@extends('admin.layouts.app')
@push('custom-css')
<style>
    .card-primary:not(.card-outline)>.card-header {
        background-color: #dc3545;
    }
</style>
@endpush

@section('content')
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        {{Session::get('success')}}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                            {{Session::get('error')}}
                    </div>
                @endif
                

                



                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add Blog</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('blog.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if (isset($blog->id))
                                <input type="hidden" name="id" value="{{ @$blog->id }}">
                            @endif
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title"
                                        class="form-control @error('title') is-invalid @enderror"
                                        placeholder="Enter Title" value="{{ @$blog->title }}">
                                    @error('title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Sub-Title</label>
                                    <input type="text" name="sub_title"
                                        class="form-control @error('sub_title') is-invalid @enderror"
                                        id="" placeholder="Enter Sub Title" value="{{ @$blog->sub_title }}">
                                    @error('sub_title')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea class="form-control @error('description') is-invalid @enderror"
                                        name="description" rows="3" placeholder="Enter ...">{{ @$blog->description }}</textarea>
                                    @error('description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Logo</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" name="logo"
                                                class="custom-file-input @error('logo') is-invalid @enderror"
                                                id="">
                                            <label class="custom-file-label" for="">Choose file</label>
                                        </div>
                                        
                                    </div>
                                    @error('logo')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                @if (isset($blog->logo))
                                    <div class="form-group">
                                        <img src="{{ @asset('website/images/blog')."/".@$blog->logo}}" height="150" width="auto" alt="" srcset="">
                                    </div>
                                @endif
                                
                               
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('blog.view')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection