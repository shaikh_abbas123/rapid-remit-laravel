@extends('admin.layouts.app')
@push('custom-css')
<style>
    .card-primary:not(.card-outline)>.card-header {
        background-color: #dc3545;
    }
</style>
@endpush

@section('content')
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                    {{Session::get('success')}}
                </div>
                @elseif(Session::has('error'))
                <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                    {{Session::get('error')}}
                </div>
                @endif






                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add Setting</h3>
                        </div>

                        <form action="{{route('setting.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">

                            @csrf
                            @if (isset($data->id))
                                <input type="hidden" name="id" value="{{ @$data->id }}">
                            @endif
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="text-input" class="">Email</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input id="text-input" name="email" placeholder="Email" type="email"
                                            class="form-control @error('email') is-invalid @enderror"
                                            value="{{ @$data->email }}">
                                        @error('email')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="text-input" class="">Company Name</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input id="text-input" name="company_name" placeholder="Company Name"
                                            type="text" class="form-control @error('company_name') is-invalid @enderror"
                                            value="{{ @$data->company_name }}">
                                        @error('company_name')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="text-input" class="">Office Address</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input id="text-input" name="office_address" placeholder="Office Address"
                                            type="text"
                                            class="form-control @error('office_address') is-invalid @enderror"
                                            value="{{ @$data->office_address }}">
                                        @error('office_address')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="text-input" class="">P.O.Box #</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input id="text-input" name="po_box" placeholder="P.O.Box #" type="text"
                                            class="form-control @error('po_box') is-invalid @enderror"
                                            value="{{ @$data->po_box }}">
                                        @error('po_box')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3">
                                        <label for="text-input" class="">City</label>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <input id="text-input" name="city" placeholder="City" type="text"
                                            class="form-control @error('city') is-invalid @enderror"
                                            value="{{ @$data->city }}">
                                        @error('city')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3"><label for="text-input" class="">Country</label></div>
                                    <div class="col-12 col-md-9"><input id="text-input" name="country"
                                            placeholder="Country" type="text"
                                            class="form-control @error('country') is-invalid @enderror"
                                            value="{{ @$data->country }}">
                                        @error('country')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3"><label for="text-input" class="">Map Coordinates</label></div>
                                    <div class="col-12 col-md-4"><input id="text-input" name="latitude"
                                            placeholder="Latitude" type="text"
                                            class="form-control @error('latitude') is-invalid @enderror"
                                            value="{{ @$data->latitude }}">
                                        @error('latitude')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                    <div class="col-12 col-md-4"><input id="text-input" name="longitude"
                                            placeholder="Longitude" type="text"
                                            class="form-control @error('longitude') is-invalid @enderror"
                                            value="{{ @$data->longitude }}">
                                        @error('longitude')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3"><label for="text-input" class="">Facebook Link</label></div>
                                    <div class="col-12 col-md-9"><input id="text-input" name="facebook_link"
                                            placeholder="FaceBook Link" type="text"
                                            class="form-control @error('facebook_link') is-invalid @enderror"
                                            value="{{ @$data->facebook_link }}">
                                        @error('facebook_link')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3"><label for="text-input" class="">Instagram Link</label></div>
                                    <div class="col-12 col-md-9"><input id="text-input" name="instagram_link"
                                            placeholder="Instagram Link" type="text"
                                            class="form-control @error('instagram_link') is-invalid @enderror"
                                            value="{{ @$data->instagram_link }}">
                                        @error('instagram_link')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3"><label for="text-input" class="">LinkedIn Link</label></div>
                                    <div class="col-12 col-md-9"><input id="text-input" name="linkedIn_link"
                                            placeholder="LinkedIn Link" type="text"
                                            class="form-control @error('linkedIn_link') is-invalid @enderror"
                                            value="{{ @$data->linkedIn_link }}">
                                        @error('linkedIn_link')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3"><label for="text-input" class="">Twitter Link</label></div>
                                    <div class="col-12 col-md-9"><input id="text-input" name="twitter_link"
                                            placeholder="Twitter Link" type="text"
                                            class="form-control @error('twitter_link') is-invalid @enderror"
                                            value="{{ @$data->twitter_link}}">
                                        @error('twitter_link')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3"><label for="text-input" class="">IOS URL</label></div>
                                    <div class="col-12 col-md-9"><input id="text-input" name="ios_link"
                                            placeholder="IOS URL" type="text"
                                            class="form-control @error('ios_link') is-invalid @enderror"
                                            value="{{ @$data->ios_link }}">
                                        @error('ios_link')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-3"><label for="text-input" class="">Android URL</label></div>
                                    <div class="col-12 col-md-9"><input id="text-input" name="android_link"
                                            placeholder="Android URL" type="text"
                                            class="form-control @error('android_link') is-invalid @enderror"
                                            value="{{ @$data->android_link }}">
                                        @error('android_link')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i>
                                    Submit
                                </button>
                                @if (isset($data->id))
                                <a href="{{route('setting.delete',@$data->id)}}" type="reset" class="ml-2 btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Reset
                                </a>    
                                @endif
                                
                            
                            {{-- <form action="{{route('setting.delete')}}" method="post">
                                
                            </form> --}}
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection