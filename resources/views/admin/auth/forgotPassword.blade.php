<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AdminLTE 3 | Log in</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('backend/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.min.css')}}">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href=""><b>Template</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Admin Panel Forgot Password</p>

                <form action="{{route('admin.login.store')}}" method="post">
                    @if (Session::has('message'))
                    <p class="text-danger">{{Session::get('message')}}</p>
                    @endif

                    @csrf
                    <div class="input-group mb-2">
                        <input type="email" class="form-control" value="{{old('email')}}" name="email" id="email"
                            placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    @error('email')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror
                    
                    <div class="row">
                        
                        <!-- /.col -->
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block" id="send_email">
                                <span id="send_email_text" >Send Email</span> 
                                {{-- <div class="clearfix verify-spinner-loading" style="display: block" >
                                    <div class="spinner-border float-right" role="status">
                                    <span class="sr-only">Loading...</span>
                                    </div>
                                </div> --}}
                                <div id="loaading" style="display: none">
                                    <span class="spinner-border spinner-border-sm"></span>
                                    Loading..
                                </div>
                            </button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
    {{-- <p>Mail is send</p> --}}
    
    <p id="verify_code_msg" style="display:none"></p>

    {{-- <a href="#">Forgot Password?</a> --}}
    <!-- jQuery -->
    <script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>

    <script>

        $("#send_email").click(function (e) { 
            e.preventDefault();

            let email = $("#email").val();

            $("#send_email_text").hide();
            $("#send_email").prop('disabled', true);
            $("#loaading").show();


            $.ajax({
                type: "POST",
                url: "{{route('AjaxCallForForgotPasswordEmail')}}",
                data: { _token: "{{csrf_token()}}", 
                        email: email,
                        main_type: "admin"},
                success: function (response) {
                    console.log('response',response[1]);
                    if (response[0]) 
                    {
                        $('#verify_code_msg').show();
                        $('#verify_code_msg').text(response[1]);
                        $('#verify_code_msg').addClass('text-success');
                        $('#verify_code_msg').removeClass('text-danger');

                    } 
                    else{
                        $('#verify_code_msg').show();
                        $('#verify_code_msg').text(response[1]);
                        $('#verify_code_msg').addClass('text-danger');
                        $('#verify_code_msg').removeClass('text-success');

                    }

                    $("#loaading").hide();
                    $("#send_email_text").show();
                    $("#send_email").prop('disabled', false);
                }
            });
            
        });

        
    </script>
</body>

</html>
