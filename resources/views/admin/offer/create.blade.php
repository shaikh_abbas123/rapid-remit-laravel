@extends('admin.layouts.app')
@push('custom-css')
<style>
    .card-primary:not(.card-outline)>.card-header {
        background-color: #dc3545;
    }
</style>
@endpush

@section('content')
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        {{Session::get('success')}}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                            {{Session::get('error')}}
                    </div>
                @endif
                

                



                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add Offer</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('offer.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if (isset($offer->id))
                                <input type="hidden" name="id" value="{{ @$offer->id }}">
                            @endif
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Heading</label>
                                    <input type="text" name="heading"
                                        class="form-control @error('heading') is-invalid @enderror"
                                        placeholder="Enter Heading" value="{{ @$offer->heading }}">
                                    @error('heading')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea class="form-control @error('description') is-invalid @enderror"
                                        name="description" rows="3" placeholder="Enter ...">{{ @$offer->description }}</textarea>
                                    @error('description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Expiry Date</label>
                                    <input type="date" name="expiry_date"
                                        class="form-control @error('expiry_date') is-invalid @enderror"
                                        id="" placeholder="Enter Expiry Date" value="{{ @$offer->expiry_date }}">
                                    @error('expiry_date')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Logo</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" name="logo"
                                                class="custom-file-input @error('logo') is-invalid @enderror"
                                                id="">
                                            <label class="custom-file-label" for="">Choose file</label>
                                        </div>
                                        
                                    </div>
                                    @error('logo')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                @if (isset($offer->logo))
                                    <div class="form-group">
                                        <img src="{{ @asset('website/images/offer')."/".@$offer->logo}}" height="150" width="auto" alt="" srcset="">
                                    </div>
                                @endif
                                
                               
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('offer.view')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection