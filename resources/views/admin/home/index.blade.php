@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible col-md-12 mt-2">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-check"></i> sAlert!</h5>
                {{Session::get('success')}}
            </div>
        @elseif(Session::has('error'))
            <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                    {{Session::get('error')}}
            </div>
        @endif
        <form action="{{route('home.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
          @csrf
          @if (isset($news->id))
              <input type="hidden" name="id" value="{{ @$news->id }}">
          @endif
        <div class="card">
          <div class="card-header"><strong>Home Banner</strong></div>
          <div class="card-body">
             
                <div class="row form-group">
                   <div class="col-md-3"><label for="text-input" class="">Banner Text</label></div>
                   <div class="col-12 col-md-9"><textarea name="home_banner_text" rows="5" placeholder="Banner Text" class="form-control">{{ @$data[0]->home_banner_text }}</textarea></div>
                </div>
             
          </div>
          
       </div>

       <div class="card">
         <div class="card-header"><strong>Why Choose Us</strong></div>
         <div class="card-body">
           <div class="row">
             <div class="col-md-3">
               Left Text
             </div>
             <div class="col-md-9">
                <textarea class="editor"  name="why_choose_us_left_text">
                  {{ @$data[0]->why_choose_us_left_text }}
                </textarea>  
             </div>
            </div>

            <div class="row">
              <div class="col-md-3">
                Right Text
              </div>
              <div class="col-md-9">
                 <textarea class="editor"  name="why_choose_us_right_text">
                   {{ @$data[0]->why_choose_us_right_text }}
                 </textarea>  
              </div>
            </div>

           </div>
         </div>
         <div class="card-footer"></div>
       </div>
       <div class="card-footer"><button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Submit</button></div>
      </form>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection

@push('custom-script')
<script>
 $('.editor').summernote();
</script>
@endpush