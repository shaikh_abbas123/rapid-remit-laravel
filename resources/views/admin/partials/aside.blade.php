 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
 	<!-- Brand Logo -->
 	<a href="index3.html" class="brand-link">
 		<img src="{{asset('backend/dist/img/new-rapid-remit-logo.png')}}" alt="AdminLTE Logo" class="brand-image "
 			style="opacity: .8">
 		<h3 class="brand-text font-weight-light"><strong> Rapid Remit</strong></h3>
 	</a>

 	<!-- Sidebar -->
 	<div class="sidebar">
 		<!-- Sidebar user panel (optional) -->
 		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
 			<div class="image">
 				<img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2"
 					alt="User Image">
 			</div>
 			<div class="info">
 				<a href="#" class="d-block">Admin User</a>
 			</div>
 		</div>

 		<!-- SidebarSearch Form -->
 		{{-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> --}}

 		<!-- Sidebar Menu -->
 		<nav class="mt-2">
 			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
 				data-accordion="false">
 				<!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
			   <li class="nav-item">
					<a href="{{route('admin.home')}}" class="nav-link">
						{{-- <i class="fas fa-th-large"></i> --}}
						<i class="fas fa-home"></i>
						<p>
							Home
						</p>
					</a>
				</li>
 				<li class="nav-item">
 					<a href="{{route('service.view')}}" class="nav-link">
 						{{-- <i class="fas fa-th-large"></i> --}}
 						<i class="fas fa-boxes"></i>
 						<p>
 							Services
 						</p>
 					</a>
 				</li>
 				<li class="nav-item">
 					<a href="{{route('article.view')}}" class="nav-link">
 						<i class="fas fa-newspaper"></i>
 						<p>
 							Article
 						</p>
 					</a>
 				</li>
 				<li class="nav-item">
 					<a href="{{route('blog.view')}}" class="nav-link">
 						<i class="fab fa-blogger-b"></i>
 						<p>
 							Blogs
 						</p>
 					</a>
 				</li>
 				<li class="nav-item">
 					<a href="{{route('news.view')}}" class="nav-link">
 						<i class="far fa-newspaper"></i>
 						<p>
 							News
 						</p>
 					</a>
 				</li>

 				<li class="nav-item">
 					<a href="{{route('offer.view')}}" class="nav-link">
 						<i class="fas fa-shopping-bag"></i>
 						<p>Offer
 						</p>
 					</a>
 				</li>

				<li class="nav-item">
					<a href="{{route('setting.create')}}" class="nav-link">
						 <i class="fas fa-cog"></i>
						<p>Setting
						</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{route('faq.view')}}" class="nav-link">
						<i class="fas fa-list-ul"></i>
						<p>FAQ</p>
					</a>
				</li>

				<li class="nav-item">
					<a href="{{route('policy.create')}}" class="nav-link">
						<i class="fas fa-copy"></i>
						<p>Policies</p>
					</a>
					
				</li>


 				{{-- <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Charts
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/charts/chartjs.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ChartJS</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/charts/flot.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Flot</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/charts/inline.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inline</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/charts/uplot.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>uPlot</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tree"></i>
              <p>
                UI Elements
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/UI/general.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>General</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/icons.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Icons</p>
                </a>
              </li>
            </ul>
          </li> --}}
 			</ul>
 		</nav>
 		<!-- /.sidebar-menu -->
 	</div>
 	<!-- /.sidebar -->
 </aside>