@extends('admin.layouts.app')
@push('custom-css')
<style>
    .card-primary:not(.card-outline)>.card-header {
        background-color: #dc3545;
    }
</style>
@endpush

@section('content')
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        {{Session::get('success')}}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                            {{Session::get('error')}}
                    </div>
                @endif
                

                



                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add Privacy Policy</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('privacyPolicy.store')}}">
                            @csrf
                            {{-- @if (isset($news->id))
                                <input type="hidden" name="id" value="{{ @$news->id }}">
                            @endif --}}
                            <div class="card-body">
                                
                                <div class="form-group">
                                    {{-- <label for="">Privacy Policy</label> --}}
                                    <textarea class="form-control editor @error('privacy_policy') is-invalid @enderror"
                                        name="privacy_policy" rows="5" placeholder="Enter ...">{{ @$news->news }}</textarea>
                                    @error('privacy_policy')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('news.view')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add Terms and Conditions</h3>
                        </div>

                        <form method="POST" action="{{route('termsAndConditions.store')}}">
                            @csrf
                            {{-- @if (isset($news->id))
                                <input type="hidden" name="id" value="{{ @$news->id }}">
                            @endif --}}
                            <div class="card-body">

                                <div class="form-group">
                                    {{-- <label for="">Terms and Conditions</label> --}}
                                    <textarea class="form-control editor @error('terms_and_conditions') is-invalid @enderror"
                                        name="terms_and_conditions" rows="5" placeholder="Enter ...">{{ @$news->news }}</textarea>
                                    @error('terms_and_conditions')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('news.view')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add Cookies Policy</h3>
                        </div>

                        <form method="POST" action="{{route('cookiesPolicy.store')}}">
                            @csrf
                            {{-- @if (isset($news->id))
                                <input type="hidden" name="id" value="{{ @$news->id }}">
                            @endif --}}
                            <div class="card-body">
                                
                                <div class="form-group">
                                    {{-- <label for="">Cookies Policy</label> --}}
                                    <textarea class="form-control editor @error('cookies_policy') is-invalid @enderror"
                                        name="cookies_policy" rows="5" placeholder="Enter ...">{{ @$news->news }}</textarea>
                                    @error('cookies_policy')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('news.view')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>
@endsection

@push('custom-script')
<script>
 $('.editor').summernote();
</script>
@endpush