@extends('admin.layouts.app')
@push('custom-css')
<style>
    .card-primary:not(.card-outline)>.card-header {
        background-color: #dc3545;
    }
</style>
@endpush

@section('content')
<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        {{Session::get('success')}}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                            {{Session::get('error')}}
                    </div>
                @endif
                

                



                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add Service</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('service.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if (isset($service->id))
                                <input type="hidden" name="id" value="{{ @$service->id }}">
                            @endif
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Heading</label>
                                    <input type="text" name="heading"
                                        class="form-control @error('heading') is-invalid @enderror"
                                        id="exampleInputEmail1" placeholder="Enter Heading" value="{{ @$service->heading }}">
                                    @error('heading')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Description</label>
                                    {{-- <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"> --}}
                                    <textarea class="form-control @error('description') is-invalid @enderror"
                                        name="description" rows="3" placeholder="Enter ...">{{ @$service->description }}</textarea>
                                    @error('description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Logo</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" accept="image/*" name="logo"
                                                class="custom-file-input @error('logo') is-invalid @enderror"
                                                id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        
                                        {{-- <div class="input-group-append">
                                  <span class="input-group-text">Upload</span>
                                </div> --}}
                                    </div>
                                    @error('logo')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                @if (isset($service->logo))
                                    <div class="form-group">
                                        <img src="{{ @asset('website/images/service')."/".@$service->logo}}" height="150" width="auto" alt="" srcset="">
                                    </div>
                                @endif
                                
                                {{-- <div class="form-check">
                              <input type="checkbox" class="form-check-input" id="exampleCheck1">
                              <label class="form-check-label" for="exampleCheck1">Check me out</label>
                            </div> --}}
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('service.view')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection