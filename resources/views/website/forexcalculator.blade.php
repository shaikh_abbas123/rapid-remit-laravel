@extends('website.layouts.app')
@section('content')
<div class="coooom">
	<div class="Converter">
		<div class="container-fluid cal-container">
			<div class="row">
				<div class="cal-first-heading col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<h1></h1></div>
			</div>
			<div class="container1">
				<div class="cal-container2">
					<div class="row cal-row">
						<form>
							<div class="form-group col-xl-4 col-lg-4 col-md-10 col-sm-10 col-11">
								<label class="center-label">Amount</label>
								<input class="center-label bg-white" placeholder="" id="ammount" type="number" min="0">
							</div>
							<div class="form-group col-xl-3 col-lg-3 col-md-5 col-sm-10 col-11">
								<label class="center-label1">From</label>
								<select name="countries" class="cal-dropdown1 center-label1 bg-white" id="from">
									<option class="cal-opt" id="cal-opt7" value="AED">AED United Arab Emirates Dirham</option>
									<option class="cal-opt" id="cal-opt7" value="AUD">AUD Australian Dollar</option>
									<option class="cal-opt" id="cal-opt7" value="CAD">CAD Canadian Dollar</option>
									<option class="cal-opt" id="cal-opt7" value="CNY">CNY Chinese Yuan</option>
									<option class="cal-opt" id="cal-opt7" value="EGP">EGP Egyptian Pound</option>
									<option class="cal-opt" id="cal-opt7" value="EUR">EUR Euro</option>
									<option class="cal-opt" id="cal-opt7" value="GBP">GBP British Pound Sterling</option>
									<option class="cal-opt" id="cal-opt7" value="INR">INR Indian Rupee</option>
									<option class="cal-opt" id="cal-opt7" value="JOD">JOD Jordanian Dinar</option>
									<option class="cal-opt" id="cal-opt7" value="LKR">LKR Sri Lankan Rupee</option>
									<option class="cal-opt" id="cal-opt7" value="NPR">NPR Nepalese Rupee</option>
									<option class="cal-opt" id="cal-opt7" value="PHP">PHP Philippine Peso</option>
									<option class="cal-opt" id="cal-opt7" value="PKR">PKR Pakistani Rupee</option>
									<option class="cal-opt" id="cal-opt7" value="USD">USD United States Dollar</option>
								</select>
							</div>
							<div class="form-group col-xl-1 col-lg-1 col-md-1 col-sm-0 col-0"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAzCAIAAAALlf8gAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAGWSURBVFhH7ZU9TwJBEIZtMIqFjSVGLCz8k5ZEKQ+ohCMmxjts9KzQTiu10L8BtXxpLhlfYNg7RjjvY4iJYfNUt5Mntzsfu9HJFdRZS+Wn7Kyl8lN2/lT6cXJGs/X18tbdOxYBYRL8aa9ksZXo8+m5u3skAgzJjt+36myF9+Gxu3MoAqYkvNPN/UHjiq1Eo7v7Tr4oY9Ikautg6NywlWjo3uKLiEkuBdvFkddmK9HAdnCCcEAgHZxfJuDimnyfrUT9SsN4QCDl/bSrd1oxKj1puWZUSsev2sYDNBLVdJcmKi6ipFpe5pJaRfHrt6n+QJkbfa/vaqMvPv9AilvDs8G3SIQ7FQERLJYis8gv+9DXJUsERLNAiupDDbIPfW3VRcCv/JDmi+gT9i0awHGYl6KvWx77ljwVcQhJ0de2wz70tdfGNAp2kxBI8SSwD8v3MTHlDI3EeABL8RiwLu0yRjCTlmu8mXYZIwgdv2rzPpbK8ccgUU2XpVqJGqNfUlP0i3/CCtp0gv5AmaI/+jKylspP2VmBNFf4BitAqCrzkGMqAAAAAElFTkSuQmCC" class="arrows" alt="" style="cursor: pointer;"></div>
							<div class="form-group col-xl-3 col-lg-3 col-md-5 col-sm-10 col-11">
								<label class="center-label2">To</label>
								<select name="countries" class="cal-dropdown2 center-label2 bg-white" id="to">
									<option class="cal-opt" id="cal-opt7" value="AED">AED United Arab Emirates Dirham</option>
									<option class="cal-opt" id="cal-opt7" value="AUD">AUD Australian Dollar</option>
									<option class="cal-opt" id="cal-opt7" value="CAD">CAD Canadian Dollar</option>
									<option class="cal-opt" id="cal-opt7" value="CNY">CNY Chinese Yuan</option>
									<option class="cal-opt" id="cal-opt7" value="EGP">EGP Egyptian Pound</option>
									<option class="cal-opt" id="cal-opt7" value="EUR">EUR Euro</option>
									<option class="cal-opt" id="cal-opt7" value="GBP">GBP British Pound Sterling</option>
									<option class="cal-opt" id="cal-opt7" value="INR">INR Indian Rupee</option>
									<option class="cal-opt" id="cal-opt7" value="JOD">JOD Jordanian Dinar</option>
									<option class="cal-opt" id="cal-opt7" value="LKR">LKR Sri Lankan Rupee</option>
									<option class="cal-opt" id="cal-opt7" value="NPR">NPR Nepalese Rupee</option>
									<option class="cal-opt" id="cal-opt7" value="PHP">PHP Philippine Peso</option>
									<option class="cal-opt" id="cal-opt7" value="PKR">PKR Pakistani Rupee</option>
									<option class="cal-opt" id="cal-opt7" value="USD">USD United States Dollar</option>
								</select>
							</div>
							<div class="form-group col-xl-1 col-lg-1 col-md-1 col-sm-12 col-11">
								<button type="button"><img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAYAAAA6GuKaAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAANnSURBVGhD7ZjbS9NhGMe/Ox/VeXabhzCRtDwjHa2MiiKiiG667qKLDpAkJBR4FXRRiAiZUUmFVJfddNFFf0MhZrp5nsdM3eZ0zkPP+/rGVIz0t58/EH4fGPvxPNv47P097/O8m+YlHCvYZWjF865ClVYKVVopVGmlUKWVYldKSx7jWoMBhjg79FaLiGyAPjUSCmExGMRyZFEE5UGydHJ5MQ7U3ELutavQaDUiGmVlcRFdrW3oaGzBVHsHBeQ74uguwVwvrrdFeGIS/m4v9GYjksuKRTSKRqtFYkE+TEmJCPb1Y25kTGRiR7L0yvIywlPTCHT3QqPTIaWiVGSisBKy52TBkpaG2SEfQr4RkYkNydKMlaUlhKdnEPCSOJVIUlEhNHq9yK6iM5tgdWXAkp6O0PAIZgd9IiOdmKQZTHxhagp+D4nTyibk53HRtegtFi5uzWDio5gdGBIZacQszeClMvkbgd4BEjbzkjDYbCK7it5ihs3tpBVPozIZRojVOL1PCrJIc6g5zE/8ots/xMVtmS4Y7HbakdHOwu6APSebajyV1/fc2ATvMttFPmnB/PgEF9fbrLC6XTBSL18rzjZnQv5emBwOXuPsiy5HIiK7NWSXZsyP04pT3bJatmW5+RBaK87aoYPaoTEhntd4mIkvbF18R6QZbAVDdPutLicS9+/johth4mwjT3d00usnRfT/7NjZQ2s0whgfxzfgv4gEZxEJ0JjfZl3vjDSVQtrBCpTU3UX25Qt8+GyEdZyfza/Q/qQJ/i6viG6NHZF2VlehrP4+nCerNi0L1jG+P25Ae8MzSVNSdmn32VMoe1iL1EOV0JqMIhplgSbot0dP8aPpBeZGx/iKbxfZpFkJZJ4/jdIH95BSWb7pkZVJdjQ2o7Olla7HaRNKGy6ySOtoszHhkroaEi7jPXoddCwN9g2g83krul638ZKQssJ/iVnaQB0i89xpFNXeQerhSj4N10HCfk8Puuls7XnznsvHSkzSpkQH3GeqUXj7BjKOH4F2wwmPCc90eeB595E/Aj19IhEbkqVNyUlwkXDBzevULY6JaBR2+5mwl2S9bz/w46tcSP65FZ+Xiz1XLiLjxFERWQ+bdL4vXzHw6TOC/YMiKg/q/9NKoUorhSqtFKq0UqjSSqFKK8UulAb+ABthNqjkHRqbAAAAAElFTkSuQmCC"></button>
							</div>
						</form>
					</div>
					<div class="row">
						<div class="center-para col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<h1>Enter your amount </h1></div>
						<div class="sides-para col-xl-3 col-lg-4 col-md-4 col-sm-0 col-0"></div>
						<div class="center-para col-xl-6 col-lg-5 col-md-4 col-sm-12 col-12">
							<h4></h4></div>
						<div class="sides-para col-xl-3 col-lg-3 col-md-4 col-sm-0 col-0"></div>
						<div class="center-para col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
							<p>All figures are live mid-market rates, which are not available to consumers and are for informational purposes only.</p>
						</div>
					</div>
				</div>
				<div class="cal-container3">
					<div class="country-card-wrapper df f-wrap jcc">
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/AED.png?alt=media&amp;token=757a7f9d-ebca-4d96-b461-e34bc2e8ec94" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>AED</h3>
									<p>United Arab Emirates Dirham</p>
								</div>
							</div>
							<div class="">
								<h4>3.6730</h4>
								<p>1 <span>USD</span> = 3.672975 <span>AED</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/AUD.png?alt=media&amp;token=fffb3752-2b96-4825-9f4e-737dc87abc38" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>AUD</h3>
									<p>Australian Dollar</p>
								</div>
							</div>
							<div class="">
								<h4>1.2956</h4>
								<p>1 <span>USD</span> = 1.29563 <span>AUD</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/CAD.png?alt=media&amp;token=2c483bf5-3484-430c-aad0-ccd5565187e3" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>CAD</h3>
									<p>Canadian Dollar</p>
								</div>
							</div>
							<div class="">
								<h4>1.2082</h4>
								<p>1 <span>USD</span> = 1.208225 <span>CAD</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/CNY.png?alt=media&amp;token=44dd8710-0009-42d6-9578-96802690b13d" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>CNY</h3>
									<p>Chinese Yuan</p>
								</div>
							</div>
							<div class="">
								<h4>6.3848</h4>
								<p>1 <span>USD</span> = 6.384801 <span>CNY</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/EGP.png?alt=media&amp;token=b33dde86-6542-4d8d-8de5-f7d32ebca549" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>EGP</h3>
									<p>Egyptian Pound</p>
								</div>
							</div>
							<div class="">
								<h4>15.6950</h4>
								<p>1 <span>USD</span> = 15.694981 <span>EGP</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://www.countryflags.io/BE/flat/64.png" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>EUR</h3>
									<p>Euro</p>
								</div>
							</div>
							<div class="">
								<h4>0.8210</h4>
								<p>1 <span>USD</span> = 0.820985 <span>EUR</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://www.countryflags.io/GB/flat/64.png" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>GBP</h3>
									<p>British Pound Sterling</p>
								</div>
							</div>
							<div class="">
								<h4>0.7075</h4>
								<p>1 <span>USD</span> = 0.707515 <span>GBP</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/INR.png?alt=media&amp;token=d067c420-b8c5-408f-9c88-380b1a6397fe" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>INR</h3>
									<p>Indian Rupee</p>
								</div>
							</div>
							<div class="">
								<h4>73.0765</h4>
								<p>1 <span>USD</span> = 73.076501 <span>INR</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://www.countryflags.io/JO/flat/64.png" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>JOD</h3>
									<p>Jordanian Dinar</p>
								</div>
							</div>
							<div class="">
								<h4>0.7090</h4>
								<p>1 <span>USD</span> = 0.70901 <span>JOD</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://firebasestorage.googleapis.com/v0/b/beta-rapidremit.appspot.com/o/sri-lanka-flag-round-medium.png?alt=media&amp;token=48060b53-a20b-4c4d-b10e-70cd1837d801" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>LKR</h3>
									<p>Sri Lankan Rupee</p>
								</div>
							</div>
							<div class="">
								<h4>197.5533</h4>
								<p>1 <span>USD</span> = 197.553335 <span>LKR</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://www.countryflags.io/NP/flat/64.png" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>NPR</h3>
									<p>Nepalese Rupee</p>
								</div>
							</div>
							<div class="">
								<h4>116.6886</h4>
								<p>1 <span>USD</span> = 116.688625 <span>NPR</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://www.countryflags.io/PH/flat/64.png" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>PHP</h3>
									<p>Philippine Peso</p>
								</div>
							</div>
							<div class="">
								<h4>47.8330</h4>
								<p>1 <span>USD</span> = 47.832997 <span>PHP</span></p>
							</div>
						</div>
						<div class="contact-card df jcb aic">
							<div class="df aic">
								<div class="divFlag df aic"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/PKR.png?alt=media&amp;token=caa92f87-fd7a-4876-a157-c83a1dd534e7" class="flag" alt=""></div>
								<div class="currency-name-div">
									<h3>PKR</h3>
									<p>Pakistani Rupee</p>
								</div>
							</div>
							<div class="">
								<h4>155.2137</h4>
								<p>1 <span>USD</span> = 155.213725 <span>PKR</span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container1">
		<div class="row cal-note">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h2>Note</h2>
				<p>All figures re live mid-market rates, which are not available to consumers and are for informational purposes only. To see the rates we quote for money transfer, please select Live Money Transfer Rates.</p>
			</div>
		</div>
	</div>
</div>


@endsection
@push('custom-css')
<link href="{{asset('website/css/cal.css')}}" rel="stylesheet">
@endpush




