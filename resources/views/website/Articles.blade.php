@extends('website.layouts.app')
@section('content')
<div class="container-fluid article">
	<div class="container1">
		<div class="row articleRow1">
			<div class="col-xl-9 col-lg-9 col-md-8 col-sm-12 col-12"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/images%2FarticleImg1.b4c1c12f.png?alt=media&amp;token=02f50e0d-e327-4256-b51b-1e6c69e1cdbb" alt="">
				<div class="articleRedLine"></div>
				<h2>Nothing adverse' in overseas remittance data, says CBDT</h2></div>
			<div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
				<div class="News01">
					<h4>ARTICLE</h4>
					<h6>1</h6>
					<h5>Feb 1 2021</h5></div>
			</div>
		</div>
		<div>
			<div class="row articleRow2">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<h1 style="line-height: 1;">High net-worth individuals transferring funds abroad under the Liberalised Remittance Scheme (LRS) can breathe easy.</h1></div>
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<p>The Directorate of Intelligence &amp; Criminal Investigation (I&amp;CI) of the income tax department during August and September 2019 obtained LRS data from several banks in Mumbai and Delhi and verification of the top 100 cases was undertaken. However, nothing adverse was found,” said the report, which was submitted to SIT probing black money earlier this year.</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection