<link href="{{asset('website/css/2.13dc7278.chunk.css')}}" rel="stylesheet">
<link href="{{asset('website/css/main.0bc0ab0e.chunk.css')}}" rel="stylesheet">
<link href="{{asset('website/css/custom.css')}}" rel="stylesheet">
<link href="{{asset('website/css/animate.css')}}" rel="stylesheet">
<!-- <link href="{{asset('website/css/carousel.min.css')}}" rel="stylesheet"> -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="" crossorigin="">

@stack('custom-css')