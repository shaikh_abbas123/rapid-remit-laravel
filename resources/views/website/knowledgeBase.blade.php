@extends('website.layouts.app')
@section('content')
<div class="coooom">
    <div class="newsK">
        <div class="newsDivTop">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <h1 class="react-reveal" style="opacity: 1;">
                        <span class="span-1 wow animate__flipInYMainPageTitle1">K</span>
                        <span class="span-2 wow animate__flipInYMainPageTitle2">N</span>
                        <span class="span-3 wow animate__flipInYMainPageTitle3">O</span>
                        <span class="span-4 wow animate__flipInYMainPageTitle4">W</span>
                        <span class="span-5 wow animate__flipInYMainPageTitle5">L</span>
                        <span class="span-6 wow animate__flipInYMainPageTitle6">E</span>
                        <span class="span-7 wow animate__flipInYMainPageTitle7">D</span>
                        <span class="span-8 wow animate__flipInYMainPageTitle8">G</span>
                        <span class="span-9 wow animate__flipInYMainPageTitle9">E</span>
                        <span class="span-10 wow animate__flipInYMainPageTitle10"></span>
                        <span class="span-11 wow animate__flipInYMainPageTitle11">B</span>
                        <span class="span-12 wow animate__flipInYMainPageTitle12">A</span>
                        <span class="span-13 wow animate__flipInYMainPageTitle13">S</span>
                        <span class="span-14 wow animate__flipInYMainPageTitle14">E</span>
                    </h1>
                </div>
            </div>
            <div class="container1 newsPartB">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                        <a href="#firstNews" id="firstNews news1">
                            <div class="divNews" id="newsa">
                                <h2>NEWS</h2>
                            </div>
                        </a>
                        <a href="#firstNews" id="firstNews news2">
                            <div class="divNews" id="newsb" style="display: none;">
                                <div class="blackBelt">
                                    <h3 class="blackBeltH3">NEWS</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                        <a href="#firstArticle" id="firstArticle">
                            <div class="divNews" id="articlesa" style="display: none;">
                                <div>
                                    <h2>ARTICLES</h2>
                                </div>
                            </div>
                        </a>
                        <a href="#firstArticle" id="firstArticle">
                            <div class="divNews" id="articlesb">
                                <div class="blackBelt">
                                    <h3 class="blackBeltH3">ARTICLES</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                        <a href="#firstBlog" id="firstBlog">
                            <div class="divNews" id="blogsa" style="display: none;">
                                <div>
                                    <h2>BLOGS</h2>
                                </div>
                            </div>
                        </a>
                        <a href="#firstBlog" id="firstBlog">
                            <div class="divNews" id="blogsb">
                                <div class="blackBelt">
                                    <h3 class="blackBeltH3">BLOGS</h3>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid newsK">
        <div class="container-fluid newsPartTwo" id="newsTab">
            <div class="container1">
                <div class="row no-gutters">
                    @foreach($newsData as $item)
                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="cardNews">
                                <span class="spanHeading">News</span><span class="spanDate"><time datetime="1612188093424"></time></span>
                                <p class="cardNewsTitle" style="height: 82px;">{{@$item->heading}}</p>
                                <p class="cardNewsP" style="height: 92px;">{{@$item->news}}</p>
                                <a href="/News">
                                    <span class="spanHeading" style="color: rgb(249, 13, 38);">+Read More..</span>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="container-fluid newsPartThree" id="articlesTab" style="display: none;">
            <div class="container1">
                <div class="row justify-content-center">
                    <!-- <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12" style="margin-bottom: 20px;">
                        <div class="card"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/images%2FarticleImg1.b4c1c12f.png?alt=media&amp;token=02f50e0d-e327-4256-b51b-1e6c69e1cdbb" height="200" class="card-img-top" alt="...">
                            <div class="card-body" style="height: 265px; overflow: hidden;">
                                <h5 class="card-title">Article</h5>
                                <p style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; color: rgb(95, 95, 95); font-weight: 700;">High net-worth individuals transferring funds abroad under the Liberalised Remittance Scheme (LRS) can breathe easy.</p>
                                <p class="card-text" style="display: -webkit-box; -webkit-line-clamp: 5; -webkit-box-orient: vertical; overflow: hidden;">The Directorate of Intelligence &amp; Criminal Investigation (I&amp;CI) of the income tax department during August and September 2019 obtained LRS data from several banks in Mumbai and Delhi and verification of the top
                                    100 cases was undertaken. However, nothing adverse was found,” said the report, which was submitted to SIT probing black money earlier this year.</p>
                                <a href="/Articles">
                                    <p class="card-text"><small class="" style="color: rgb(249, 13, 38); font-size: 14px; font-weight: 700;">+Read More..</small></p>
                                </a>
                            </div>
                        </div>
                    </div> -->
                    @foreach($data as $item)
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12" style="margin-bottom: 20px;">
                        <div class="card"><img src="{{ @asset('website/images/article')."/".$item->logo}}" height="200" class="card-img-top" alt="...">
                            <div class="card-body" style="height: 265px; overflow: hidden;">
                                <h5 class="card-title">{{@$item->banner_title}}</h5>
                                <p style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; color: rgb(95, 95, 95); font-weight: 700;">{{@$item->title}}</p>
                                <p class="card-text" style="display: -webkit-box; -webkit-line-clamp: 5; -webkit-box-orient: vertical; overflow: hidden;">
                                {{@$item->description}}
                                </p>
                                <a href="/Articles">
                                    <p class="card-text"><small class="" style="color: rgb(249, 13, 38); font-size: 14px; font-weight: 700;">+Read More..</small></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12" style="margin-bottom: 20px;">
                        <div class="card">
                            <img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/images%2FDollar.jpg?alt=media&amp;token=247bf9e1-dcf7-4c2b-af89-4599ca6edfee" height="200" class="card-img-top" alt="...">
                            <div class="card-body" style="height: 265px; overflow: hidden;">
                                <h5 class="card-title">Article</h5>
                                <p style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; color: rgb(95, 95, 95); font-weight: 700;">The Dollar, The World’s Best Currency at This Time</p>
                                <p class="card-text" style="display: -webkit-box; -webkit-line-clamp: 5; -webkit-box-orient: vertical; overflow: hidden;">
                                    When you are debating about the world’s best currency in 2021, you cannot leave the US Dollar out of the conversation. You can make the case for other currencies like the British Pound (GBP) and the Euro (EUR) as they are stronger
                                    and valued more than the US Dollar; or the Chinese Yuan (CNY), a currency in massive demand in 2021 due to China’s economic prowess; but the US Dollar is bound to be part of the conversation. In this article, we examine
                                    why is the US dollar the world’s best currency today in 2021? In 2021, the US Dollar remains the strongest currency due to two major reasons: 1. The US Dollar is the world’s major reserve currency: A reserve currency is
                                    a currency held by central banks in all countries and used for all international transactions. So let’s say if India wanted to trade 100 tonnes of rice with Maldives, they would not pay Maldives in Indian Rupees but rather
                                    use the reserve currency for the transaction. The Reserve Bank of India (India’s central bank) has an accumulated pool of reserve currency that it uses to pay for all international transactions. The reserve currency is
                                    also used for investments and international debt obligations. The US Dollar became the world’s reserve currency after the Bretton Woods agreement in 1944 at the end of WWII. Before WWII, the British Pound (GBP) used to
                                    be the major reserve currency but after the war ended, most European and Asian countries were completely shattered. The US economy had suffered from the war but not as seriously as other countries and was able to establish
                                    itself as the major reserve currency. Today, the US dollar is the dominant reserve currency around the world and 88 percent of all international transactions were done with the US dollar in 2019. The US dollar also accounts
                                    for 61% of official foreign exchange reserves in central banks around the world. The US Dollar is also widely used as a reserve currency today because of its stability and consistency as the dollar exchange rate has not
                                    experienced any significant fluctuations since WWII. 2. The US Dollar is the currency of international funding and investment The second reason for US Dollar dominance in 2021 is that it is commonly used as a currency for
                                    international debt and investments. In its paper, the Bank of International Settlements asserted that ‘around half of all cross-border bank loans and international debt securities are denominated in US dollars.” This means
                                    that third world countries like Brazil and Yemen are completely dependent on the US Dollar for running their economies. Every time, Yemen takes a loan from IMF or the World Bank or any international financial institution
                                    to battle its domestic economic crisis, the loan is processed in US Dollars. This overdependence of poorer and less-developed countries on the US Dollar for loans and investment purposes is another major reason for the
                                    dominance of the dollar. Moreover, residents in third world countries have been converting their savings to US Dollars rather than saving in their local currency. They view the US dollar as a completely safe instrument
                                    of saving due to the dollar exchange rate today and are assured that their saving won’t lose value in the long run. Future of the US Dollar: Leading experts say it is unlikely that the US dollar will be dethroned as the
                                    global reserve currency any time soon, despite the far reaching consequences of the coronavirus pandemic. Part of the reason for this is that most countries that use the US dollar as their reserve currency have a huge reserve
                                    of dollars in their central banks. In order to move away from the US dollar as your reserve currency, countries will need to find a way to exchange the millions and trillions of dollars they already have in their official
                                    reserves. This is also the reason that despite some inflation in USA under Biden, the world is still confidently continuing to use the US dollar. It is true that there are currencies that are stronger than the US dollar
                                    in terms of exchange rate for example the Kuwaiti dinar or the Swiss franc, but the US dollar still remains the best currency in 2021 due to world’s overdependence on it. The only way that US dollar will be dethroned as
                                    the global reserve currency is when countries lose their confidence in the US Dollar and decide to stop using the US dollar as their financial reserve currency and for international debt purposes. In order to find best
                                    currency transfer rates in the US and live exchange rate today in the United States, visit our website www.rapidremit.biz
                                </p>
                                <a href="/Articles">
                                    <p class="card-text"><small class="" style="color: rgb(249, 13, 38); font-size: 14px; font-weight: 700;">+Read More..</small></p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12" style="margin-bottom: 20px;">
                        <div class="card">
                            <img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/images%2FFixed-And-Floating-Exchange-Rate-Systems.jpg?alt=media&amp;token=55b1851a-3dfe-45fa-af97-ebd9519b9bb4" height="200" class="card-img-top" alt="...">
                            <div class="card-body" style="height: 265px; overflow: hidden;">
                                <h5 class="card-title">Article</h5>
                                <p style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden; color: rgb(95, 95, 95); font-weight: 700;">FIXED AND FLOATING EXCHANGE RATE SYSTEMS</p>
                                <p class="card-text" style="display: -webkit-box; -webkit-line-clamp: 5; -webkit-box-orient: vertical; overflow: hidden;">
                                    An exchange rate system is the mechanism through which the exchange rate of a country is determined. The choice of the exchange rate system has a huge impact on other macroeconomic indicators of the country like inflation,
                                    unemployment and on macroeconomic policies like monetary and fiscal policy. We closely look at the fixed and floating exchange rate systems and analyze which is the best in today’s world. What is a Fixed Exchange Rate?
                                    A fixed exchange rate is when the central bank of a country pegs its own currency to that of another currency (or a basket of currencies) and maintains the currency rate over a period of years. The central bank fixes a
                                    narrow exchange rate range and maintains the currency rate in this range only. Bahrain, United Arab Emirates and Hong Kong are some countries that have been using the fixed exchange rate system. Countries with this system
                                    usually fix their currency rate to one or more major trading partners. Most countries in Africa with a fixed exchange rate peg their currency against the euro whereas most countries in the Middle East peg their currency
                                    to the US dollar. Countries prefer pegging their exchange rate against the US dollar and the Euro as these currencies are extremely strong and stable and resultantly less prone to changes compared to other currencies. Every
                                    time there is increased demand or supply, the central bank intervenes in the currency market to maintain the exchange rate within the range defined. In the case of increased demand, the central bank will withhold supply
                                    of the currency to bring demand levels to equilibrium and increase currency supply when demand is low. Advantages and Disadvantages of Fixed Exchange Rate System: The foremost advantage of a fixed exchange rate system is
                                    stability. Foreign investors feel safer when investing their money into the country as they can speculate the rate of return without being fearful of fluctuations to exchange rates in the future. Moreover, in a fixed exchange
                                    rate system, expatriates are more certain of the currency rate they will get when transferring their money. A fixed exchange rate system also leads to lower domestic inflation. The foremost disadvantage of a fixed exchange
                                    rate system is that the central bank needs a lot of foreign reserves to maintain the currency rate. It often happens that banks accumulate large idle reserves of foreign currencies or of their own currency which cannot
                                    be utilized. The central bank would also need to constantly monitor the domestic market and international trade and quickly respond to any changes in the market. This can also lead to a currency crisis when the central
                                    bank does not have enough foreign exchange reserves and has to devalue or revalue its own currency to maintain the fixed currency rate. What is a Floating Exchange Rate? Floating exchange rate is determined by the forces
                                    of the private market of currencies. This means that the forces of demand and supply will determine the exchange rate. The underlying idea behind the floating exchange rate system is that the currency rate will automatically
                                    correct itself to fluctuations in the market. If demand for a currency like the Japanese yen is high, the value of the yen will automatically increase. As the value of the Japanese yen increases, Japanese products will
                                    become expensive and imported products will become cheaper which will bring the currency rate back to its previous position. Similarly when the yen decreases in value Japanese products will become cheaper which will lead
                                    to increase in its demand. This self correcting mechanism also corrects imbalances in the Balance of Payments of a country. Advantages and Disadvantages of Free Floating Exchange Rate: Due to automatic adjustment in the
                                    free floating exchange rate, the government or central bank does not have to intervene into the market to maintain the exchange rate. Moreover, when a fixed exchange rate is used by a country, the central bank also has
                                    to keep a large reserve of foreign currency to correct any market fluctuations. The resources used to maintain this foreign reserve can be utilized in other areas of the economy like healthcare or education. Under the free
                                    floating exchange rate, this is not required as markets correct themselves based on principles of demand and supply. However, the free floating exchange rate system comes with its limitations. Most importantly, the exchange
                                    rate is extremely unpredictable and results in uncertainty for foreign investors and expatriates who wish to send money into the country. Also if a country is currently facing economic problems, a floating exchange rate
                                    can worsen them. For example, if there is unemployment in a country, depreciation of the currency will lead to further unemployment as imports will become cheaper. Which is the better system? Although fixed and free floating
                                    exchange rates have their own pros and cons, most economies today use a midway option to these two called the managed floating exchange rate system. In fact, 43% of all countries today are using a managed floating exchange
                                    rate, including India and China. The popularity of the managed floating exchange rate system comes from the fact that it takes the advantages of the free floating exchange rate and eliminates the unpredictability and lack
                                    of control that comes with the system. It mostly happens that when left to market forces, the exchange rate will not automatically correct itself and there always remains the risk of over depreciation or over appreciation
                                    of the exchange rate. In a free floating system, the free market does not always correct itself and more often than not, governments are forced to intervene. In a managed floating exchange rate system, the market is allowed
                                    to work its magic without any government intervention. The government will only intervene when it sees the risk of high depreciation or appreciation of the currency rate. The popularity of the managed exchange rate system
                                    stems from the fact that the other two exchange rate systems have their flaws and there always remains a risk of running an economic or monetary crisis.
                                </p>
                                <a href="/Articles">
                                    <p class="card-text">
                                        <small class="" style="color: rgb(249, 13, 38); font-size: 14px; font-weight: 700;">+Read More..</small>
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            
            <!-- <table>
                <tr>
                    <th>Banner Title</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Logo</th>
                </tr>
                @foreach($data as $item)
                    <tr>
                        <td>{{@$item->banner_title}}</td>
                        <td>{{@$item->title}}</td>
                        <td>{{@$item->description}}</td>
                        <td> <img src="{{ @asset('website/images/article')."/".$item->logo}}" alt="" srcset=""></td>
                    </tr>
                @endforeach



            
            </table> -->

        </div>
        <div class="container-fluid newsPartFour" id="blogsTab" style="display: none;">
            <div class="container1">
                <div class="row">
                    @foreach($blogData as $item)
                        <div style="display: flex; justify-content: center; background-color: rgb(242, 242, 242); margin-bottom: 50px; padding: 20px 0px;">
                            <div class="john-detail col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12"><span class="spanHeading">{{@$item->sub_title}}</span><span class="spanDate"><time datetime="1612189557210">{{@$item->created_at->format('d.m.Y')}}</time></span><br>
                                <p class="cardNewsTitle" style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden;">{{@$item->title}}</p>
                                <p id="para2" style="display: -webkit-box; -webkit-line-clamp: 5; -webkit-box-orient: vertical; overflow: hidden;">{{@$item->description}}</p><a href="/blogs"><span class="spanHeading" style="color: rgb(249, 13, 38);">+Read More..</span></a></div>
                            <div class="col-xl-4 col-lg-4 d-lg-block d-md-none d-sm-none col-sm-12 d-none col-12">
                                <div class="pic1">
                                    <div class="pic-i"></div>
                                    <div class="pic-j"><img src="{{ @asset('website/images/blog')."/".$item->logo}}" alt=""></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <!-- <div style="display: flex; justify-content: center; background-color: rgb(242, 242, 242); margin-bottom: 50px; padding: 20px 0px;">
                        <div class="col-xl-4 col-lg-4 d-lg-block d-md-none d-sm-none col-sm-12 d-none col-12">
                            <div class="pic1">
                                <div class="pic-i-mirror"></div>
                                <div class="pic-j"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/images%2FInformal.jpg?alt=media&amp;token=3a757bd5-98e7-493c-9c45-80085b69d709" alt=""></div>
                            </div>
                        </div>
                        <div class="john-detail col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12" style="margin-bottom: 20px;"><span class="spanHeading">Blogs</span><span class="spanDate"><time datetime="1619174129090">23rd Apr,2021</time></span><br>
                            <p class="cardNewsTitle" style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden;">Are Informal Remittance Channels Worth the Risk?</p>
                            <p id="para2" style="display: -webkit-box; -webkit-line-clamp: 5; -webkit-box-orient: vertical; overflow: hidden;">If you are an expat living in the UAE and have been sending your remittance to your home country, there is a high chance that you have been confused between opting for an informal channel of remittance like hawala and a formal
                                channel like a bank or transfer service. We analyze whether it is viable to stick to the cheaper but more risky option of using hawala or, if it is more feasible to use the slightly expensive but more formal options of a bank
                                or a transfer service. In order for you to reach to a sensible decision, it is important to understand how the two systems operate. "Informal Remittance Channel or Hawala:" So how exactly does the hawala work? &lt;br&gt; Let’s
                                assume that Asif works in Abu Dhabi and wants to send the money to his hometown in New Delhi to his family. Asif approaches a hawala agent in Abu Dhabi with his request. The agent in Abu Dhabi reaches out to his associate in
                                New Delhi and asks him to transfer the money to Asif’s family on his behalf. The hawala agent in Abu Dhabi receives cash from Asif here and his associate in New Delhi gives it to his family. No cash has been transferred physically
                                or electronically and Simply put, the hawala system moves money across borders without actually moving money. Why is hawala not a good remittance channel? The hawala system is used primarily by expats in UAE, especially from
                                South Asia because it is a cheaper form of remittance. So why should Asif not use it? Although a cheaper remittance channel, informal channels have some alarming limitations. First and foremost, hawala operators are at risk
                                of arrest all the time and if they are to be arrested at any time while you are performing your transaction, you will never see your money again. Just imagine losing your monthly salary of thousands of dirhams to save fifteen
                                to twenty dirhams of transaction fee. Also, since the transaction is done in cash only, there always remains a chance of getting mugged, especially when the amount is so big. Asif or his family member in New Delhi can always
                                get mugged after they have received the cash. Given New Delhi’s crime rate, this would be a big no. Formal Remittance Channels Moving to the formal and more secure remittance channels, there are bank transfers and transfer
                                services like MoneyGram and XpressMoney. Banks charge a hefty transfer fee on your remittance and their exchange rate is usually much higher than the open market rate. The most viable option for expats to send remittances is
                                transfer services like MoneyGram and Al Ghurair Exchange that operate locally in UAE. These transfer services are extremely safe and reliable. You know the money you transfer will reach your family, GUARANTEED. With some transfer
                                companies like UAE Exchange or XpressMoney, you can even track the status of your remit to stay on top of your transaction. These transfer companies charge a small fee (15-25 dirhams) for their services. Like I said above,
                                I would forego a few dirhams rather than a few thousand dirhams. PRO TIP: You can use Rapid Remit to compare the exact amount that will be received by your family back home through different transfer services in the UAE. Moreover,
                                most of these exchange companies have websites and phone apps that make it extremely convenient to transfer money from the comfort of your home. When you use the hawala service, you have to pull cash from an ATM or your bank
                                branch and coordinate with the agent in your city. Similarly your family in New Delhi has to deposit the money into a bank account after receiving it from the associate and coordinate with the associate in India. With a transfer
                                service, you can literally send money in a few clicks from your mobile phone or website. The bottomline is: Use a transfer service and you will have peace of mind and a sense of security AND it is extremely convenient.
                            </p><a href="/blogs"><span class="spanHeading" style="color: rgb(249, 13, 38);">+Read More..</span></a></div>
                    </div>
                    <div style="display: flex; justify-content: center; background-color: rgb(242, 242, 242); margin-bottom: 50px; padding: 20px 0px;">
                        <div class="john-detail col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12"><span class="spanHeading">Blogs</span><span class="spanDate"><time datetime="1619922695618">2nd May,2021</time></span><br>
                            <p class="cardNewsTitle" style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden;">Best 5 online money transfer platforms of 2021</p>
                            <p id="para2" style="display: -webkit-box; -webkit-line-clamp: 5; -webkit-box-orient: vertical; overflow: hidden;">If you are looking for a way to transfer money internationally and confused about which is the best option for you? We shortlist top 5 online money transfer platforms in 2021 according to six factors that can make or break your
                                international money transfer. These factors are speed of transfer, security of the process, cost accrued on your money transfer, how convenient is the platform, what customers say about them and the level of transparency of
                                the online money transfer platforms. 1. Wise 2. World Remit 3. MoneyGram 4. Rapid Remit 5. OFX 1. Wise The first and foremost on our list is Wise, formerly known as TransferWise. They have more than 114,000 reviews on TrustPilot
                                and 86% of these customers find their service excellent. They are regulated by the Financial Conduct Authority, UK’s financial regulatory authority and Wise is committed to keeping its customers’ personal data safe with extra
                                layers of security like two-factor authentication. For most currencies, Wise transfers your money the same day or might take a maximum of two days, if you have a bigger amount. Cost Wise (pun intended), they are ideal for small
                                transfers. This is because they charge a percentage of the total amount that you are thinking of sending. Since you can use the Wise platform to transfer money with other services like Remitly or Western Union , for most cases
                                Wise itself is not the cheaper option. This is the case for most areas in the Middle East and Asia. Transferring money through Wise is extremely convenient as it has mobile applications for Google Play Store and Apple’s App
                                Store that are easy to use and you can send your money in less than a minute. In terms of transparency, the online money transfer platform does extremely well. All variables are clearly mentioned on their transfer page and
                                when you change one variable, other variables adjust themselves automatically. 2. World Remit World Remit is a really fast money transfer platform with your money reaching the recipient in minutes in most cases. As with Wise,
                                they are also registered with FCA in UK and fully licensed. World Remit has 45,660 reviews on TrustPilot with 79% rating their service as ‘Excellent’. They have served over 4 million customers since its creation in 2010. World
                                Remit is a cheap option for most transfers. However, this rate varies massively depending on the locations of your transfer so do check in their forex calculator for yourself. World Remit is extremely convenient for customers
                                as they have a lot of options including bank transfer through credit or debit cards, cash pickups and mobile money. However, the website is not as user-friendly as the Wise website. With World Remit, you can transfer money
                                to more than 90 locations but there is a catch. For example if you want to send money from UAE to Pakistan or India, there is only one transfer option available to send money which is mobile airtime. So, transfer methods on
                                World Remit vary according to where you want to send your money and how much. World Remit claims that they do not charge you transfer fee as is displayed on the Forex calculator on their website.Whereas we found out that the
                                exchange rate offered by them is weaker than what might be offered on the open market. By appreciating the exchange rate, World Remit makes up for the ‘no transfer fee’ gimmick. 3. Money Gram Money Gram has been operating for
                                more than 70 years now in the international money transfer space and is the second largest money transfer company in the world after Western Union. With Money Gram, cash transfers are available within minutes while bank transfers
                                might take longer (a few hours to a day). They have more than 10,000 reviews on TrustPilot out of which 66% are excellent. Money Gram accepts credit, debit cards, bank transfers, cash pickups and mobile wallets. The transfer
                                process is a little tedious and lengthy. If you wish to transfer through Money Gram, they will first verify the information you have provided them with third party sources or by calling you. Once verified, you will be provided
                                an 8-digit PIN number which you need to share with the receiver. These steps also ensure that your international transfer end up where it is meant to be. Money Gram also have a huge network of countries where you can send money
                                to. If you are using the common currencies of USD, GBP and Euro the rates are favorable. However, if you are using a third world country like Kenya or Bangladesh, you might end up paying more money. Best way to find out how
                                much you will eventually pay is to use the Money Gram pricing calculator. Money Gram is comparatively more expensive compared to other options on this list for international money transfers. 4. Rapid Remit Rapid Remit is an
                                out-of-the-box solution for international money transfer needs of people residing in the UAE and GCC region. Rapid Remit is an online money transfer comparison platform where you can compare more than 20 features of your desired
                                money transfer service. Rapid Remit not only helps you find a money transfer service best suited to your needs, but also allows you to transfer the money using its platform. Rapid Remit is extremely secure as all money transfer
                                services listed on Rapid Remit are licensed and regulated by the government of UAE. The best part of Rapid Remit is you get to decide what you would prefer in your transfer service. Whether you want your money transfer service
                                to be the cheapest, fastest, most reliable, most convenient or all of them; you make the decision. By using Rapid Remit for example, you can find a very cheap service for your international money transfer from the UAE. Let’s
                                say you want to send 5,000 AED from UAE to Pakistan. With Wise, the transfer amount is PKR 202,302, whereas Al Ghurair Exchange listed on Rapid Remit offers a receivable amount of PKR 227,000 in Pakistan. This means you can
                                save up to 25,000 rupees on your international money transfer compared to all other options in this list. Rapid Remit not only gives you the option to compare transfer services, but also carry out your international money transfers
                                on their website, which makes it extremely convenient to use as you can compare and transact in one place. They have a user friendly website and Rapid Remit mobile applications are available for Google Play Store and Apple
                                Store. Moreover, with their unique remittance tracking feature, you also have the option to track your transactions at every single stage and stay updated with the status of your transfer. Best of all, the Rapid Remit service
                                is absolutely free to use and they do not charge their customers a single penny for all of these services. All you need to pay for is the charges of your selected money transfer service (if there are any). 5. OFX Operating
                                since 1998 in the international money transfer space, OFX is a slightly different but interesting option for international money transfers, especially for expatriates working away from their home country. For example, you can
                                lock rates today for the next 12 months and OFX will automatically transfer money every month from your bank account on the locked rate. Moreover, you can also lock a limit for your desired exchange rate and once that rate
                                is met, your currency will be automatically converted. Transfers with OFX are extremely cheap as they do not charge any transaction fee and their exchange rates are also pretty reasonable (a little higher than the market rate
                                but still cheaper than most alternatives). Using OFX comes with its disadvantages. They are not the fastest as their transactions typically take anywhere between one to five days, depending on where you want to send your money.
                                Another con of using OFX is that it only allows bank transfers.
                            </p><a href="/blogs"><span class="spanHeading" style="color: rgb(249, 13, 38);">+Read More..</span></a></div>
                        <div class="col-xl-4 col-lg-4 d-lg-block d-md-none d-sm-none col-sm-12 d-none col-12">
                            <div class="pic1">
                                <div class="pic-i"></div>
                                <div class="pic-j"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/images%2Fimage.jpg?alt=media&amp;token=1d36b315-dadd-4d9d-bf76-2e6e1cd95812" alt=""></div>
                            </div>
                        </div>
                    </div>
                    <div style="display: flex; justify-content: center; background-color: rgb(242, 242, 242); margin-bottom: 50px; padding: 20px 0px;">
                        <div class="col-xl-4 col-lg-4 d-lg-block d-md-none d-sm-none col-sm-12 d-none col-12">
                            <div class="pic1">
                                <div class="pic-i-mirror"></div>
                                <div class="pic-j"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/images%2Fmoney-transfer-vancouver-bc.png?alt=media&amp;token=08b01690-41d3-4158-bdec-b1429385aa9b" alt=""></div>
                            </div>
                        </div>
                        <div class="john-detail col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12" style="margin-bottom: 20px;"><span class="spanHeading">Blogs</span><span class="spanDate"><time datetime="1619922958939">2nd May,2021</time></span><br>
                            <p class="cardNewsTitle" style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden;">How to Make an International Money Transfer Online</p>
                            <p id="para2" style="display: -webkit-box; -webkit-line-clamp: 5; -webkit-box-orient: vertical; overflow: hidden;">The two primary reasons for choosing to do your international money transfer online are: i) It is extremely convenient. You can seriously perform the whole transaction from your smartphone sitting on your couch in minutes. ii)
                                With the coronavirus pandemic worsening, online money transfers give you the option to remain in your safety bubble and eliminate the risk of getting the virus. If you are looking to complete an international money transfer
                                online, there are two popular options available that are willing to do the job for you. When transferring your money internationally, there is a long list of wishes that you would want in your transfer service. You want your
                                international transfer service to be fast, reliable, convenient, and most importantly cheap. International money transfers through banks: If you are using a bank to transfer your money, you can send money from the bank’s online
                                internet banking service or mobile application (given the bank you are using has these services). It is extremely convenient as you can finish your transfer in less than 60 seconds. Also banks are the fastest mode of international
                                money transfer so if you are in a rush or have an emergency, you should use a bank. However, banks are really expensive. Depending on the bank you are using, you might be charged with a transaction fee, an expensive exchange
                                rate and hidden charges on your international money transfer. Personally, I wouldn’t recommend using a bank for your international money transfer but if you do want to pay more money or have an emergency, check out the DirectRemit
                                service by Emirates NBD Bank and Mashreq Bank’s QuickRemit services. In order to use online banking services for your international money transfer, you will need to have a bank account and then register for the bank’s online
                                services like Internet Banking or Mobile App. Some banks might ask you for more information, but some pieces of information that you should have ready before starting your transfer are: i) the country you want to send your
                                money to ii) the amount you want to send iii) your own and the receiver’s contact details like phone number, email address, residential address and proof of identity in some cases. International money transfers through transfer
                                services: The other option for you is transfer operators like MoneyGram and Western Union. With this option, you do pay some transfer fee for your international money transfer but this fee is considerably less compared to bank
                                charges. These transfer services are extremely convenient as most transfer operators have an online money transfer option and majority of them have mobile apps. They are also extremely reliable as they are registered with the
                                central banks of the country so don’t be worried about your money disappearing in thin air. Moreover, some money transfer operators are as fast as some banks as these services do use bank services for their transfers. If you
                                don’t want to overspend on your international money transfer, I would recommend using a transfer service. International money transfers through Rapid Remit: Although you can complete your transaction on the website of the money
                                transfer operator of your choice, you will be stuck with their service only. Let’s assume that you are currently using MoneyGram to transfer your money and it takes them approximately three hours to complete the transaction,
                                whereas Western Union takes only an hour. If you want to shift from MoneyGram to Western Union, you will need to register for a separate account with Western Union and connect your bank with them. A better option if you are
                                living in UAE is using the Rapid Remit service. Rapid Remit lets you compare transfer services of your choice in the UAE region and then personally facilitates your transfer process from start to end without charging an extra
                                penny from you. With Rapid Remit, you can now use a different transfer company each time and not go through the hassle of creating an account for each service. Simply put, you can change your money transfer operator as your
                                needs change. The Rapid Remit portal is regularly updated with live exchange rates and complete information on all international money transfer operators. Also once you have decided your money transfer operator, you can complete
                                your international money transfer in less than a minute. In order to sign up for a one-time Rapid Remit account, provide your personal details here. Once registered, you will need all the information as transferring with a
                                bank (personal information of yourself and the receiver). Moreover, if you sign up for an account, you can also avail amazing promotions and discounts on your favorite transfer operators. With Rapid Remit, you can avail all
                                the benefits of the money transfer services and have the flexibility to shift your transfer service any time.
                            </p><a href="/blogs"><span class="spanHeading" style="color: rgb(249, 13, 38);">+Read More..</span></a></div>
                    </div>
                    <div style="display: flex; justify-content: center; background-color: rgb(242, 242, 242); margin-bottom: 50px; padding: 20px 0px;">
                        <div class="john-detail col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12"><span class="spanHeading">Blogs</span><span class="spanDate"><time datetime="1619923184970">2nd May,2021</time></span><br>
                            <p class="cardNewsTitle" style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; overflow: hidden;">5 Tips for Currency Exchange Online</p>
                            <p id="para2" style="display: -webkit-box; -webkit-line-clamp: 5; -webkit-box-orient: vertical; overflow: hidden;">If you are on an international trip or an expatriate working away from home, currency exchange is something that should bother you. You might be thinking to yourself ‘What’s there to currency exchange? You go to the airport or
                                a money transfer service near you and exchange your money. Sounds simple, doesn’t it? You will be surprised to know that currency exchange is something that can cause you a lot of pain if you don’t take it seriously, like the
                                flu. We compile five most important tips for getting the most out of your next currency exchange: 1. Choosing the right channel There are three popular options for currency exchange. You can choose from hawala, bank transfers
                                and money operators. While hawala agents are really cheap, I would seriously not advise you to use a hawala agent for your currency exchange. Most of them work off-the-grid and are extremely unreliable, unsafe and inconvenient.
                                For a full comparison between hawala and other formal means of remittance, read our detailed feature here (hyperlink blog titled ‘Formal vs Informal Remittance Channels here). The two formal options are bank transfers and money
                                transfer operators operating within UAE. Banks are extremely safe and reliable but they charge a hefty transaction fee and most banks take a few days to complete your transaction. Money transfer operators are becoming really
                                popular around the world and a majority of expats use this channel to send their remittances. They are safe, reliable and extremely convenient. However, deciding the best money transfer operator can be a bit of a challenge.
                                With Rapid Remit’s transfer comparison service, you can comprehensively compare all features of money transfer operators from your screen and pick the one best suited for you. 2. Always compare the amount you will receive at
                                the end of your transaction. In every country, you will find some transfer operators that will tell you they don’t have a transaction fee and it will automatically sound more appealing to use their services. Word of caution
                                for you: These transfer operators will give you a weaker exchange rate compared to other transfer operators. What they don’t charge in transaction fee, they will compensate for in the exchange rate they offer you. So they might
                                not have a transaction fee, but they will offer you a weaker exchange rate. It is best to look at the amount that will be received when you exchange, after accounting for all charges. With Rapid Remit’s transparent portal,
                                we display the exact amount that will be received by your family and friends back home, after accounting for all charges. So beware of these transfer gimmicks and always compare the amount received at the end of the transaction.
                                3. Choose the right time for exchanging your currency. Exchange rates have the tendency to fluctuate through the day. So, keep an eye on your desired exchange rates a few days before you actually have to exchange your money.
                                Remember, the more time you have before making your exchange, the greater chances that you will find a cheaper exchange rate. Some transfer operators and banks allow you to lock in rates and when your desired rate is met, your
                                currency will automatically be bought. One of the most unique features of Rapid Remit is that you can view live rates for your desired currency on our website. With exchange rates being updated every minute you can make sure
                                that you never miss out on a good exchange deal. 4. Never exchange on an airport. Even if you forgot to exchange your cash before your flight, converting cash at the airport is highly discouraged. You will end up paying 8-12%
                                more than the usual amount you pay with a transfer operator. This is because the exchange rate offered by exchange companies or banks at the airport is really high compared to what you will find otherwise. Don’t believe me?
                                The next time you take an international trip, inquire the rates at the airport and compare it with the exchange rate mentioned on Google. You will notice that there is a marked difference between the two rates. 5. Do bulk transactions.
                                If you are on a trip to Turkey for 10 days, you know you will spend a 1000 Liras, so why only exchange a 100 liras? You know you will need at least a 1000 liras and you should exchange this money in one or two transactions
                                rather than 10 transactions. Whether you want to send money to your home country or want to convert currency for your international trip, make sure to do it in one or two big transactions rather than multiple smaller transactions.
                                It’s more convenient and cheaper to exchange/remit more money at once. Remember, currency exchange done right can save you a ton of money especially if you are an expat sending money home every month. So think before you ex-change
                                and make the most out of your exchange deals. Also, Rapid Remit offers a ton of amazing deals and special discounts if you sign up for a free account here.
                            </p><a href="/blogs"><span class="spanHeading" style="color: rgb(249, 13, 38);">+Read More..</span></a></div>
                        <div class="col-xl-4 col-lg-4 d-lg-block d-md-none d-sm-none col-sm-12 d-none col-12">
                            <div class="pic1">
                                <div class="pic-i"></div>
                                <div class="pic-j"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/images%2Fmoney-transfer-scam.jpg?alt=media&amp;token=0df6b76b-c797-41d9-b16c-7e99c9451ae3" alt=""></div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('custom-script')
<script>
 $(document).ready(function(){
    $("#articlesb").click(function(){
        $("#articlesa").css("display", "block");
        $("#articlesTab").css("display", "block");
        $("#newsb").css("display", "block");
        $("#blogsb").css("display", "block");
        $("#newsa").css("display", "none");
        $("#newsTab").css("display", "none");
        $("#blogsa").css("display", "none");
        $("#blogsTab").css("display", "none");
        $("#articlesb").css("display", "none");
    });
    
    $("#newsb").click(function(){
        $("#newsa").css("display", "block");
        $("#newsTab").css("display", "block");
        $("#articlesb").css("display", "block");
        $("#blogsb").css("display", "block");
        $("#articlesa").css("display", "none");
        $("#articlesTab").css("display", "none");
        $("#blogsa").css("display", "none");
        $("#blogsTab").css("display", "none");
        $("#newsb").css("display", "none");
    });

    $("#blogsb").click(function(){
        $("#blogsa").css("display", "block");
        $("#blogsTab").css("display", "block");
        $("#articlesb").css("display", "block");
        $("#newsb").css("display", "block");
        $("#articlesa").css("display", "none");
        $("#articlesTab").css("display", "none");
        $("#newsa").css("display", "none");
        $("#newsTab").css("display", "none");
        $("#blogsb").css("display", "none");
    });	
});
</script>
@endpush