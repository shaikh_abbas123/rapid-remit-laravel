 
@extends('website.layouts.app2')

@section('content')

<script src="{{asset('website/js/home_custom.js')}}"></script>
           
            
            <div class="ReactDiv-main-div">
                <div class="ReactDiv">
                   
                    <div class="container-fluid whyChooseUs" id="whyCHooseUs">
                        <div class="dropdown2 whyChooseUsFistDropdown2">
                            <h1>01</h1>
                            <h2 class="wow tada">Why Choose Us</h2>
                        </div>
                        <div class="container1 wow animate__zoomIn" >
                            <div class="row whyChooseUs-row">
                                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    <h1 style="box-sizing: border-box; font-family: lato-regular; margin: 0px 0px 1rem; padding: 0px; color: rgb(122, 122, 122); font-size: 15px; background-color: rgb(255, 255, 255);"><span style="font-family: lato, sans-serif; font-size: 12pt;"><span style="color: red;">One-stop solution for your remittance needs</span>&nbsp;</span>
                                    </h1>
                                    <p style="box-sizing: border-box; font-family: lato-regular; margin: 0px 0px 1rem; padding: 0px; color: rgb(122, 122, 122); font-size: 15px; background-color: rgb(255, 255, 255);"><span style="font-family: lato, sans-serif; font-size: 12pt;"><br><span style="color: rgb(0, 0, 0);">Whether you want to <strong>transfer money online</strong> through one of our services, find the <strong>best currency exchange service</strong>, learn about fluctuations to your desired exchange rates or just want to stay updated with the UAE financial market, we have you covered.</span></span>
                                    </p>
                                    <h1 style="box-sizing: border-box; font-family: lato-regular; margin: 0px 0px 1rem; padding: 0px; color: rgb(122, 122, 122); font-size: 15px; background-color: rgb(255, 255, 255);">&nbsp;</h1>
                                    <h1 style="box-sizing: border-box; font-family: lato-regular; margin: 0px 0px 1rem; padding: 0px; color: rgb(122, 122, 122); font-size: 15px; background-color: rgb(255, 255, 255);"><span style="font-family: lato, sans-serif; font-size: 12pt;"><span style="color: red;">It’s completely free</span></span>
                                    </h1>
                                    <p style="box-sizing: border-box; font-family: lato-regular; margin: 0px 0px 1rem; padding: 0px; color: rgb(122, 122, 122); font-size: 15px; background-color: rgb(255, 255, 255);"><span style="font-family: lato, sans-serif; font-size: 12pt;"><br><span style="color: rgb(0, 0, 0);">Rapid Remit is completely free and does not make any money from its users or <strong>currency exchange services</strong>.</span></span>
                                    </p>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-12">
                                    <div class="divTextBackground">
                                        <div class="picDiv1">
                                            <div class="picDiva"></div>
                                            <div class="picDivb"><img src="{{asset('website/media/building1.8de86612.png')}}" alt=""></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-8 col-sm-7 col-12 whyChooseUs-div">
                                    <h1 class="MsoNormal"><span style="font-family: lato, sans-serif; font-size: 12pt;"><span style="color: red;">Transparent, Comprehensive and Updated</span>&nbsp;</span>
                                    </h1>
                                    <p class="MsoNormal"><span style="font-family: lato, sans-serif; font-size: 12pt;"><br></span><span style="font-family: lato, sans-serif; font-size: 12pt;">Rapid Remit is a transparent and comprehensive <strong>online money transfer</strong> platform, tailored to the needs of expatriates residing in UAE. With Rapid Remit, you can compare real-time updated information on the <strong>best</strong> <strong>currency exchange services </strong>in the UAE from the ease of your screen. </span></p>
                                    <h1 style="box-sizing: border-box; font-family: lato-regular; margin: 0px 0px 1rem; padding: 0px; color: rgb(122, 122, 122); font-size: 15px; background-color: rgb(255, 255, 255);"><span style="font-family: lato, sans-serif; font-size: 12pt;"> <span style="color: red;">Convenient and Easy</span></span>
                                    </h1>
                                    <p style="box-sizing: border-box; font-family: lato-regular; margin: 0px 0px 1rem; padding: 0px; color: rgb(122, 122, 122); font-size: 15px; background-color: rgb(255, 255, 255);"><span style="font-family: lato, sans-serif; font-size: 12pt;"><br><span style="color: rgb(0, 0, 0);">No more visits to your <strong>money transfer</strong> providers in Marina Mall or Dubai Mall. With a few clicks, we compare all your money transfer services options, allowing you to decide which remittance providers offer the best value for your money</span></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="img"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAABFCAYAAACoh90mAAAACXBIWXMAAAsSAAALEgHS3X78AAAAmElEQVRYhe3YsQ3CMBCF4T9Ecu3OlaVsQEbJBjACDJFBsolHYAdX7qB1YxoKFMWERCnfSdc83X26+iilsNZA+GfuxIElTJgwYcKECRMmTJgwYcKECRMmTNjxWDLeJuNvY2u7ZPx1DWs+/7FFCAjA+SueXI5V9NdlwwwCuCTj+z1YV8ntHiwsZC/gsRlzOQbgPoMGl+OztvMGKEVP4LwzlTMAAAAASUVORK5CYII="
                                alt=""></div>
                        <div class="dropdown2">
                            <h1>02</h1>
                            <h2 class="wow animate__rubberBand">Services</h2>
                        </div>
                    </div>
                    
                    <div class="container-fluid services">
                        <div class="container1 wow animate__zoomIn">
                            <div class="row"></div>
                            <div class="row rowServices">
                                <div class="col-xl-2 col-lg-2 col-md-2 d-md-block d-sm-none col-sm-0 col-12">
                                    <h1 class="react-reveal" style="animation-fill-mode: both; animation-duration: 1000ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-173020880468990-1;">Services</h1>
                                </div>
                                <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12" style="padding: 0px;">
                                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-controls"><a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next"
                                                href="#carouselExampleControls" role="button" data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Next</span></a></div>
                                        <div class="carousel-inner">
                                            @php
                                                $count = 0
                                            @endphp
                                            @foreach ($service as $item)
                                            @php
                                                $count++
                                            @endphp
                                            <div class="carousel-item <?= ($count == 1)?'active':''?>">
                                                <div class="row sliderRow2" style="background-image: url(&quot;undefined&quot;);">
                                                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-7 col-12">
                                                        <h2>{{Str::limit(@$item->heading, 9, $end='...')}}</h2><img class="img1" src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/images%2Fservice1.f1623fb9.jpg?alt=media&amp;token=622a9f00-d328-45e1-ba7f-e2b388e192d5"
                                                            alt="">
                                                        <a href="" class="remove_border">
                                                            <button class="button1 border-0" style="opacity: 0; height: 10px;">Read More</button>
                                                        </a>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-5 col-12">
                                                        <div class="Number01" style="color: rgb(249, 13, 38);"></div>
                                                        <h5 style="color: rgb(249, 13, 38);">{{ @$item->heading }}</h5>
                                                        <p>{{ @$item->description }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="img"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAABFCAYAAACoh90mAAAACXBIWXMAAAsSAAALEgHS3X78AAAAmElEQVRYhe3YsQ3CMBCF4T9Ecu3OlaVsQEbJBjACDJFBsolHYAdX7qB1YxoKFMWERCnfSdc83X26+iilsNZA+GfuxIElTJgwYcKECRMmTJgwYcKECRMmTNjxWDLeJuNvY2u7ZPx1DWs+/7FFCAjA+SueXI5V9NdlwwwCuCTj+z1YV8ntHiwsZC/gsRlzOQbgPoMGl+OztvMGKEVP4LwzlTMAAAAASUVORK5CYII="
                                alt=""></div>
                            <div class="dropdown2">
                                <h1>03</h1>
                                <h2 class="wow swing">Our Networks</h2>
                            </div>
                        </div>
                        <div class=" container-fluid partners wow animate__zoomIn" style="">
                            <div class="container1">
                                <div class="row rowPartners">
                                    <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12 img-col" style="padding: 0px;"><img class="img1" src="{{asset('website/media/partner-img.31fe7edd.jpg')}}" alt=""></div>
                                    <div class="col-xl-5 col-lg-6 col-md-6 col-sm-12 col-12 latest-news-paragraph-col">
                                        <p>Reach out to a highly valuable and growing remittance audience. Our service creates a marketplace for money transfer providers to connect for individuals and business users to access your service in real time through
                                            a convenient and user friendly tool. Our simple and user friendly integration tool make it easier for your company to market your services and reach new customers base.</p>
                                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">
                                                <div class="carousel-item active" style="text-align: center;">
                                                    <div class="diva"><img class="" src="{{asset('website/media/MoneyGram g.9be8885d.png')}}" alt=""></div>
                                                    <div class="diva"><img class="" src="{{asset('website/media/luluExchange.692e4253.png')}}" alt=""></div>
                                                    <div class="diva"><img class="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWgAAABkCAAAAACOO/XGAAAACXBIWXMAAAsTAAALEwEAmpwYAAAGu2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDggNzkuMTY0MDM2LCAyMDE5LzA4LzEzLTAxOjA2OjU3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgMjEuMCAoTWFjaW50b3NoKSIgeG1wOkNyZWF0ZURhdGU9IjIwMjAtMTEtMjBUMTg6NDkrMDU6MDAiIHhtcDpNb2RpZnlEYXRlPSIyMDIwLTEyLTAxVDE3OjU1OjM0KzA1OjAwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDIwLTEyLTAxVDE3OjU1OjM0KzA1OjAwIiBkYzpmb3JtYXQ9ImltYWdlL3BuZyIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMSIgcGhvdG9zaG9wOklDQ1Byb2ZpbGU9IkRvdCBHYWluIDIwJSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpmYmRlMWEzZi00ZGUyLTQxOWQtYWYzZi03ZGFmYzhmYTBjZGEiIHhtcE1NOkRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDpjZGUwYmE3YS1lZWFkLTU1NDQtYTZiZS02NGY2NGQxNWVkNzUiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo5M2IzOGMxZi01YzU2LTQwYjktYTg3MC0wYTJkZWE2ZGIyZjMiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjkzYjM4YzFmLTVjNTYtNDBiOS1hODcwLTBhMmRlYTZkYjJmMyIgc3RFdnQ6d2hlbj0iMjAyMC0xMS0yMFQxODo0OSswNTowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIxLjAgKE1hY2ludG9zaCkiLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmNiY2ExY2E3LWYzMjUtNGI4YS1iOTkyLWM2NWM1NzY2YzgwYiIgc3RFdnQ6d2hlbj0iMjAyMC0xMi0wMVQxNzo1NTozNCswNTowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIxLjAgKE1hY2ludG9zaCkiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOmZiZGUxYTNmLTRkZTItNDE5ZC1hZjNmLTdkYWZjOGZhMGNkYSIgc3RFdnQ6d2hlbj0iMjAyMC0xMi0wMVQxNzo1NTozNCswNTowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIxLjAgKE1hY2ludG9zaCkiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+WLH3wwAAH9dJREFUeJztfXl4ldW1/ru+4cw5ScjEkBCITEoFAQWlOFa04lyc6lgRbHur7XVoq7d6661o1arUamtbW1RutVZrEVCQocWhDCKjzGMgJGROzpzzDedb94/9fWcIBvT3kJCfup4HONnjWu9ee+211t4nEOMr6gmSjjcDXxb6Cugeoq+A7iH6Cugeot4OdGPieHNwjKiXA71ile94s3CMqHcDPXfOmcebhWNFvRro2b+5uuh483CsqDcD/etnLz3/ePNwzIh6bWRoPvjO2JeONxPHjnot0NF7Vpe+POB4c3HsqLeajl23fOz9/hcI594K9Io7DtB5U483F8eSeqfpeO1Znctf8R9vNo4lKcebgU+j381JyfI9XyiceyPQ5n8vVdzJa74woYqg3mc6wndvdCkdJ71Mx5uRY0u97jDcM32DR0n6fvYFw7nXAb3q+9U+KYVpw483I8eaepmNfvPXSQ+xdsZ3jjcjx5x6l42e9Rq7QB3Fc0qPNyfHnHqTRls/f1dyM0zlji8ezr0J6NC9G1WZAW3KJceblW6g3mM6qu+pcUkgdAx+xXW8eekG6jVex6rvHfBIAOvun3wRce41puONZzQPAFipG0873rx0C/US0/HsK5abAVBi1IvHm5fuod6h0Q+9LQuck4H7jjcv3US9Aejm/1rvkRjEZPLtX7iQ0KZeAPTen1a7JQYY0M+64Xhz013UgzZ6ZzTAIIDBVt/idPH7M9vdBBCDkmX/W9hj7PQw9aBGr3tedWbTKp4rsD/WPNzmAwAm1uUffWFx7kk/+voT2kLhUDgcDoU6ts1ySh9t94EAgGF+c3LPcdPTlDEdJoMBECit5QYTg5iY1Ow+luV0lnPGSnVlhux2h6a1esDEAJnWzy4DADz3sktkngmJqlcz0zSu2lSbkAP9x4wty9aF9NSQnGJOpWtzt6f56cVpLjPcdzlEYtfa3U2mVDhyUpU7pyKrRycSA3CD2TcHswzQ1rc/CRIDFD15tv2wsPnmRhfAQHzsnKwu1m1rggAxpWIPXZ091p+fDmaGzaygZdx5q/i0YCYpdnmy+MV+ANbdnVRtNgzlN+PsHtpfX9ob0TWG5HIH+l9x48D0FLu/GxH8xy57xC7a+MO4CCXbL34qm5vn/uQVc8UnvJBd/tcnVBkAEv3/5DxnqJne7AGAlPXja7Narvz9R62argOK21dy0fQRWVV7p4dzkbcpMmE2AKxdXG2Wf/2b2ZcXmRXcuEt01UxntZIbGt1gYug5mrvm9YQbAJi0F3KA3rPNzSAADAITwAQmGFa13eDS1e8qDADEnuaZvwW0X8a8dp2VusXB+bXHtqY8kqISA2Zb08fPTL2/wq4Krw+7iAnQqpxZW9fH3WAQNKeRoL9scgkutNr/HJlVvn8j3ABgffJfL9tF8fWtLmJig2uyxJz5XtQjkcsDhhWPPDV78v9koA6vj7gZxMLo2f8C0NwA8NK7109T9v9x1S8+DWjywi1EcESH5PUKjTa9WT2wIJFvGxll054hWRVueIStATGl0YaaSi/+/TtqPExgBjwf//k2PFHtERVMyVP+w5b6tre0gMwghgSQ7OLw88tmT7LZ9RgKgUFGmiXFkxIareVw+e+9HhEDQY3MzQba7VYUgIn0f0yx9Vf2etxggoszaZZHnm7z5gtNIVageDpeX/X8xZlJdVcaZDENGIDmAfCvRc/t/DsPeOKBP96emTZtADl3vzulDCLKub9L/sMFe2S59dVO7YU+g0moNoHBWb0D96oGmAFA4r/UrVykkjgJSe9zv2gSu/xvUlACAWJBCPDk7b7qX84EDFGYOylRpzL8pcUFIiKGnFqUI5I9Dquxh0NOEQPEjIz4Mx6IBN0QmxJgJnjyDt7wZueBxB8iQlbfN24o+YP31Lf++uDqaKZ1Gmhy+mV3ARE4twQfN6jMRAwmst7OFZjBREI8MRJTbuczrjIsMS67o3fPMlW72uQfDBUtrv9nngrH6hALsPMbb93isEaiLntUm8ts0j+22SbAt3lFTmPBE3Ng670OZvYA6cW6/08+PywbfghGpPzEd5fnTMJpdWJmgr3YTcYYuBAcEOqbvyrTNqPRtmS5+sL2eFn0bEi2tRDs3v9RJwEdIZgttiyGZTFbWfV3jtSdgT37D7ps8Vg/60pR/z8LfJJgHkY4qjMBYOJgzY+d4Tvrs5itUxHe3ppns85Qooty2wMAEWTPqwvFqGL3Z8Z97TmvS/zMqVgkaQlg2Nd6f46kBAIDlgUGLLbEntBdKjrW36b/FyQt01zK7scEOmxXdhIt+QnSG4Vczc93EkCYipRhGqZpmuJvzci0Ue8PGE53J35h0svtXNJHT3tdzASGHi69YkpfIwEiYpB/2SIxBYFwuEJTZ/AXapKtYgRy/70jh0vbSLCn476YPUTOSkUeirkYzERGxHf+1SdaceEfsO+jJ7KmZQDEumGahmkYpmmaBnSgPHEI+Y/cBdSGshyVjNdhWyjO2eviQM1m4+/7/WwXMBE+MTMjCE0kpHDr9e1ZY1jZZ9GJ058xlWzjxCBdusMOyR+L5FsgJiT9903vj/r1P18XJAagxF+4yOaSOut07q4HALS/77H5BxOU+lXndcJHnNXBzQ/OQsYgOZLP3BkQohg8457hiGx5dKFfIgCuxJs/cWQVR37o5N/Fsnas1hfAGa+M7Ze47uOVO/sNzdSkYbL5P0wMQg7ymKd5wCATKoORt23BlVkCk1gYPuUMdEk3frRSzjp2GbCMy+yQcNMK+9i31MdmAOh38YTJW30gBrwrD1YAtkfFORpMLFQ8i803D8lgYjIVIoYceeE8ZHcQWwMg14tXnC2MLFHaS4ssgPBnjeS9jwEITlxw4dIAMSHl2zz/slxxiiYdJuH0ex743gA8+Ov9M7MKsw9DG+cclphti29TzSofAzBKBmhMRJKWdRzaS8XMycMmz6L7y7QsUAiUHP6A/cP8VpcwBNGzZoiS4sdcJoGYpY6NEI4Rg4mttCOmSozDFGRJQmYweJAEgAhrM+c/O+rMYHjDd2uA5fjDosXbe/LAYHB87GM2k78pi+mGruupjuXpUYSF1g8XUJ5Z+LO77r0r9cvyrMLsjc+HH982B5mCfzT6wITEqSc+rAKA54NQQUYEW9XoiN9Z6/8fM81MBMTQA/c6XKywZDBAhu9Wp/6cih3O1JcKjpgBUkIHLJ2YuKzaIlAnBdm9WiEwWf4Zv4pIxPDXzr05SyjBJ4OA/PUPPgG3wuR4AgA+MiWBh3uK02XELzZ6wQTEJzhwiQWD51MkDNyzr9rqOyInBs+YDjimI+ecYeIcKealxJIM/O4fwh4myHVvzMhpD2LCu6Z9yMCybujfmY9LPnrHmxkylbrRCQlrdrmEDY0Pv8Cpdn9vaZAA5uhI2NYJALyrz2QwCJJuqsSdGF92MI+JWB9+0/IFQQYp8XlpoEVYwGQbRdcLF5zv91vZ8d0mVeiLGbwqPWJW6JHBiwG17g9JyzZn+hlnObVVVZ3bZyJDO2BhdLYe2WfhJztcBIblO2NA5VoPGHJ8cRbQ4kCR+e15ziCsjT0MaNx/cHt6tdmalB6gKawITlCSzprgRz/K5UYEY9QRFecRZJVA6RBJ0HxIAEgbUXTufAYxvGsOpgN0x4O1CAR4Qz87v9RnsuOrAaEaRaxBavARr3sIxOQ/9CPH7LD23bO6bp2drbKVGrlFObvyxYYgEyhVdj5OXM8EhvLR3hNyp2eQ5PQhS83N8AEAfBN3pEe1XJnnMnFNHP4WKrsUzgaVFZlEMJuJ9dO0dkUAYLLkSaj0mQpAatPcHzoi2SpFlFKYQIE1j/804Jh5AKhN2N66WSXUYf3itBBs5l2fb7MiDmA710lgRm6+LpeygSY+PAZ3ckQ2rRNbO3lqAa54A2CCr3b+XdmtGQBktjXvMOsJADi02LLZY8jJOWc7DOqWIjqoXX+PkzOZBZtZ29PKMh0LNQ+YkCi/ABefsDkfABlpoJ1kcHKksiYIhuKZdckA4boIXltTEBpqBUSHuTMz8YTV5/x8+yOBmImU9DnmOlJyP6dOeP6dsE5vKQCLPhYOJo8HxpcZYop/ZloyROzAjkt9WBQHAHiiOuuNjLr11+nPzJydC/gU4kxSI2v9xUGW/nFeCgzmVOlguEbBYjDUndvTbQVpFY/4DBAsV+NPTcdtIYiEdeYnAG74AoFAwO8P+P0eXyaLnZXLOczgHkYZoEXkjFxsKDeAWZJUCASjz7nAgFM6AAblvZcV0YszgmFZlsWWxWzlBOCC5qzwZLWW1LecpVJk28vsGmh7FZlBzgZkJkcZBS2qVgEidp0H4HQfEwie+t/ncgg0nv/tDgYIgZXv54msFDMAmdKZBBsjlSSJSJZJkkhytpGDj03WES4DgE65Dru7U0YOzI5BSSx2MzOQGHkqgNMgvIzk4sxwLFJpWjQajUai0UgkljTRibbNyb2ZUZPPhsQnr2oJm2PUdsEu224wJDYM0zTMlG7aFjWD9MJ2lQhIBa4FcFF+SmjPWkcqZ6MZeHpwnBgkG1EpC7wSybYuSpMzIrGd+cgwYq8ypaKRSCQSjUQj0VQMXVPaRkvkOBjpK1RJspM4bBvRt2sU4WyOAIBLnmv1AADPfyjDEIGJefzXnJCFzUGdp3yq3ZfrQXpqZj4JACjKa1KFxh7K1C/8MEAgRqxquiMqMbGs2HcLnN69NrUtFckYq3QEgBNOWeICwP5Niy90eBJrZXDwke9oLgAyp8sBlAci4sBVtosYwTRIJjU7RgfsBWO97CbdvvCzkke68swchi4bVSUaKhMlsagsmJJKRMH8eBAMMoLj9Ca4yvvXewgg175FF2UhTbBw8/SuZ/ztJndOqoMhuT/827UAMLTyoLhUVBqrBzstHvrY/jBqejrDRqxN+jkbAFC8/OGkkmPi1u73EwAYY4ywKRWNXMIEkBJe6ABtu8xmh+/bb7+qUtqltilYWeMiArFcv+wqAAgWudxWCDkRBZFwKJODs9NqR6AM0G47Neau22kDvSLiEvtDFg5cw0qh9rLy5BNgKCGRiVTD7wigGSJjwPwpcalDK15V5eysFQBWkn8+rQoATv23RQDIc2Du3XaD1bs8KgDmpEgq2aLq/Z2vxyVkS2R4na0/R/MwAfCuGGeBlQ6fyL+4/xX3Z4FESCV9eHRVnde5gHbSahi7XEwkR167CgBuuUge8pt7vZyt02yr8aeF4J9KmcOwyk4gKrF34HBsuxws0n3zq/0AGJJZd+hQfUOtJoHBJGNJawY3YhB17VBGZumynfxlwzHf7G79JQDggnyDALCi/uGAXfdI1E0SkYy8q+wZAGaidKpXs3J9/bb3vOKAlFtr6+vrD7aL9Dkp+0RaJn2zzwxUPmBflgv9FMNMdLOwDN7FrwJA4fAhTa+RfdOUjhBAwhM9OsYAsjX6pnmGwgSw7w9ldwNoe3C1z94rkrgZXArJzieq9skpTv28faudyzRRRvWR5nQUwWQVF6RnebLaDWEwoJ+a3Oy1FcS14YUZAC4ats5NIFi+vZc9ORlA472L/QIAbdho2LkUzhbYdmjTlzp4IyTZvCkyOVdRBEjRd661WRSrRQRg2ty3RR4WmfzfpcM25wNMcCfv2PfDIMALf7o1v3OMYVuceFs0fdwTp/xd/aKADNDjA83iB1V/4JXTAns+qVPtK+vouIkAsHlJgInFQgpf1zZuUuqvFztCM4EkPPfHdOqBkDLvd+wAFi1xybYnkyx8vPpOzX5soFh/GT8awO0zLJHQ8W+bOma0f/faQ3YUwOYMFbCTHZzl2tugZXIdb8SD4qOdi3WWW8LKlmLRITtV9vSmujw7uHSGVK/ebCogJnZ3PDxnfHls3dZkUDjq2VgziClv7yhnH4NBkdOXHBXovAtmi9tIdvGW9YDXazNkWVe5AWBZ0iPUyEoRhObaiWXfssYyZCtZIp41haWl3Z6GZy2XzaupfDc4+toXZVlkJF3xx1/wA9MXzg2Ku4aAufIDwOu10xiR0beKoW2hMraC4FwiEgDs2KmKbcW2ohFLEjExBfbOnZHukh5i6E/vSSlWOsgEADy4cHW+sMBuPrgbUD0+PiyLbLvCZrvNFgDiRKgLnLMjwwf7RSR7J3rz8/NdABjMiI66EwDwmkEivWhJRCSBiC0ARCxF34FobbvdsqKoqqIoqqKqquJJT/JIo8sOuVg/dyrwg1FJOzXJrh3PAMBzIyKiwJL9+cF8VXhCFC983AMHns4RDdv5TQaAt2o9YIDIIpIkIgJZQltlvJfpkrll/MHkmEVMlB0B/7pv3J6HXMFgUOQambNtlnMbL6mqqqiKIv7q+i1jVsWgp6eF8xnsJGqE+iBcMssLACv2upjAZHh+fEaIQWAP/2xrAGBIsTnTkAn/07fU4lNG1eesdom1YCQHPgAAD3y/zX70IbvmTzwH6P/ytfsDMoQVEmaTgJjnibQTnBVAZSHtqKj1DkkAA8aU2xLC0gVeeUMFgeFfuPEUu312+DtrfX3QDqid0gm/vzkctPEU2ReydEXNLCdgR4+Ulo8OY6sroHFd809CAcVZa7FNUrHALHENNLc1j5iAROV96U7LN4tFVnbvHG4Hrc5rAHL4zMy+8yVJtQRMpu8uHwBUTZtl2UZYTT71tWJg/NxpGyRhNZ2uZrzgqWk2U2I75CYWKMu/3bLNLxjQr0s79xULDAkgVsLvCaBzM11D7v5xyr7YT/N6+Z9+0OxTmUC2I5eKjSz+WMlotJ3UtqEn7mTPDqecpNKdL56YDBmWZQm1SxlG2Bw/V7wNj8yDqRm6pmFQZnHO8Ud13dB15dArAAzohqHphq7puqFrmqYbhq7reodtLX8Zdlk2+MYVdur22rM0wzAM0zQN6eB/A8ApH94/IBwzLPumOmWEzUlv2zjDSiZ0Xdc1zUr7r5beoeuapunQAPy2zdR1XdMjBaPTXA6siAlO+GUGdC2pGbquIelkYe6ZHNM0Xdc0rSPtQFy99EKEDZOZAbbMSPKC1ecmdF3v0CzBR4em26Tpum5ouq5ruoasBwa5lGtTrpvy5l+3RnW7tccVHHvLlfYybTWrZBDDpGmZ9meest/PACiyE0Bp3zzbzpG9FQGAU6ZIej65xWOvPSdO+ZEzxI8b65ynXMae5ecC8D96x+8X1MXjDACKp+DcaVem1cFbWeACE1Ms7Ub5KmIiExAqB1DfL49B4MgNmRto/5SmPALAHfrmUSiuENnlaGVa9qeujbsAkMmZ9Ozod5f+bm1YvDNwBU6ccTsNLPZIpJWKG7zKkNuxFM5BSAwKZ95jdqLDH4Ht37xme9yyJCl/5PiTM15hvNXNDJAll2btkNYORRz4ZqkLsbDM6Wmzhud8P4BVd7Nql8ZHzOqbHiKSFNaNWEp47FgfyZ2rVjWaKUmuPGfi4Mx0MFtE6AjLV2AXaW0sMTGx5SsE6i2ZAYJZmPULbDraJGEuzPw8JEISE4MspTi9fqGYLBy8gpz3ewf/vazGshTf6CljXUCsHgSWy10OH+nDk5xzg1LuYnw69dhXK8LTDojohEzj1Gc+9c3rF5p67MX/k/s9IvQ2jMm//fLh3GPfYVmyWCQF2Exd85MemrNXUQ+ZjoPTwvY3NpUZ047a+otIPaTRD7f6GQA0z91XHq2toXmOy7cfzaT7s6bi/h+oZ0R6fr2biYGO/F8c/lQtlzavbTVdQ84/4lunrYsHd71cDQukbxV+fhYPrjhkeAadl/f5e3426hGgd70iKcxAR+Gvxhyl6bIFEZWwo/2WIzX69/tHuAfduLTySBFaF7T6rdYUKdsOzeguQHoE6MeTXgZYK/nl0XDesRATTkhsqd1eW951I6vsG6O7rvVPPCHYdW0XtPXNaMVw2t20de3pn7vvZ6OeAPqZTW4GWcmqx044WtP3o5OvASpfCDeWA9FDiT4VEtDRlu/ZHcyPuAtkaK1U5Pr66cUAkocigQo3ADQ0c/90oDByUKEExOqS7opMzGIeDLvKgwBC8UA+zHazj9todRU0RIYoABB6OzT85lJUP1+7+XS0aIU+JCJqgRwLF3l2eQZGwoWeHSVlSB1qdQ/0A6kWubilzjPYBQDNDdy3FNxi5OUB8VCXAUsPAL3hdYlAKX3Ur7piIk1tjVQKYMhUbQCwYUmd5DltKrBo9dnJ5Td0LCqb1gcfLKy6IzzbuL0UNQv2puQTr8uDsXRlWA1OGS+G0F/fff047Jx/AGrltf3scWve3WVI/a4YBryx4arzsOdv2vcGbvr7iEGL7dd1K/f1ubwUGHxqoBQH/2x8ZyiW/3PkLVj2z6n6W98e+Pr286If3FTW9tZ2Xel3fX/sfqVk5Icheey3JZhLV0YpePF4+seOiVcj+dLeCy/8VMF6AmjtEc3LMPVJj3mP2tY03P+ODqsInAdg4+zUIK3l/fIJse3m6kZv//qYYiC2JvI12llTrKL+xbpSX8tHfabi78uKq5rr3xkqTsDmvUYADS8lTlb2rJVFIh31f6zvW9S+5x//6QnV6vlAXX3ffOxpr/6kfQIBQHi9fvJQAJjKMppqyouB2sZxCur099rMCrMGK5t85XhhT7+S5t1v3459oURN0B9fN+YkLHjX3S/RMn9gXyvcCmzdWnhyV6J1P9CP7PcwTPPShz5D29Kq1Q0NHxYPGj8IkbnW5ReGZ2/7ZEIsxnxxeaWZZ5lYe3DERFTrfQux7MDJ15Q+u34H9rzX79YTtrzSuk88/m0IV1ZgVd3Zt2H5O0pKBgDrjcZxV5QuX1i75dSG9v7lQLt+Uj7CSuzkKqHQrS1BkVCRAXSk+hfCTKh90dLm1s8uH3QoaSqXV1S8vmf0TfnvLqppLmlj7+VjVr/FCWxf6bv4vDX/27inb9CTtKQNyfGHP521qduBXrBEBnS64a7P1PpK7462aOLAlu9UrW8qH2m6ig0NMU2+chxQ4I4Dm/Qz3QjrJdi/0X/hAHyjcoDxXmpohV6a12znTVsMnw8HAw2Lh5x6mp12/mhPyWX9MXp1TRytsYF9gPZUGcy4NtH55SCtiQHD0iwcUgqA9lB+XzS245KJQChWcMMwNG9VR/pTZZJhIqpPOA0VpJRidfvE8zE6ZFTAT0lp//bSrr9S0t1AN/3G9LKu3nndZ2tecF1rbfWWcPO/qra5os8z6bIHTdqQUwB4XGHvni0njYcZk0pR2zLmJGD0aNTuKdrxECjqsT3vJsoDhu6oPRAs/rr99GOHXlkOsEVuNFgeFe1NbjdaWoPO433UkC+dH03s8xYBDc19+yEeHjYOQLU5cBiwri2wZBlShs+vt6pDgIZkeZlWp4wGvN8EUCCDN7Re3vVvRuxuoGe2eaH57rvo6C0BdOxTK4uKRk+aHbIQSZX3SSpuPgnVZj8ZgOpWDn0iTZTQ0lZQjnrJPlmjum8IWarqEQ/NtRbvIOCivDUt0QPNpcIyJFKDADS3BkrQ5O4HNDTnF6OhZVA6VWuSqXkBa9muqimhVm9/oDmR1wftZqkbsPYqRQBCyf4VJnvkguC2trx8oM4sdW8LBQoBgAklirpv3YAjRGPdDPTs1W50FP1iwtFbAsCh37tvORnwScqJmqmdMQ5WwihEU6oIABSPtHHDieOBxpby/mhVFKB2rnqzbKjXeZCKyiKmi7cEhmB7fPSk6OqlWrMA2oQGYFtk7AmJZt8goD42vBKNmi/gzNtfatk8Htj4TvxrCBuBvuA9sheo9vQB0BGVygGktLIbgWSHH3WxIYVAo1mE1qTPBSzYOuFcFLuSq9suLOksUYa6F+gdcyAlBjw99OgtAQCyFH+vwNf+wb6yMW63vHUYL1h30vRoHIUAIJXs266foQBNSb8HBVzT4Jn34TifW45/dOa+efVTxM1mS0egEPP3jL8lr9xU7SsDn/LJ0H4rVxSciXgH7xi+Yamrj4omKRPll7v1ZVyy973Y189BHPqWMUt25PVHS623GICWVPoBcLtqdw5b+S7PqAwni70IR7kI+R5tW9nqRYnJgDcvtL504hFk616gn4i5O4Y+WXH0hoLKh2zd/Yza0ZF3aQAj9q3bZTUVnYO6Zp9QlBItdtJ4ALWGD6hy1T5jtX/tZvQt3zvvvWj7WPsY2h0tdGPI7s1Pueq0C+37oXHbG16Qo9JlY+D1RdeulxQziGSdlLl1Gjh8U8OrSlIfdS3gt8x58yWioQiF1DIA+9sKgwBGramf7WvWLqnEIS4CmhtdRTixdN/iD9r56nGAy9+inXkEhYb80OdC7vPRr/7p0sf/9qhhSpqkyngyxflVU0cBlcmkIY+6qRK1B0ecrgAA1xSdOwAwd8inVaJvKpxyn3RNfyiVrWYyeO7V9tv2XfGTT8YQsyMSK5l8gZ3z6OeOsVox5VzApdYrxd8Imqf279je5+z89LxVSZ3VkolTA0CfeLur6pvJvEmeproBX3cBu1qGnyIDRf6YqQ/41nmIbfedXobGA4MnuuWikI6yb04GoGyrLr36SJmw7sxHf3C/jnMe/VypR6u+XSkqkgCA20OBEgIM036wbZmkArB0ScS+kWAfGQDM5mRROruhsUsC0B6isqz4KN7iKhGDtMRK/Aa7YOmUc83T2qj2FVY+1WD1U3RSYZqSC4CesufvaOZ+LiBlyGq6NNkglXgBwPrTmguuOZJk3Qh05KYD6tVfjtsUrt2wPP+OI27dbrTRj+/33PyD7hu+N1HDs03ebx3ZRHYf0G8t8t11xM30BaJEftGYI7kc6EbTUXeV9cTZ3TR2ryPTUI+msd0G9I37nx7fTUP/f0ndZToebZlz2PfOv9TUTQ9olu59+iucc6h7NDq0677PGnZ/Wah7bPQBtcsE+JeVugdoq9f8Zxi9hnrsNemXnb5SvR6ir4DuIfoK6B6i/wNotwAE0YEEiwAAAABJRU5ErkJggg=="
                                                            alt=""></div>
                                                    <div class="diva"><img class="" src="{{asset('website/media/ExpressMoney.1e8175d6.png')}}" alt=""></div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="/ournetwork">
                                            <div class="btn-see-more">SEE MORE</div>
                                        </a>
                                    </div>
                                    <div class="col-xl-2 col-lg-1 d-lg-block d-md-none d-sn-none d-none">
                                        <h1>Partners</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid latestNewsUpdates">
                            <div class="container1">
                            <div class="img"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAABFCAYAAACoh90mAAAACXBIWXMAAAsSAAALEgHS3X78AAAAmElEQVRYhe3YsQ3CMBCF4T9Ecu3OlaVsQEbJBjACDJFBsolHYAdX7qB1YxoKFMWERCnfSdc83X26+iilsNZA+GfuxIElTJgwYcKECRMmTJgwYcKECRMmTNjxWDLeJuNvY2u7ZPx1DWs+/7FFCAjA+SueXI5V9NdlwwwCuCTj+z1YV8ntHiwsZC/gsRlzOQbgPoMGl+OztvMGKEVP4LwzlTMAAAAASUVORK5CYII="
                                alt=""></div>
                                <div class="dropdown2" style="margin-bottom: -200px;">
                                    <h1>04</h1>
                                    <h2 class="wow animate__jello" >Latest News</h2><span class="blackHeading"><h2 class="wow animate__jello">&amp; Updates</h2></span></div>
                                <div class="row rowLatestNewsUpdates">
                                    <div class="col-xl-2 col-lg-2 d-lg-block d-md-none d-sm-none d-none">
                                        <h3>News&amp;Updates</h3>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" style="margin-top: 200px;">
                                        <div class="react-reveal latestNewsUpdatesRedDiv wow animate__zoomIn" >
                                            <div class="carousel-root" tabindex="0">
                                                <div class="carousel carousel-slider" style="width: 100%;">
                                                    <button type="button" aria-label="previous slide / item" class="control-arrow control-prev control-disabled"></button>
                                                    <div class="slider-wrapper axis-horizontal">
                                                        <ul class="slider animated" style="transform: translate3d(0px, 0px, 0px); transition-duration: 350ms;">
                                                            @php
                                                                $count = 0
                                                            @endphp
                                                            @foreach ($news as $item)
                                                            @php
                                                                $count++
                                                            @endphp
                                                            <li class="slide <?= ($count == 1)?'selected':''?> ">
                                                                <div class="">
                                                                    <div class="news1">
                                                                        <h1>{{ $item->heading }}</h1>
                                                                        <div class="news1Line-div">
                                                                            <div class="news1Line"></div>
                                                                        </div>
                                                                        <p>{{ $item->news }}</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <button type="button" aria-label="next slide / item" class="control-arrow control-next control-disabled"></button>
                                                </div>
                                            </div>
                                            <div class="bottom-part"></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 d-lg-block d-md-none d-sm-none d-none">
                                        <div class="latestNewsUpdatesPicture-Div"><img src="{{asset('website/media/latestNewsUpdatesPicture.8c92e4a4.png')}}" alt=""></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid testimonial ">
                            <div class="container1">
                                <div class="row rowTestimonial1 wow animate__zoomIn">
                                    <div class="col-xl-4 col-lg-4 col-md-2 col-sm-1 col-12"></div>
                                    <div class="col-xl-4 col-lg-4 col-md-8 col-sm-10 col-12">
                                        <div class="img"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAABFCAYAAACoh90mAAAACXBIWXMAAAsSAAALEgHS3X78AAAAmElEQVRYhe3YsQ3CMBCF4T9Ecu3OlaVsQEbJBjACDJFBsolHYAdX7qB1YxoKFMWERCnfSdc83X26+iilsNZA+GfuxIElTJgwYcKECRMmTJgwYcKECRMmTNjxWDLeJuNvY2u7ZPx1DWs+/7FFCAjA+SueXI5V9NdlwwwCuCTj+z1YV8ntHiwsZC/gsRlzOQbgPoMGl+OztvMGKEVP4LwzlTMAAAAASUVORK5CYII="
                                                alt=""></div>
                                        <div class="dropdown2">
                                            <h1>05</h1>
                                            <h2 class="wow tada">Testimonial</h2>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12"></div>
                                </div>
                                <div class="react-reveal row rowTestimonal2 wow animate__zoomIn">
                                    <div class="row1">
                                        <div class="diva">
                                            <div class="box1" id="man1" style="background-image: url({{asset('website/media/1.jpg')}});">
                                                <div class="box1a">
                                                    <h3>Head</h3>
                                                    <p></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divb">
                                            <div class="divb1">
                                                <div class="divb1a">
                                                    <div class="box2" id="man2" style="background-image: url({{asset('website/media/2.jpg')}});">
                                                        <div class="box2a">
                                                            <h3>head</h3>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="divb1b">
                                                    <div class="box2" id="man3" style="background-image: url({{asset('website/media/3.png')}});">
                                                        <div class="box2a">
                                                            <h3>head</h3>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divb2">
                                                <div class="divb2a">
                                                    <div class="box2" id="man4" style="background-image: url({{asset('website/media/4.png')}});">
                                                        <div class="box2a">
                                                            <h3>head</h3>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="divb2b">
                                                    <div class="box2" id="man5" style="background-image: url({{asset('website/media/5.png')}});">
                                                        <div class="box2a">
                                                            <h3>head</h3>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="react-reveal row rowTestimonal3 wow animate__zoomIn">
                                    <div class="row1">
                                        <div class="diva">
                                            <div class="box3" id="man6" style="background-image: url({{asset('website/media/6.png')}});">
                                                <div class="box3a">
                                                    <h3>head</h3>
                                                    <p></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divb">
                                            <div class="divb1 ">
                                                <div class="divb1a ">
                                                    <div class="box2" id="man7" style="background-image: url({{asset('website/media/7.png')}});">
                                                        <div class="box2a">
                                                            <h3>head</h3>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="divb1b ">
                                                    <div class="box2" id="man11" style="background-image: url({{asset('website/media/11.pn')}}g);">
                                                        <div class="box2a">
                                                            <h3>head</h3>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divb2">
                                                <div class="divb2a ">
                                                    <div class="box2" id="man8" style="background-image: url({{asset('website/media/8.png')}});">
                                                        <div class="box2a">
                                                            <h3>head</h3>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="divb2b ">
                                                    <div class="box2" id="man12" style="background-image: url({{asset('website/media/12.pn')}}g);">
                                                        <div class="box2a">
                                                            <h3>head</h3>
                                                            <p></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divc">
                                            <div class="divc1 ">
                                                <div class="box2" id="man9" style="background-image: url({{asset('website/media/9.png')}});">
                                                    <div class="box2a">
                                                        <h3>head</h3>
                                                        <p> </p>
                                                    </div>
                                                </div>
                                                <div class="box2" id="man10" style="background-image: url({{asset('website/media/10.pn')}});">
                                                    <div class="box2a">
                                                        <h3>head</h3>
                                                        <p></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divc2">
                                                <div class="testimonialRedLine"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
 
@endsection

@push('custom-script')
<script>
 $('body').addClass("home_body") ;
</script>
@endpush
@push('custom-css')
<style>


</style>
@endpush