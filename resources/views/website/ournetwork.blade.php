@extends('website.layouts.app')
@section('content')
<div class="LiveRate">
    <div class="our-network">
        <div class="container1">
            <div class="row">
                <div class="col-x-12 col-lg-12 col-md-12 col-sm-12 col-12"><h1 class="first-heading Lato_Black">Best Money Transfer Companies</h1></div>
                <div class="col-x-12 col-lg-12 col-md-12 col-sm-12 col-12"><img src="{{asset('website/media/dum.cb717a84.png')}}" class="buildings" alt="" /></div>
                <div class="divRedWhiteLine"></div>
            </div>
        </div>
        <div id="ournetwork-companies" class="container1 rate-boxes">
            <div class="row">
                <div class="col-x-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <br class="react-reveal mi-br"/>
                    <h1 class="react-reveal second-heading Lato_Black mi-second-headng">
                        <span class="span-1 wow animate__flipInY1">
                            B
                        </span>
                        <span class="span-2 wow animate__flipInY2">
                            e
                        </span>
                        <span class="span-3 wow animate__flipInY3">
                            s
                        </span>
                        <span class="span-4 wow animate__flipInY4">
                            t
                        </span>
                        <span class="span-5 wow animate__flipInY5">
                        </span>
                        <span class="span-6 wow animate__flipInY6">
                            M
                        </span>
                        <span class="span-7  wow animate__flipInY7">
                            o
                        </span>
                        <span class="span-8 wow animate__flipInY8">
                            n
                        </span>
                        <span class="span-9 wow animate__flipInY9">
                            e
                        </span>
                        <span class="span-10 wow animate__flipInY10">
                            y
                        </span>
                        <span class="span-11 wow animate__flipInY11">
                        </span>
                        <span class="span-12 wow animate__flipInY12">
                            T
                        </span>
                        <span class="span-13 wow animate__flipInY13">
                            r
                        </span>
                        <span class="span-14 wow animate__flipInY14">
                            a
                        </span>
                        <span class="span-15 wow animate__flipInY15">
                            n
                        </span>
                        <span class="span-16 wow animate__flipInY16">
                            s
                        </span>
                        <span class="span-17 wow animate__flipInY17">
                            f
                        </span>
                        <span class="span-18 wow animate__flipInY18">
                            e
                        </span>
                        <span class="span-19 wow animate__flipInY19">
                            r
                        </span>
                        <span class="span-20 wow animate__flipInY20">
                        </span>
                        <span class="span-21 wow animate__flipInY21">
                            C
                        </span>
                        <span class="span-22 wow animate__flipInY22">
                            o
                        </span>
                        <span class="span-23 wow animate__flipInY23">
                            m
                        </span>
                        <span class="span-24 wow animate__flipInY24">
                            p
                        </span>
                        <span class="span-25 wow animate__flipInY25">
                            a
                        </span>
                        <span class="span-26 wow animate__flipInY26">
                            n
                        </span>
                        <span class="span-27 wow animate__flipInY27">
                            i
                        </span>
                        <span class="span-28 wow animate__flipInY28">
                            e
                        </span>
                        <span class="span-29 wow animate__flipInY29">
                            s
                        </span>
                    </h1>
                    <p id="para1" class="react-reveal" style="opacity: 1;">Our network of money transfer services is licensed and government regulated..</p>
                    <center class="react-reveal" style="opacity: 1;">
                        <p class="ournetwork-para2 wow flipInY">
                            Find the best money transfer service for you in the UAE by filtering your results for cheapest, fastest or most reliable transfer services.
                        </p>
                    </center>
                </div>
                <div style="display: flex; justify-content: flex-end; width: 100%;">
                    <div id="comparision-list" class="container3" style="width: 95%;">
                        <div class="row no-gutters comparision-form-row2" style="background: white; padding: 0px;">
                            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-5 ml-auto">
                                <form>
                                    <select name="result-dropdown" placeholder="Result per Page" class="dropdownsC cal-dropdown1 center-label1 bg-white dropdownInput">
                                        <option class="opt" value="6" selected="">6</option>
                                        <option class="opt" value="9">9</option>
                                        <option class="opt" value="12">12</option>
                                        <option class="opt" value="15">15</option>
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="our-network-rating-cards-main-div">
                    <div style="margin: 5px;">
                        <div class="column_column">
                            <div class="div-img"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-41516.appspot.com/o/images%2FMoneyGram.png?alt=media&amp;token=657d761e-e84e-43d6-957e-5cd33cb5d060" alt="" /></div>
                            <br />
                            <br />
                            <br />
                            <i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i>
                            <div class="rating-card-bottom">
                                <div>
                                    <div class="review-div"><span class="review">REVIEWS</span><span class="three-twenty">1</span></div>
                                </div>
                                <div class="two-buttons">
                                    <div class="two-btn-div"></div>
                                    <a href=""><p class="btn-see-details">SEE DETAILS</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin: 5px;">
                        <div class="column_column">
                            <div class="div-img"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-41516.appspot.com/o/images%2FExpressMoney.png?alt=media&amp;token=84aaee2a-f6e3-4284-9003-6d136a7288e7" alt="" /></div>
                            <br />
                            <br />
                            <br />
                            <i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star gray"></i>
                            <div class="rating-card-bottom">
                                <div>
                                    <div class="review-div"><span class="review">REVIEWS</span><span class="three-twenty">1</span></div>
                                </div>
                                <div class="two-buttons">
                                    <div class="two-btn-div"></div>
                                    <a href="/xpressmoney"><p class="btn-see-details">SEE DETAILS</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin: 5px;">
                        <div class="column_column">
                            <div class="div-img"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-e8df5.appspot.com/o/images%2FAlansair.png?alt=media&amp;token=c6a9578b-8a88-4f39-8029-1855a4515f8c" alt="" /></div>
                            <br />
                            <br />
                            <br />
                            <i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i>
                            <div class="rating-card-bottom">
                                <div>
                                    <div class="review-div"><span class="review">REVIEWS</span><span class="three-twenty">1</span></div>
                                </div>
                                <div class="two-buttons">
                                    <div class="two-btn-div"></div>
                                    <a href="/partner/5e956db273e97f46a8fc0063"><p class="btn-see-details">SEE DETAILS</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin: 5px;">
                        <div class="column_column">
                            <div class="div-img"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-41516.appspot.com/o/images%2FluluExchange.png?alt=media&amp;token=b34ffcd1-f2cf-4600-8b53-ccf1c3d9822f" alt="" /></div>
                            <br />
                            <br />
                            <br />
                            <i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i>
                            <div class="rating-card-bottom">
                                <div>
                                    <div class="review-div"><span class="review">REVIEWS</span><span class="three-twenty">1</span></div>
                                </div>
                                <div class="two-buttons">
                                    <div class="two-btn-div"></div>
                                    <a href="/partner/5fb17f474086ed2618b6eb60"><p class="btn-see-details">SEE DETAILS</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin: 5px;">
                        <div class="column_column">
                            <div class="div-img"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-41516.appspot.com/o/images%2Fsharaf.png?alt=media&amp;token=b32f1e2c-7e7d-4d3b-ba68-27b2f46f2fe4" alt="" /></div>
                            <br />
                            <br />
                            <br />
                            <i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star red"></i><i class="fas fa-star gray"></i>
                            <div class="rating-card-bottom">
                                <div>
                                    <div class="review-div"><span class="review">REVIEWS</span><span class="three-twenty">1</span></div>
                                </div>
                                <div class="two-buttons">
                                    <div class="two-btn-div"></div>
                                    <a href="/partner/5fb17f724086ed2618b6eb61"><p class="btn-see-details">SEE DETAILS</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin: 5px;">
                        <div class="column_column">
                            <div class="div-img"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-41516.appspot.com/o/images%2FAlfardan.png?alt=media&amp;token=27d99596-2c85-485c-8c79-0dff3f6d0730" alt="" /></div>
                            <br />
                            <br />
                            <br />
                            <i class="fas fa-star gray"></i><i class="fas fa-star gray"></i><i class="fas fa-star gray"></i><i class="fas fa-star gray"></i><i class="fas fa-star gray"></i>
                            <div class="rating-card-bottom">
                                <div>
                                    <div class="review-div"><span class="review">REVIEWS</span><span class="three-twenty">0</span></div>
                                </div>
                                <div class="two-buttons">
                                    <div class="two-btn-div"></div>
                                    <a href="/partner/5fb17fac4086ed2618b6eb62"><p class="btn-see-details">SEE DETAILS</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container3" style="width: 97%;">
                    <div style="width: 100%;">
                        <ul id="paginationUl" class="pagination justify-content-end">
                            <a href="#ournetwork-companies" class="anchor-decotation">
                                <li id="pagination-li-0" class="page-item" ><p class="page-link">1</p></li>
                            </a>
                            <a href="#ournetwork-companies" class="anchor-decotation">
                                <li id="pagination-li-1" class="page-item" ><p class="page-link">2</p></li>
                            </a>
                            <a href="#ournetwork-companies" class="anchor-decotation">
                                <li id="pagination-li-2" class="page-item" ><p class="page-link">3</p></li>
                            </a>
                            <a href="#ournetwork-companies" class="anchor-decotation">
                                <li id="pagination-li-3" class="page-item" ><p class="page-link">4</p></li>
                            </a>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
