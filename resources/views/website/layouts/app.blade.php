<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('website.partials.style')
   
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />



</head>

<body>
    
    <style>
        html {
            box-sizing: border-box
        }
        
        *,
         :after,
         :before {
            box-sizing: inherit
        }
        
        body {
            font-family: "Droid Sans", sans-serif;
            font-size: 14px;
            line-height: 21px
        }
        
        .container {
            width: 960px;
            margin: 20px auto
        }
        
        @media only screen and (min-width:768px) and (max-width:1000px) {
            .container {
                width: 768px
            }
        }
        
        @media only screen and (max-width:767px) {
            .container {
                width: 420px
            }
        }
        
        @media only screen and (max-width:480px) {
            .container {
                width: 300px
            }
        }
        
        a img {
            border: none
        }
        
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-weight: 400
        }
        
        h1 {
            font-size: 26px;
            line-height: 32px
        }
        
        p,
        ul {
            margin-bottom: 10px
        }


    </style>

  
    
        <div class="div1" id="navigationGray">
        <header id="navigationIndex" class="header-main">
            <div class="navFirstMainDiv" id="navFirstMainDiv">
                <div class="navFirst" style="border-bottom: 0px;">
                    <div class="divLeft">
                        <div class="bs-example"></div><a href="/forexcalculator" class="supportBtn remove_border"><span class="span2" style="color: white;">Eng &nbsp;<i class="fa fa-angle-down"></i></span></a></div>
                    <div class="divRight">
                    <!-- <a href="#"> -->
                    @if (Route::has('login'))
                        <div class="top-right links">
                            @auth
                                <span><a href="{{ url('/logout') }}">Logout</a></span>
                            @else
                                @if (Route::has('register'))
                                <a class="remove_border register-link" href="{{ route('register') }}"> <span class="log-link">Sign Up<img src="{{asset('website/media/download.png')}}"/></span></a>
                                @endif
                                <a class="remove_border signin-link" href="{{ route('login') }}"> <span class="log-link">Sign In &nbsp;</span></a>

                                
                            @endauth
                        </div>
                    @endif
                    <!-- <span>Sign In / Register
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAApcAAAKmCAYAAAAYSrWZAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAABmNSURBVHhe7d1rkxxXfcfx3pVWMmBsbCDk8iABwi2hgk1MksoroIpHvFH7iV3281QlcWKwoUi4BQhV+IYsCRtiS3vN9Oxgy9buamb2193nnP58qoRGu0uVS3vOnK/+3TO78/Szz590AAAQsLv6HQAALk1cAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIGbn6WefP1k9Bpil737n26tHGc8898LqEcD8iEugaelwTBGgQKvEJdCEUiNyU6ITqJ24BKrTSkiuS3ACNRGXQPHmFpMPIjaBkolLoEiCcj1CEyiNuASKICYzxCYwNXEJTEZQDktoAlMQl8DoROW4RCYwJnEJjEJQlkFoAkPz4x8BAIgxuQQGZWJZJhNMYCjiEogTlHURmkCSy+JAlLCsj+8ZkGRyCUQIlDaYYgKXJS6BSxGVbRKZwLbEJbAVUTkPIhPYlLgENiIq50lkAusSl8BaRCU9kQk8iLgELiQqOYvIBM7jrYiAcwlLzmNtAOcxuQTuIxzYhCkmcC+TS+BDhCWbsmaAe4lLAABiXBYHlkyfSHCJHDC5BIQlMdYSYHIJMyYEGJIpJsyTySXMlLBkaNYYzJO4hBly6DMWaw3mR1zCzDjsGZs1B/PinkuYCQc8JXAfJrTP5BJmQFhSCmsR2icuoXEOc0pjTULbxCU0zCFOqaxNaJd7LqFBDm5q4j5MaIvJJTRGWFIbaxbaIi4BAIgRl9AQEyBqZe1CO8QlNMLhTO2sYWiDuIQGOJRphbUM9ROXUDmHMa2xpqFu4hIq5hCmVdY21EtcQqUcvrTOGoc6iUuokEOXubDWoT7iEirjsGVurHmoi7iEijhkmStrH+ohLqESDlfmzh6AOohLAABixCVUwMQGTtkLUD5xCYVzmMKH2RNQNnEJAECMuISCmdDA2ewNKJe4hEI5POFi9giUSVxCgRyasB57BcojLgEAiBGXUBiTGNiMPQNlEZdQEIckbMfegXKISyiEwxEuxx6CMohLKIBDETLsJZieuAQAIEZcwsRMWiDLnoJpiUuYkEMQhmFvwXTEJQAAMeISAIAYcQkTcdkOhmWPwTTEJUzAoQfjsNdgfOISAIAYcQkjM0mBcdlzMC5xCSNyyME07D0Yj7gEACBGXMJITE5gWvYgjENcAgAQIy5hBCYmUAZ7EYYnLmFgDjMoiz0JwxKXAADEiEsYkAkJlMnehOGISwAAYsQlAAAx4hIG4rIblM0ehWGISwAAYsQlDMBEBOpgr0KeuAQAIEZcQphJCNTFnoUscQkAQIy4hCATEKiTvQs54hIAgBhxCSEmH1A3exgyxCUAADHiEgCAGHEJAS6nQRvsZbg8cQkAQIy4BAAgRlzCJbmMBm2xp+FyxCUAADHiEi7BhAPaZG/D9sQlAAAx4hIAgBhxCVty2QzaZo/DdsQlAAAx4hIAgBhxCVtwuQzmwV6HzYlLAABixCUAADHiEgCAGHEJG3IPFsyLPQ+bEZcAAMSISwAAYsQlbMDlMZgnex/WJy4BAIgRlwAAxIhLWJPLYjBvngNgPeISAIAYcQkAQIy4BAAgRlwCABAjLmENbuQHep4L4MHEJQAAMeISAIAYcQkAQIy4BAAgRlzCA7iBH7iX5wS4mLgEACBGXAIAECMuAQCIEZdwAfdWAWfx3ADnE5cAAMSISwAAYsQlAAAx4hIAgBhxCQBAjLgEACBGXAIAECMuAQCIEZdwDm+SDFzEcwScTVwCABAjLgEAiBGXAADEiEsAAGLEJQAAMeISAIAYcQkAQIy4BAAgRlwCABAjLgEAiBGXcAY/1g1Yh+cKuJ+4BAAgRlwCABAjLgEAiBGXAADEiEsAAGLEJQAAMeISAIAYcQkAQIy4BAAgRlwCABAjLgEAiBGXAADEiEsAAGLEJQAAMeISAIAYcQkAQIy4BAAgRlwCABAjLgEAiBGXcIZnnnth9QjgfJ4r4H7iEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAGHEJAECMuAQAIEZcwjn8zGDgIp4j4GziEgCAGHEJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCVcwPvYAWfx3ADnE5cAAMSISwAAYsQlAAAx4hIewL1VwL08J8DFxCUAADHiEgCAGHEJAECMuAQAIEZcwhrcwA/0PBfAg4lLAABixCUAADHiEgCAGHEJAECMuIQ1uZEf5s1zAKxHXAIAECMuAQCIEZewAZfFYJ7sfVifuAQAIEZcAgAQIy5hQy6PwbzY87AZcQkAQIy4BAAgRlzCFlwmg3mw12Fz4hIAgBhxCQBAjLgEACBGXMKW3IsFbbPHYTviEgCAGHEJAECMuIRLcNkM2mRvw/bEJQAAMeISLsmEA9piT8PliEsAAGLEJQAAMeISAlxGgzbYy3B54hIAgBhxCSEmHlA3exgyxCUAADHiEgCAGHEJQS6rQZ3sXcgRlwAAxIhLCDMBgbrYs5AlLgEAiBGXMACTEKiDvQp54hIAgBhxCQMxEYGy2aMwDHEJAECMuIQBmYxAmexNGI64BAAgRlzCwExIoCz2JAxLXMIIHGZQBnsRhicuAQCIEZcAAMSISxiJy3EwLXsQxiEuAQCIEZcwIpMTmIa9B+MRlzAyhxyMy56DcYlLAABixCVMwCQFxmGvwfjEJUzkN6+9vnoEDMEeg2mIS5jIf778g9UjYAj2GExDXMKEXLKDYdhbMB1xCRNz6Q6y7CmYlriEibl0B1n2FExLXEIBXMKDDHsJpicuAQCIEZdQCPeJweXYQ1AGcQmF6O8TczjCdvq9415LKIO4hII4HGE79g6UQ1xCYbwgATZjz0BZxCUUyOVxWI+9AuURl1Ag91/Cg7nPEsokLqFQAhPOJyyhXOISCtYfnq+98ebqT0Cv3xPCEsolLqFwx8fHq0dAz56AsolLKJzL4/ABl8OhfOISKiAwQVhCLcQlVEJgMmfCEuohLgEAiBGXUBHTS+bI1BLqIi6hMgKTORGWUB9xCRXqD9tXBSaN69e4sIT67Dz97PMnq8dAhb77nW+vHkE7nnnuhdUjoDYml1C51954Y/UI2mBNQ93EJVTuxe+94kdE0ox+LfdrGqiXuIQGvPi9lwUm1TsNy5dXfwJqJS6hEQKTmglLaIe4hIYITGokLKEt4hIa0x/S3geTWvRrVVhCW8QlNOiPb7R+69bt1UegLP3a9Abp0CZxCY3qD+1379zp3rp5a/URKEO/Jvu1KSyhTeISGtYf3nfu3u1uvHVz9RGYVr8W+zUpLKFd4hIAgBhxCY3rJ0R39/e7N357Y/URmEa/Bvu1aGoJbROXMAP9YX54eLh8y5ebt3+3+iiMo19z/drr16CwhPbtPP3s8yerx8AM/OPfP9k9dP169/hjn1p9BIZzaxGW/T2W3m4I5sPkEmamP+Tffe+97sZNL/JhWP0a69easIR5EZcwQ8v7MO+e3of51k3vhUlWv6aW91cu1pjL4DA/LovDzC0vk1+71j3++GOrj8D2+jdHv7O/b1oJM2ZyCTO3vEx+50534+at7s0bb60+Cpvp106/hvq1JCxh3sQlsLpMfrc7Pjrubt52mZzN9GumXzv9GnIZHHBZHPiQbz35je5j1693dw4Our/408+tPgr3e/WNN7uH9va690QlcA+TS+BD+kjoY2F3Z6e7ZYrJOfq10a8RYQl8lMklcK5+inn9+vXu5OSk+5PPfHr1Uebst2/d7HYWUekSOHAek0vgXH089BHR/2SV/lXAv/7Nb1afYW76732/Bvq1ICyBi4hLAABiXBYH1tJfIt/b2+uu7u52N99+u/vyFz6/+gwt+9kvf9V9+tFHu8Pj4+7g4MDEEnggcQlsZHkf5t715XWPN2/c6L7+1a+sPkNLfvSTn3af++xnu+646+4euAwOrM9lcWAjy/swF7FxsL+/jI8+MGlL/z3tv7f991hYApsyuQS21k8xd3d3u+vX9rrDo+NFkHxm9Rlq1P+UnatXdru7+wfd8fGxqAS2Ii6BS+sj88qVK921q1eX9+aJzLoso3Lxj4T9w8Pu6OhIVAKXIi6BmPcnmXt7i8g86t66ebv7269+efVZSvJfP/lZ95lPP7aIyivd3QOTSiBHXAJxp5G50+3tXVu+4fbb77zTffGv/nL1Wab0i//9dffoI48s3xj/4GB/EZUnohKI8oIeIK6PlRe/90p3586d7l/+7cXu4U98ort563b36uuvd9//4Q9XX8VY+r/z/u++/x7034v+e9J/b/rvkbAE0kwugcF968knFv97cnpf5rWr3fFx173zzu+7L37eNHNIv/jVr7tHHvlkt7vbdfv7p/dTLp72F0H5yukXAAxAXAKjeuqJv+teeuWH3T//w1Pd3iI29xfBc/v277qvffmvV1/BZfz4Z//TPfbYp7pri7/bg8Xf7b/+x0vv/50DjEFcApN46sknup1+mrmz0129dq3bXfx+cHjQvf3277uvfOmLq69iHT/9+S+6Rx/9ZLd3da87PjnpDvf3u6PF7yeLv+GXTCmBkYlLYHL9ZK1/4U9/2fzq1SuL4NxdvqXR7//wh+7W7d8tXyDEB/r7JB9/7FPdJx9+ePkWQkcnx93h4dHysnf/Qh1TSmBKXtADAECMySVQlH6K2fvjfZn9T4zpL+/2P4rwD+++233pC59ffn5ufv7LX3UPf/zj3d61a8vbCfqfiPTH+yl7ppVAKcQlUKz+vszu5HgZTv/01De7K4vQvLLbx2a3iM3D7s7dO93/vfte9/WvfeX0/9CIH/34p90nPv6x7qHrDy1i8uoiJrvu6Pi4O1oE5b+/9P3ToNzZdT8lUCRxCVTj3ildH5v9TwNa/trpZ3ldd3hw0N29e7CMzjt373bf+PrfLL++VD/40X8vAvL6MiKvX9/rru7tLUOyf1FO/xNz+l/vx+SC6SRQA3EJVOupbz6xKLHj5ePT4Hyy29k5Dc7+VeiLPyx/9ZF2dHTYHR0edX/x53+2/Pqxvfra692V/sVKV64u//u6RUD2v/pXdff/fScnfUi+/H5I9m9O+dL3TSaB+ohLYFa++51vrx6N65nnXlg9AmibuAQAIMZbEQEAECMuAQCIEZcAAMSISwAAYsQlAAAxXi0OFGGqtwhqjbc8AqYmLoFRiMcyiE9gaOISiBGQdROeQIK4BDYiIOdJeALrEpfAuYQkFxGcwFnEJSAiiRKdMG/iEmZGSDIFwQnzIS6hcWKSEolNaJe4hIYISWomOKEN4hIqJiZpmdiEOolLqIiYZM7EJtRBXELhBCXcT2hCucQlFEZMwubEJpRDXMLExCTkiU2YjriECQhKGI/QhHGJSxiJoITpCU0YnriEAQlKKJfQhGGISwgSk1AvsQkZ4hICRCW0RWjC9sQlbElQQvtEJmxOXMIGBCXMl9CE9YhLeABBCXyU0ITziUs4g6AE1iU04cPEJdxDVALbEplwSlwye4ISSBOazJm4ZLZEJTA0kckciUtmRVACUxGazIW4ZBZEJVAKkUnrxCXNEpRA6YQmLRKXNEdUArURmbREXNIMUQnUTmTSAnFJ9UQl0BqRSc3EJdUSlcAcCE1qIy6piqAE5kpkUgtxSRVEJcApkUnpxCVFE5UAZxOZlEpcUiRRCbAekUlpxCVFEZUA2xGZlEJcUgRRCZAhMpmauGRSohJgGCKTqYhLJiEqAcYhMhmbuGRUohJgGiKTseyufofBCUuA6XgOZiwmlwzOExpAWUwxGZK4ZDCiEqBsIpMhiEviRCVAXUQmSeKSGFEJUDeRSYIX9BAhLAHq57mcBJNLLsUTEUCbTDHZlrhkK6ISYB5EJptyWZyNCUuA+fCcz6ZMLlmbJxiAeTPFZB0ml6xFWALgLGAdJpdcyBMJAGcxxeQ84pIziUoA1iEy+SiXxbmPsARgXc4MPsrkkvd5ggDgMkwx6ZlcsiQsAbis/ixxniAuZ84TAQBpzpV5E5czZvMDMBTDi/kSlzNkwwMwFufN/IjLmbHJARiboca8iMuZsLEBmJpzaB7E5QzYzACUwrCjfeKycTYwACVyPrVLXDbKvwwBKJ1zqk3iskE2KwC1MAxpj7hsjA0KQI2cX+3ws8UbYVMC0Ao/o7xuJpcNEJYAtMS5VjdxWTkbEIAWOd/qJS4rZuMB0DLnXJ3cc1khmw2AuXEfZj1MLisjLAGYI+dfPcRlRWwsAObMOVgHcVkJGwoATs9DZ2LZxGXhbCIAuJ+zsVzismA2DgCczzlZJnFZKBsGAB7MeVkecVkgGwUA1ufcLIu4LIwNAgCbc36WQ1wWxMYAgO3156izdHrisgA2AwDkOFOnJS4nZgMAQJ7zdTrickIWPgAMxzk7DXE5EQseAIbnvB2fuJyAhQ4A43HujktcjswCB4DxOX/HIy5HZGEDwHScw+MQlyOxoAFges7j4YnLEVjIAFAO5/KwxOXALGAAKI/zeTjickAWLgCUyzk9DHE5EAsWAMrnvM4TlwOwUAGgHs7tLHEZZoECQH2c3zniMsjCBADmTlyGCEsAqJuzPENcBliMANAGZ/rlictLsggBoC3O9ssRl5dg8QFAm5zx2xOXW7LoAKBtzvrtiMstWGwAMA/O/M2Jyw1ZZAAwL87+zYjLDVhcADBPGmB94hIAgBhxuSb/YgGAedMC6xGXa7CYAICeJngwcfkAFhEAcC9tcDFxeQGLBwA4i0Y4n7g8h0UDAFxEK5xNXJ7BYgEA1qEZ7icuAQCIEZcf4V8gAMAmtMOHict7WBwAwDY0xAfE5YpFAQBchpY4JS4XLAYAIEFTiEsAgKi5B+bs49K/MAAAcmYdl8ISABjCnBtjtnEpLAGAIc21NdxzCQBAzCzj0tQSABjDHJtjdnEpLAGAMc2tPWYVl8ISAJjCnBrEPZcAAMTMJi5NLQGAKc2lRWYRl8ISACjBHJqk+bgUlgBASVpvE/dcAgAQ03RcmloCACVquVGajUthCQCUrNVWcVkcAICYJuPS1BIAqEGLzWJyCQAwodYCs7m4NLUEAJhOU3EpLAGAGrXUMC6LAwAUoJXAbCYuTS0BAKbXRFwKSwCgBS00jcviAADEVB+XppYAQEtqbxuTSwCAwtQcmFXHpaklAEBZqo1LYQkAtKzW1nFZHACAmCrj0tQSAJiDGpvH5BIAgJjq4tLUEgCYk9rax+QSAKBwNQVmVXFpagkAULZq4lJYAgBzVksLuSwOAEBMFXFpagkAUEcTmVwCABBTfFyaWgIAfKD0NjK5BAAgpui4NLUEALhfyY1kcgkAQEyxcWlqCQBwvlJbyeQSAICYIuPS1BIA4MFKbCaTSwAAYsQlAAAxxcWlS+IAAOsrrZ1MLgEAiCkqLk0tAQA2V1JDmVwCABBTTFyaWgIAbK+UljK5BAAgRlwCABBTRFy6JA4AcHklNJXJJQAAMZPHpaklAEDO1G1lcgkAQIy4BAAgZtK4dEkcACBvysYyuQQAIGayuDS1BAAYzlStZXIJAECMuAQAIGaSuHRJHABgeFM0l8klAAAx4hIAgJjR49IlcQCA8YzdXiaXAADEiEsAAGJGjUuXxAEAxjdmg5lcAgAQIy4BAIgZLS5dEgcAmM5YLWZyCQBAjLgEACBGXAIAEDNKXLrfEgBgemM0mcklAAAx4hIAgJjB49IlcQCAcgzdZiaXAADEiEsAAGLEJQAAMYPGpfstAQDKM2SjmVwCABAjLgEAiBGXAADEiEsAAGIGi0sv5gEAKNdQrWZyCQBAjLgEACBGXAIAECMuAQCIGSQuvZgHAKB8QzSbySUAADHiEgCAGHEJAECMuAQAIEZcAgAQE49LrxQHAKhHut1MLgEAiBGXAADEiEsAAGLEJQAAMeISAIAYcQkAQEw0Lr0NEQBAfZINZ3IJAECMuAQAIEZcAgAQIy4BAIgRlwAAxIhLAABixCUAADHiEgCAkK77f2h7/k6Af8tHAAAAAElFTkSuQmCC" alt="">
                    </span>
                    </a> -->
                    </div>
                </div>
            </div>
            <div class="navSecond">
                <nav class="navbar navbar-expand-lg navbar-light ">
                    <a class="navbar-brand remove_border" href="/"><img alt="" src="{{asset('website/media/logo.png')}}"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                        <ul class="navbar-nav">
                            <li class="nav-item active"><a class="nav-link" href="/comparison">Comparison</a></li>
                            <li class="nav-item active"><a class="nav-link" href="/ournetwork">Our Network</a></li>
                            <li class="nav-item active"><a class="nav-link" href="/SpecialOffer">Special Offer</a></li>
                            <li class="nav-item active"><a class="nav-link" href="/AboutUs">About Us</a></li>
                            <li class="nav-item dropdown"><a class="nav-link" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Learn</a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item remove_border drop-down-news" href="/knowledgeBase#firstNews">News</a>
                                    <a class="dropdown-item remove_border drop-down-article" href="/knowledgeBase#firstArticle">Articles</a>
                                    <a class="dropdown-item remove_border drop-down-blogs" href="/knowledgeBase#firstBlog">Blogs</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>

  
<!-- Header -->
@yield('content')
              <div class="Footer">
                    <div class="picture">
                        <div class="container1">
                            <div class="row no-gutters">
                                <div class="col-x-2 col-lg-2 col-md-0 col-sm-0 col-0"></div>
                                <div class="col-x-6 col-lg-6 col-md-12 col-sm-12 col-12 promotionbanner">
                                    <h1 class="Lato_Black">Download Now!</h1>
                                    <p>Get the Best Rates,
                                        <br>Important Remittance Information and</p>
                                    <h4 class="Lato_Black">Special Promotion Alerts on the go!</h4>
                                    <div class="app-stores">
                                        <a target="_blank" rel="noopener noreferrer" href="A androidLink"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALcAAAA5CAYAAAB01Lo3AAAYrUlEQVR4nO1dB5gUVbb+q6q7Z3oyQ4YhIzkjKAKSlCB5RRRYkCAgyV3XRXisKGHxPVhccXUFySAIKCAwIDmD5CggSQYYcpRhcvf0fd9/p6vo7umBmRYJY//fd7/uvnVv1a2qc88959xzTiu4hzIA+gHoACAKfvjxdOECgIUAvgJw3HXkzQHcBCD8xV+e8nIbQAsSNTl3CQD7AOTycyo/cghuAaipARgGoIn/rfqRg2AFYCPnPuGUt/3wIyfhJIn7LoAQ/2v1I4chXnUqD374kdMg/MTtR06FMP1RXm3hwoXRu3dv5MuXD3fv3sXUqVNx6tQpeax8+fLo0KEDTCYTVFVFQEAAEhMT8eWXXyIyMhKvv/467HY7bt26JY/lzp0bFy9exKxZs5CUlGRco0iRIujUqRNmzpyJwMBAvPnmm1AUBTdv3pTnZb+4uDhMnz4dv/76a4YxhoWFoW/fvihRogRSUlIwf/587Nq1yzj+8ssvo1atWvjss8+QkJAg63idgQMHYuvWrW5t/UjHnZxu23399ddFcnKyIG7evCl09OvXTx7v06ePUWez2eRnQkKCKF++vGjfvr3sm5iY6Nbm0KFDIjIy0u06zZs3l8fr168vGjRoIJKSkmTRkZKSIi5duiSKFy+eYYzVqlUT169fN8aoj2PYsGFGm5kzZ8q6yZMnG3W5c+eWdWPHjvXb590L6TpnE3eZMmWE3W4XFy5cEEWLFjUIYtmyZeLdd9+Vv3v16iUJpEiRIhn6q6oqTCaTCAwMlG0mTJggNE2TdZ5tGzduLNu0adNGKIoi27CcOXNGrF271ujHY679AgICxPHjx8WdO3dE9erVZZ3FYhGzZs2S52vZsqWs+/TTT42J0qlTJ1nHCUZ89NFHfzTifVC547tYElUGYW1aITw4DVdmz4ft6tUnciF85513oGka6tati/Pnz8s6iglt2rQx2lB0gFM8IYKDg5GWlibbUzxwOBxSLCH4m8e8QQgha/nJovdJTU2VJbN+rVq1QtmyZeXngQMHjD4Ua5o2bYoRI0ZgxYoVCA0Nxblz56RY9c0332Dnzp2IiYmR7TlGP9yh+vQ8LAFAvZZIjKqJwIYtUPObSSjcoyvUsPAn7vGWLl1aEiSJgkQ8b948bNiwAQsWLMDIkSNlG13+jY6OxpEjR3D06FFs3rxZyr46KI8TlJ2zC15Xn0DeUK1aNTkZjh8/nuHozz//jMqVK8vvlK8p4//5z3+GzWaTMj/rkpOTf+/H+FTCN85tDYYSHAL75cs4fe4sTE2eQY0RA3Gr9UuImbUAl5b+8MQ8izt37kglkERJ7kbueePGDTRu3FgqaB999JHBUQcPHozLly9LgiF3pNL4KHDlyhVJ/LlyZfSAyJs3r1xpdFDBPXToEJo3b47169dj8uTJxn354Q7fOLcANOGAJcAEVQFOrT+FMxuOIapqMdQfPwQNZvwbhZo0cLquPF6QQxMff/yx/CTX69ixI9atWyeXeYJiC/Gf//wH3333Hb7++mssWbJEEvijAEUO4r333nO7GsWUSpUqYcqUKfcevRAICgqSq88XX3yBrl27yt9cnfxwh0+cW5HLM2BSVZgCTbCnOnB671VYzQrKV8uD0i9WRena5XB+Y2Mcmvodrh088tge++LFi+XyPWTIECnXLl26FDVr1sQbb7yBtWvXyjYkDoKmtwsXLiAkJETKvDS5/fLLL27n09t6g9lslrX6p47w8HBjInnD2bNnpW7AyVWwYEEpOlWoUEHWUQYfP3687MVxRUREyJWFpkpOhkaNGqFixYqyzg93kGX9D4CA7DwXJdCKgMrPQQ0KhepIg9msySXg+qUEWEwKCuQCNFsKilUugSptGyKiaBRunr2M5FsZbbuPAuTCXPrJCd966y3kz59fEsy7774rZdc8efJIOzgJq0yZMihWrBgKFCiANWvWyH5wys3PP/+8nBC60ucJKqKU0ym7u4o0zzzzDA4fPoxNmzZlere7d++Wsn6dOnXQv39/FC1aFJ9//jl69eqF+Ph42YZjvH37trwfjpuiyJYtW+Sk5ViPHTvmJ+97SFGcpsCw7PRSwiMR0mkQtNz5gZQkUFdSVAVpqUISwbN1IlG5QghEsg3BQSaER4YhLvYa9i3egm0zo3H30pXf+b68g0ohuR+5HjmzK3SZHE5C1i0fbvedSX122zwI5PQUM7KqKFKs4th/63VzGOJ84txqoBXWanWgBYfC5LBLwmCxmDX5cq9eTIE1UEWxQhaIVBtEfAJyhVtQvWEl1GhRBwIqzv0cC2G3PdLHSQIgwXhTvp4kwiBh62bErMBP1F6R4iNxByGoWh1YQkKhiDRoqgpNVeSn2aRBgYLLl20IsigoUcgEVTgAmw2O+ATkjwzEi22fQ41G1ZGSCpw7Hgs4/Jq+Hw8dvhN3SPUXYA4JcxK3Ak1RoZnSiVxycKi4eNWOkEAFJQqo0BwCZhUQNhuU5EQUL54bjV6pjsp1K+HWjURcPvNozG5+/GHgG3FrQUEIq1EXptBQaI40aJri5N6q8d1iUaGoKs5fFrBaFJQuCGgOBzQFMMMBR3IKkJqMiuXyolXn+ihZoTiuX0vE1fOPRx73I8fBR85tDUZErXqw6MTtJGqTlk7g6Z/k4Ko0pcdeB0IDgdIFhDTPaBCwKAImRcCWlAKzPQXP1ymO5u2fRZ6iRRBzIBZxTguBH374CB85tzUIEbXrIzA0TJoCdXlbErWmSvu35iT0ADOVTQUx11SEWBSUyWeHyk0gcnBVwKICqsOBVFsKVJOGRskCvUvmQ0RkBA5cvYvkxET/y/XDF6T47DhlJhFrGgQVSJoCKWWrivyuKhRJ7tVZFMCWpmL9UQUBKtCoTDJoKFGEkFzcHKAgJU1D0OpTiDh1BtYwDR+0rIA3ni2OL7afxuS1+5Dk9F9+GKCDVNu2bVGvXj3p300LD+3H9IfmJs/evXufKmqifb5cuXLSwkJbOf3O/UjfbMy2ndsSmQdlBn2AwPyFIJITpY1bcdqK0z8Vp7PQvTpy99S09O9tqiSiYckEpKYq0Cwq0oSK4O3nUDYmBqZAFamqCRocCAgKoHEaB64kY8wPh7Bo61EgzfdtZhLy8OHD0a9fP2PL3Rvmzp2LUaNG4eTJk08FiQwYMEBuxRP0duQmkh+I841zK4q0jJjNKoRdvUfI5NiKk7ihEzc5u4CqKHL30i5UrPo5DNygblghGfHJGkJ2nkOFS+eghJthV7jbKUCenmyzQ7XbUT2/hoXvNMCmlyvin9EHsH5n9rfzq1evjoULF6JkyZJGHbk1OR13+7jLV6hQIVnfpUsXvPbaa6hfv77cOfyjgY5mfBb6xpbu1Uhfm+vXr2ewq3NzLCoqSn7yOJ3VngT47FtC+drMnTETiVG5R9SKIgnZk3Prxyhj22waVpzLByXsLlrdPIai12OhhFtgUzQpqriCv5IFYI67g4ZVItGg7EtoONaOLXsyuodmBm6n08ekePHisgVdS8eOHSsdlujuypfFEC9ur9MzsGHDhtLzjj4fTwNcfbkfxoYOfVXomOUJij30qJwzZ45cKXRXYU6EH3/8Ubof/OMf/zBWkccN34hbUeRmDYtD01xk7XSOrWounJt1quKcACR8ARFplTaTwBE/wJr7AkztomBLsEOxiwx+z3xVKmVziwWcGUkOE+KSs7ezSbdQnbBXrVolvQI9Pf4op/7www+yDBo0SHL5a9euPREv6VHDYrFIFwBvYBzo6NGjZVwpRSAGS9BRjHI/cT8HsUcN3xVK526kQ27YpBO8K+dWFcVNRJF1EEgNCoJqNuGNed+jxcqVuGYNQ3xYGEq1zgMtLhV2mzAcZcmEAlQHkDcc9iQF83ecx4czNiMmJusbPu3bt5fRLMS+ffvkC6EYcj/QYemPDFf3BLrb0lGLREuib9mypSRsuuJOmzZN+sUzgIL+OvSY9PTZeZzwXSwxpfuSpNmc1hKduN2sJvdEFKpvqdYAqAEWtJ2zBI2XrUd8RBisdgcSZh3F2YDKKN4qL7RfU+BIE9JUqIaYgaBQrPoxFuMX78f6zQezN05FQY8ePeR3Lt3Dhg17IGFnB6VKlZLiDFeB06dPZ6knvQ7pvMUl/cyZM1nqQ/dXysGM1o/3Yv+/X5QPQTdZjpViBSONshPYQL93rmY66Ot+6dIl6VGpu9s+qYq3j2JJuhJBc6BqSpeppeihuMrcbKdKv2/atW0hQbJti6+jUS96I+KDQ+Awq9AsDoQmJ+Pu5J9wRlRC6fb55VY+hIZdx+/gf+eswNLVdDHNvv8JUy3Q3EccPHjQ8N/+rSDnousslVRGxlAxpYzOlA6zZ8/2evZu3bqhe/fusg+XfMqudIMlZ9QDKjzBazCOkmFo5IoMOaOrLkUARuJwQrm6xHqCY/vggw/QokULaSrkBOfqxfjLCRMmZOkpcCJ64quvvpIWJ/qQ87xUyjMDx8CIJyry5O47duxwU9Ip5tBNmMd4f950BoYKMkqJYiLDBbMKH8USRRK2xaTBrqmGNp0uX8NFoUzn2GmBFpgsJjT4egWeX7YZCdZgOCwqTCJNKpCpgYEIik+E8uVBpEXWx5lqQZjw9R5M+24HUn7DJg41eD10iy81MzdWBg+TM3q9U0WRoWf6Cxw3bpxUOl3BF0guxlKjRg389a9/dTtOBYvmOlcwfKxJkyaykOCHDh1qHCXjmDRpkiRcV5BLzpgxw6ihaMA2rqKAzsXJrVeuXInatWsbx6jcM+8JC+MyPc+f2f17giKKHpBxvwgg5nDhZNStUHp76j30WecKQN96XXllcAYZhCuqVKkifdatViv69OkjA0+yCp85t9zEMWk0m6QTMh+e5iqWKFIRtAdboZg1NJi5EjWXbEWCNQjCokETaUhzRudbHQKRERGI1RQMnLQFs29dROKtm1kYyf3hGp1CE5U38CUxoc39wECAZs2ayZelEzblzE8//VRGyJOT8sVwIv3lL3+R0Tu63P72228bhE1RhNE9fKnkVkymQ87IKCEu7UzWQ7z//vsG4TF+kpODE4yTgEl7dOiBFK5By7oPOLmrTtjbtm2T52ZCIEYgMQFRz549JQdlu/vBU/GmGMaQPe4TUMTjxhetJDp0BsJrcIXQsX//fskEqNhzA433T6ZCyxWj+Mnd+aw8iZthdFzpqPAvX7482zSQ7bwlIfkKiFenzRW91mwV3b5fJd5cslp0X7pa9IxeI3pFrxW9l68TfZavFb02bhW9tv0ovuw9UuzI3VhsK9pcbCvVXOwo8bLYVaKJOBJVX1x7pqn4qXIzMbLi8yIqT76HmrvixRdfNPJ8fPbZZ17bmM1mcfv2beENDodD1i5fvlzmLzl16pT8fffuXdGwYUO389SqVUvcuHFDHr969aqwWq0y18m1a9dkHY8999xzbn0aNWokz0WcO3dO1oWGhhrjYd9nn33WrU+PHj2MkR49elTWde7c2airW7euTPqjY82aNW79mVtFTxTE+ytQoECGZ8J70bFr1y4xe/ZsMWfOHLFgwQJx8uRJ49gnn3wi2xcrVkwmMSL0XDBDhgwR8fHxMh9LuXLljHN3795dpKamyrb8zrpmzZoZ42FCI71teHi48UzHjBnz6PKW6HZueNt+hwKH1QIlwITKM9aj/Pc7kMjYQzNFEbsURXIJFUqeCMyx38SMmzH46cp5wPFwne4p11J5IpfRc5J4gsfJMciRdGWTihe5BfOFMASN3JNyL2U/giKDZ8jYnj178Mknn0iuxp1QyvrklBQ/iP/+978Z0p1t3LgREydOlKsBw8rImTlWihQE07l5ugJQLOEGE+XozO5HP0Y5lhyU6Ny5s1wNKDrpogblX3JTfQXwBnJ/V9FGB58BVxx4rBz6d+4jcJWi2MVn+8orr8ixUVTiPTGcjs+InHr16tVS/6AIQj1DX0m5mUaZnO/FVRzLKnwWS/gSaDERukKpy9y0TIQEQDNpKDttI0ot3oWkIIoiKgLtNoSagqAEadig3sWM6wew50oMkPb7RJJQ4eJySBmTD5LKj2duECpZ3hQ6EqUemHvixAkZB6kjs+XRleB5Ldd8gJn1oTVCF3X0/CRwEinFocyuQwLWc6l43g8tMgQnFwmQ5k/K5/pxEgrvObPzu4ITkoSnix60azOni6tyntnGEWVkij9Vq1Z1q6cCDqeuooNiE5VcKuu0ajEGVRfNFi1alGVrlCt83H53cm6TBqGpxoYNlUdhtUAzm1Bi+hYUXbwXSSFBgElBLrsDIaFhOGi6i/lxJ7D20nGIpN83mQwVrW+//VYSNxUSbj6Q62UFNHXpMjtNYYyYN24/E9Obaz1fuOvOYWZ9XLmeZ9aozPxfHpQYSD8PlTUSCpwTlETCBKB6lqqsgCtLdpQ4ndA//PBDI+kR5WUGVXPCcpK5Kpg6aGViHxI8benc8aRyDifh+wLf8paAfiLpYonJycG5qaOEW6FZLSg2fTvyL9qPlKBAmDQrclnCcDFvCP6Zcgx9Tq/Eml8O/u6ErYMvU7cnc4nW85fcD1QeSdzETz/9JM2IsbGxRo/WrVt77d2gQQPjOxVEV27DyHtvoJiggxYZfWUhYb/00kte+9A9AE7xyROcRCRkOL9TaaQiTKLi1nh2CNsXUKGl5UnPwcLrU6zhvfC5MoKfIhw8OD65uS56cId4zJgx8jtNrBTffEW2FcqwAgVF34VLxbC9B8XfN20VgzduFYN37xaD9+8V4/p/IWbkfVPML9pHrKzydzGrZj/RreLLIjJXnseVENFIUKmDilGlSpVkYkrXdrly5RLvv/++zMaqg8oOj1E5ZEJLgllfmzZt6ta3Tp06MpElcfnyZZnIkn2uXLki6+Li4tyUJRaeQ88eGxMTIxNkhoSEiFu3bhmKa7169dz6vP322yItLS1ThZLtqSTq2LhxY4bnMWjQILF//36p0HKM91MomST0Qc+XCqyuULJ94cKFDaWRY3NtW6hQIak8E4sWLXI7xnei35sOXUH1ofiW5TW8YEHR//to8eGBw2Lolu1iyJ7dYujh/eL/BkwUk/P0Ed+W+JuYXXuo6FulvSiYrzD30x8bYevl1VdfdUsnzIe4YcMGmfp35MiRYtq0aeL8+fNuD3bw4MFu53AlIk6AcePGyfTIo0ePNqwewiU1Mstbb71l1LPNxx9/LPuMHz/eIACia9euRh9aGnRwUrAPs7rqKYx1eCNufdLNmDHDqDty5IgcE687depUo56WiPz58z9U4u7fv7+cMPpEX7dunahQoYKctLTk0Pqiw5O4WaKjo43jnOSeaaKzUXwj7ohChcU70SvEqENHxLCD+8QwEnb/KWJGoaFiWtXhYmCNbqJUwdKPnaA9C3Ngr1+/XjwIzKGtm6k8CyfC/fCvf/0rQx8S8v0watQot/Y0O7oSoSdI8MSxY8dk+549exotOnToIOtoUmTa5MzAicYJ7+0euQrpGDBgwAOfa8mSJY32ej7xzz//3KjjSnfixIkMI/E0U7K0aNFCppwW9zHfZrH4Rty5CkWJv69cI8acPiGGH94v/j3oGzH5mdHib1X7iaolawoEZsxd/aQUiiKtWrUSCxculByNIgQ52NmzZ8Xu3bvF8OHDvdp+XUvbtm0l4ZDTk7sw9/fWrVtFx44dM+3zpz/9SWzatEnExsbKPuxLkaF169aZ9nnttdfEnj175GRjH3LqLl26iOnTpxvEzXzftOfv3LlTrFixQtSuXdvoz3vlsr5v3z45RtrN2Yd26ypVqmR6XR7j86D4QG7/oHcXFRUlz8v2vXv3lnUc15QpUwwRSzhXO9qreX2OZ+LEiRnOxQlHezeLnqv8kRK3xRokBi5ZJL68cVNMGLJMDC37nnihaF2BAPWJJerMCpfUsmXLyoT02e2bL18+UbFiRVGwYMEs9+HEYR/2zWofJsXn0k6Ozt+cmMK5wcLE9Vk5B8dYunTpDHqGt0LC5PPgGLNyfo6L+krevHkzyPAcd7du3WSpWrWqrOPGGcdDUcXzXKtWrTLEmd/4bn3/Z4WClcuLjr2HiPpFGoug4LCngpCfhkLi69u3r1T4qAd4jpm7gfpfn8ydOzdH3Ts5ta5Qeiqij5S4/eX3Kdy2p3ikg0orOW5wcLB44YUXxObNm41j7dq1y1HvQf9bFFqO+Bz8xJ0DC//Hh7qAq1Xj8OHDbsrYvHnzctS9lypVyri3ESNGPIxz+on7SS00x23fvt2rpWTSpElSbs1J98s/tTp48KBcmaigPoRz3vEptYMfjw7t2rWTTlv0I6GvCv8qJLP84H64Ic5P3H7kVMSpT8Qf1/jhx8OH4iduP3IqBIn7kv/1+pEDcYnEvcz/Zv3IgVhOkYR/k7sPQMZ/+PTDj6cTDPWpSc5N7/XODJjwv0g/cgAY29eFdK3HMTFkZIkz800Rv2nQj6cQDJViXoh+AHYCwP8DayBFwn+HBQ0AAAAASUVORK5CYII="
                                                alt=""></a>
                                        <a target="_blank" rel="noopener noreferrer" href="A iosLink"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALcAAAA5CAYAAAB01Lo3AAATkElEQVR4nO2dBZBX1RfH79LdjXTX0FKCSIqAlIQiEqKCMIQOJqKIYBMDCgaKCIOkIEgKIgbdjdLdLN3vP58ze9//7tv3++3uL1hZ33fmzm9f3D73vnPOPedshPo/iiuleiqlnlBKPaA8eLi/cEQpNUMp9YVSapfZ8keVUmeVUpaXvHSfp/NKqSYQNTt3IaXUeqVUZm+n8pBIcE4pVTmpUuoNpVR9b1Y9JCKkVkrdYufeHcVve/CQmLAH4r6klErnTauHRIbLSaKEBw8eEhssj7g9JFYIcXvwkCjxryDuHDlyqN69e6uhQ4eqzJnDp5FMmjSpioiIiMOb4UWaNGlU1apVVc6cOcNST7Zs2dSDDz6o0qdPn6D9LFWqlCpRokSCtiEyIZX2vXv3to4dO2aBM2fOWBkzZgx5HV27drXmz59v7d2719q1a5c1ffp0q23btgnW50qVKkl/+/fvH5byO3fuLOXXqVPnnvctIiLC/nvVqlXWr7/+mlDjHJksoVZU8uTJ1aRJk1S7du3se3v37lWRkZEhr6tLly6qdu3aqm/fvurWrVuqadOmatq0aVJ/p06dQl5fQuP69ev3vAWMbZIkSdSIESPsewn9lUww4v7kk0+iETZYvnx5WOqyLEvt2rVLjR49Wq7HjRun+vTpo0aNGqUWLlyoJk+ebL9bsmRJVatWLXX+/Hk1b948dfPmTblfunRplSpVKrVhwwa5zps3r3z2KVfn4/mmTZuE3YC9OnjwoGrcuLEsqEWLFqnbt2/b7QF3796N1s5y5cqp6tWrq9OnT6v58+fbdYNixYrJs5QpU6o//vjDrlcDVqR+/frqwoULwvZQp67HFxo1aqQKFiyotm7dqlauXGm/lTVrVpU/f361ZcsW1aRJEyHaJUuWqGvXrrmWVKVKFTV48GB15swZtX37drVmzRppB/1lsypUqJCwYWxe69evj5Y3bdq0qmXLljJ2jPfJkyfjPb/+cM/Zkrp161pO3Lx50ypcuHBY6lu6dKm1e/fuaJ9M0smTJ63Fixfb1/369ZNWrVu3zrpy5Yq1evVqK1euXPLsq6++kmdZs2aV60WLFlm3b9+2kidPLtewVitWrJC/e/ToYV2/ft367bffrAMHDki+efPmWSlSpJDnFStWlHt9+/a1637xxRfl3po1a6TclStXWnny5JFn5Js2bZo8o02U3bJlSztv8eLFrUOHDlnXrl2z9uzZI23hnZo1a7qOB+XNmDFD6oN1AOPGjbOft27dWu4xNvv27ZO///rrLytz5syu5f3www/yDu1mTGG7uL9w4ULpP325dOmSvGOyg4UKFbI2bdpk7dy501q7dq11+PBhq1SpUiFjS1RCEPdPP/0Ug7hff/31sNXni7gZfHhw/i5Xrly0SS5ZsqRcM3FcV69eXa4bN24s1/v375drJjJbtmzW5cuXrRdeeEGeMYFgxIgRQhBvvvmmXDdv3tyVuMuXLy/X77//vlyXLl1armfOnCnXyZIlsxcRiwuCOXjwoFzTJxYVRJUvXz4rQ4YMsvCAL+J+6aWX5Hm9evWiXT/zzDNy3ahRI7meMmWK1Pf000/LNb9u5WXJksXasWOHtWDBAhmLpEmT2uN+69Ytq2rVqlaOHDmkzcyDSQe6HyTKmDt37v1L3OyETISJ8ePHh7VOX8TNrsGOxN9vvPGGtEjvlqTZs2fLPX199epVWYT169e35syZI5MzcOBA2UV5pnd1vfPpLxE7q0nMTuKGqEHu3Lntutj1z58/b7e5WLFi1vfffy+CMV85dlTuZ8qUSfLSfp23Xbt2cq927dqu47Ft2zbZKc174Mcff5S/GzZsGC0/Oz147bXXfI4x5fF1Me/9+eef0QTKb775RpQGSZIkkcROjZDPV5FnFy5cENrQX7hgifue89zwX6j+wJEjR9SQIUPUl19+GdY6EWzgb00eFB65fPnyUj/IkCGD/N64ccN+xymYzZkzR9WoUUP479WrVwuP+dhjj0l/tm3bps6ePWvXB1KnTi2/8MkAPtgNvEddmifXdev2olLbvHmzmjJlivrggw+k/AoVKsgz1Jvg8uXLdl4nL+8E9bkJ7vDWJhD6VZTq0l+5tIE+uj03+8zfvKPrgc8+cOCA8OlZsmRRAwcOlGtzHIJB2PTcCFyFCxeOobc+fPiwevfdd1XHjh1Vvnz5hLAZHPShEFvRokVDrp+FSJIl+/86LlOmjFqwYIEQ0Pjx4+XesmXL5PfZZ5+V34wZMwrh/vzzz3a+qVOnqocffljVrVtXhN/FixfLIunQoYNdjgk9ifrXl4BHHUx0q1at7LoR9hDMyIN2B0Lr3r27WrFihSxEvWAuXryo9u3bp7p166ZSpEgh97QG6M6dO671zZ07V9WsWVPGGrRt21Z+GRNlLJi4tp/7LDiEUF9joP/mXRYXBAwt0JeRI0eqQYMGqTFjxohQGdvijA9CxpbkzJlTPtvwskeOHJHPKp9APjtuemX41Y8++kiEJD7rAMFjw4YN1ujRo60qVaqEpF20B/z+++9SF+DXWf63334rz/iFF4evzp8/v/0c3hLQVs0u/PPPP3LvgQcesN978skn5V6FChXkunLlytH02vCgzs/8hAkT5B6/8NSUW6RIEXlWrVo1EdaWL19uTZ061bp796580jVvqwV0eFb6CAsGHnnkEdfxgC/eunWrCJ5aUIbX1Xz9448/LveaNGki17BbYNCgQT7HePjw4baAqlk75h4hWL8DDw8PDp3ocTl9+rQIw/QLQT6E5w+h47k5KKGR/sCAjho1ynrvvfeEp6Sj/oCQNmTIkKDbBhE/99xzUhbCE4cbTv5bp1atWgmBvfLKK0IEzufwo/Dc+rpWrVq2oKgTC6JTp07CD6sovvipp54SvplrhEyuS5QoES1f+/btZWG9/PLLNv9u1sOC79mzp2wKTZs2FUHT7OOnn34qeWl3mzZtbE2PW0LwRDv03XffSb3mO/D+tF/nT5kypQiT/jQZadKksZ5//nnrww8/tMfNOVYsUtpFefpe3rx5ZdF/9tlncqBnbhL/CuJGqAonWNXsmiHqtJf+Gyl44tZHveEE2gHnLuclL4V154a3Onv2bFgJG94d1ZkvNsJLXvJF3EGpArt27SoqnHAB9Vbz5s3Vxo0bw1aHh8SNgHZuJGs0CuEEvLy3M3kpwBQZEUXcGeK7fNEVY3ATLssvDG84LDl+/HjIy0aPjI5XGwL9/fffPg9Y/s3AQKts2bKiK0aHjM6bQxD640FdVIHu3KiPwokQ2hjESKjUEFIjIyMloca8n3ZIVIuo/bR9iwmOtzEb8LRLQQiU6CbDCdNKLZSJST916lS0lnPIc79MGvpprOxMcPDFmYAGFoGpU6d2zY8OXh+iJHbiDlig1HYT4UIoj2BNYFecPXt2YUmwr8iVK5fcy507d1hYoFADG3TsugG25cOHDxe7FtiSIkWKqIYNG6rdu3fHsL1+6KGHRAHQrFkzNWzYMCnnv4CAdm5tJhku4AoWjhX9+eefS4s5Jh4wYIDd+vuBNcGkVZ/qwpK4naD6SrAxGpxNJIKdObYUGbDhFN4i4UTlypVDbkCFYRFGSAAvk9mzZ9uCZIMGDXzmw0iJHT5TpkzRDLDwYsFrB0tBnrkBIyQEWL4MGEdpIARWq1ZNdlTKiQswLtP1s1tjlRgbaDvjaAr+GLNh6ccXTFv8uYH66B/OxgUKFPD5Hn3Eewe1sDboApUqVRIvHTelg7ZsrFOnjnw506ULT1yogHZubB3CDeySQ7maH330UbvFeL5wb+PGjXKNkZcvXhQDInZKPEZwLIDvnTRpknXx4kW7PJ699dZbMfLC42IzjjGUrhPba+yYNU6cOCFOEezMcR1zjKOw54itz7169RLvHPqngbES+XEUYEd35unQoYO1bNky8ezRwM4aQdXNRpzTY+qgzBYtWljp0qUT23CE9qNHj1qpUqWK9j72MRhUmcDICnueUO7cARM3xj/nzp0LK3lv377dSps2bcg6rFkSoC0Cx44da9/DycAtH5OtgWMBRl++gNG9mRfLPs1KYCODgZAvMMH+DIdYVJzYasyaNcsqWLCg3z5//PHHfsfYKUxr6z4NiBOXOw0I1snCaS8m8Pbbb8vCN2EKt2PGjLGfQD+bN2+OJgwzRwlO3CrKwyXcYPCdKz+QRBksFrB+/XrbMo1dWYNJcStbm4Ca+OKLL6wGDRqI5dvIkSOjPcO6TeeFWDFPBTdu3JBfPFT4KuEGxs5qeibha+mvf9qcVgPVH7tv2bJlXd8vU6aMuI9pryLw9ddfiyoXK74aNWrY7/bp08d+B08fLCQJtYEnkblIIHDMVXW+okWLWnfu3JGk1ZP0CetK8mnrRcZFg01AfynZAJYsWWI/M/1DE4y4TYEsnMBHEDNP01QyvgkzV+ygwbBhw+z8lKlNdWEP0qdPH6NscwFYPuya33nnHfs5E6xZBsxGTX00G4JpqkqCwFDfafhyD9MJ9zSTZbCi2CoIxtdOju24htPEVUXZ4ms7IdqCz6jzHcxjNfCX1Pcx5YXgNXA5c7YD4Rd2yIqaT2fZqGj1c+zWE5y4+RzFZpMdKvBphN8NtK2DBw+WlkAUzl1O+zCCJ554IkZe7TAL4J3ddMgQrPZ0B+zoetK084DlJ1COdhpwLj5fCYcH2BxzUVhRbIRbHUOHDrXf0Y7MZoLV0NC+lM7EDqudSvgKaRYK4tZ0wK9pw62TWb75tTDTxIkT5TlEnj179qCJOyjDKSR23LNwiQo30M4Q9yIQ4KLVokULycmxfsWKFeVoH90wrmamBgRtyowZM6LVYkr7xN1wi9+B2xRjgS4ZFC9eXGJ9mK5Zly5dUvv373ftAS5ruJEB7f7lD2vXrlXt27eXvrRp00aC4qBxyJMnj8Q8QduEvjuu0Lpz5Sd+DONPvBTqRPPEL36wJnB5M2OgaNAeDXwlmQcn0B4B+oF2JliNXFDEzcSNHTv2nhA3AXVMJ9j4gGA3+GcCVHoTJ070mZvANqj13AZfORyInTh16pR9h2AzTtB+X36IpsOuW15fwGKSNH36dPHxRH1H/v79+6sePXrEuRzttK0c/XDCDJqjnaqd/bh69WqM+6YvLb6p/oDKVDsnB4Ogvd/RFbNS0fWGC5wcBuMhT0QjDXZPBp/dWhMaum50vtzDaZldjEhU8YWp43ULaaYdZN1gTuaVK1fiXTfe8ezeut3aOz6uMD3OtaOxG8yFF2jYNjYqdnw3/TeEzQLZs2dPQGWbCElohwEDBkiIr3ChX79+QpSBwtwpWrduLeyU9vCG2Djq51PZq1cvuUcINF/E7bZbaeivAzh06FCM5/4OpWBjNI4ePRpQT7HS1ND90zAJyS10gskumW0xAeFxxK/hDOmm/MQHPHHihP03kQJYjPcCIXEQNo93Q4nYVGOxJTzQtWaBIDy+3ueABA9zK0oNZqofiTKlgbBjBu7RCSFVC1UYZmk1F+cBpkCJztytftoW2zuxpWbNmtllOPXXCKka+jDJTKZGCDt9N88nHXkKbNmyRQLrOAVKZ7AfnUx1qi+BNcQpdN7vRAlynjoFi+PHj7sSUnySDmUG8Lr3lRdth+l8YYZFMInbilLnER5MP2dy8ezX4KBCP3MSNwcXpraA8AzozDV419fBFZ7iHNx07Ngxxjt44JsHPN26dYv2HPWhBqo259kB86fDXgB09+ZzQryZ+ngztFpciBvnFkI3aGB27HbCWqBAgaBUvkYKbTg1AhtypBsq6Lh8gSZ2HyZSg0MXf2WZJ5gmgZrErU/rOJghvBnx/Ew/Uo6hTTWWSdxajWZF2asTxsFcFJahQnRLqPA0IGQ2E8KVEa/EhI4xaCYdj9BcRBBzly5d7PdQL5qqxV9++cV69dVXJRyHaSY8efLkaGXHhbhJHFqZ+nm+kPrUlgM04tVg0oDP7L+OuEnEtuAY2QmIgUHq3r27fKI4NSPGhZurGjsEn9hg26KD31h+DmjMhH5Wg5M/jrudxA1B0w83YGft9NI3iRsiRN/OSZ4TsDvYZfhrH6eTOFf4Arpngm/62vncWEfnDg0B6pNcN/C+Zkd00kFDAcGE/PWB8nVkWV/QwYCCJe6A3cz8AbVP586dJeA7KrWlS5eK7vTYsWMxcmFNhuUZVnnEmCa+NSotYlsHC2JaY7+MwIjwQ4xsf0DbQbv5ReU3c+ZMif9nCpgTJkwQXTb3EE5RLaIeo3+zZs2KoUFgLFatWiVCGlofdNjoognbhtqOeniO1ikufUagw0oPHTNuZmhgiCXOuKG12rlzp9/8xESvV6+eWCoyH8QfXLduXYw2E5ebkGtokWgj5TOPboIgZRFSjnGjTOc5gRP4AhCWDr02YffQFDHOO3bsELUmc+UrFng8ELib2X8pmTt3fO3MzZ2bT3uwMoSX4r5ze//NLJ4IxkPIGZDTQ3jhEbeHRAuPuOMA8/QwPkfjKupQQx/8wJs6Y2B7CB+8b2QcgK0FBlO4ZOl/+BRXcLSPsMnpJUZFIRCUPMQREVFSZcL+N877BOy6gfLcweT1EBAuJYkicA9xQDDE6RH2PYf87/eYymcPHu5/HIO4f/Im0kMixDxYkkI4mHAw5c2wh0SC8zj/sHNjyPuUUuqcN7MeEgFwoeoIXWuL9n9wquG/uyml8oXa1sSDh3uAw5j+KKV6KqVWKaXU/wDBccJlJhcy6wAAAABJRU5ErkJggg=="
                                                alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <footer class="footer">
                        <div class="container-fluid">
                            <div class="container1">
                                <div class="row">
                                    <div class="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
                                    <div class="col-xl-4 col-lg-4 col-md-3 col-sm-12 col-12">
                                        <div class="contact-links">
                                            <h5 class="Lato_Black">Help &amp; Support</h5><a rel="noopener noreferrer" class="remove_border" href="/faqs">FAQs</a>
                                            <br><a href="/contact" class="remove_border">Contact Us</a>
                                            <br><a href="/ournetwork" class="remove_border">Partners</a></div>
                                    </div>
                                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                        <h5 class="followus Lato_Black">Follow Us</h5>
                                        <ul class="footer-links"><a rel="noopener noreferrer" href="https://www.facebook.com/RapidRemit" target="_blank" class="Lato_Regular"><i class="fab fa-facebook-square bg-icon-img facebook"></i></a><a rel="noopener noreferrer" href="A instagramLink"
                                                target="_blank" class="Lato_Regular"><i class="fab fa-instagram-square bg-icon-img instagram"></i></a><a rel="noopener noreferrer" href="A linkedInLink" target="_blank" class="Lato_Regular"><i class="fab fa-linkedin bg-icon-img linkedin"></i></a>
                                            <a rel="noopener noreferrer" href="A twitterLink" target="_blank" class="Lato_Regular"><i class="fab fa-twitter-square bg-icon-img twitter"></i></a>
                                        </ul>
                                    </div>
                                    <div class="col-xl-3 col-lg-3 col-md-5 col-sm-12 col-12">
                                        <h5 class="contact Lato_Black">Contact Information</h5>
                                        <p class="Lato_Regular"><span> Dubai International Office<br>Office # 100, Level2, Precinct Building 5 The Gate District<br><span>P.O.Box 507048, Dubai, UAE</span></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="row row2">
                                    <div class="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-12"></div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 Lato_Regular text-center" id="footer_links"><a href="/privacypolicy" class="remove_border">Privacy Policy &nbsp; | &nbsp;</a><a href="/termsandconditions" class="remove_border">Terms &amp; Conditions &nbsp; | &nbsp;</a><a href="/cookiespolicy" class="remove_border">Cookies Policy</a></div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <div class="copyrights">
                        <div class="container1">
                            <div class="row no-gutters d-flex justify-content-center">
                                <p class="Lato_Regular">Copyright © 2021 <a href="https://www.rapidremit.biz" style="text-decoration: none; color: inherit;">RapidRemit.biz.</a> All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>

@include('website.partials.script')