@extends('website.layouts.app')
@section('content')
<div class="bg-white">
    <div class="container-fluid specialOffer">
        <div class="container1">
            <h1>Special Offer</h1>
            <img src="{{asset('website/media/specialOfferImg1.f286c333.png')}}" alt="">
            <div class="specialOfferRedLine"></div>
            <h3 class="react-reveal" style="opacity: 1;">
                <span class="SO-span-1 wow animate__flipInYMainPageTitle1">S</span>
                <span class="SO-span-2 wow animate__flipInYMainPageTitle1">p</span>
                <span class="SO-span-3 wow animate__flipInYMainPageTitle1">e</span>
                <span class="SO-span-4 wow animate__flipInYMainPageTitle1">c</span>
                <span class="SO-span-5 wow animate__flipInYMainPageTitle1">i</span>
                <span class="SO-span-6 wow animate__flipInYMainPageTitle1">a</span>
                <span class="SO-span-7 wow animate__flipInYMainPageTitle1">l</span>
                <span class="SO-span-8 wow animate__flipInYMainPageTitle1"></span>
                <span class="SO-span-9 wow animate__flipInYMainPageTitle1">O</span>
                <span class="SO-span-10 wow animate__flipInYMainPageTitle1">f</span>
                <span class="SO-span-11 wow animate__flipInYMainPageTitle1">f</span>
                <span class="SO-span-12 wow animate__flipInYMainPageTitle1">e</span>
                <span class="SO-span-13 wow animate__flipInYMainPageTitle1">r</span>
            </h3>
            <div class="react-reveal p-5 wow slideInRight" style="background-color: rgb(238, 238, 238);">
                <h1 class="first-heading Lato_Black text-center p-5 " style="color: rgb(204, 204, 204);">No Offers Found!</h1>
            </div>
        </div>
    </div>
</div>
@endsection
