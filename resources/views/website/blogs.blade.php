@extends('website.layouts.app')
@section('content')
<div class="container1 blogs">
	<div class="wrapper">
		<div class="blog_readmore_2_sec1">
			<div class=" row blog_readmore_2_sec1_row1">
				<div class="blog_readmore_2_sec1_row1_col1 col-md-6">
					<h1>Blogs</h1>
					<div class="blogsDiv">
						<div class="blogsDivSmall">
						<span>
							<p class="p1">BLOG</p>
						</span>
						<span>
							<p class="p2">1</p>
						</span>
						<span><h6> <time datetime="1619174129090">23rd Apr,2021</time> </h6></span></div><img src="{{asset('website/media/blogsImgMain.9a912555.png')}}" alt=""></div>
				</div>
				<div class="blog_readmore_2_sec1_row1_col2 col-md-6">
					<div class="blog_readmore_2_sec1_row1_col2_row1">
						<h3>Are Informal Remittance Channels Worth the Risk?</h3></div>
					<div class="blog_readmore_2_sec1_row1_col2_row2">
						<h4>Informal Remittance Channel or Hawala: </h4>
					</div>
					<div class="blog_readmore_2_sec1_row1_col2_row3">
						<p>If you are an expat living in the UAE and have been sending your remittance to your home country, there is a high chance that you have been confused between opting for an informal channel of remittance like hawala and a formal channel like a bank or transfer service. We analyze whether it is viable to stick to the cheaper but more risky option of using hawala or, if it is more feasible to use the slightly expensive but more formal options of a bank or a transfer service. In order for you to reach to a sensible decision, it is important to understand how the two systems operate. "Informal Remittance Channel or Hawala:" So how exactly does the hawala work? &lt;br&gt; Let’s assume that Asif works in Abu Dhabi and wants to send the money to his hometown in New Delhi to his family. Asif approaches a hawala agent in Abu Dhabi with his request. The agent in Abu Dhabi reaches out to his associate in New Delhi and asks him to transfer the money to Asif’s family on his behalf. The hawala agent in Abu Dhabi receives cash from Asif here and his associate in New Delhi gives it to his family. No cash has been transferred physically or electronically and Simply put, the hawala system moves money across borders without actually moving money. Why is hawala not a good remittance channel? The hawala system is used primarily by expats in UAE, especially from South Asia because it is a cheaper form of remittance. So why should Asif not use it? Although a cheaper remittance channel, informal channels have some alarming limitations. First and foremost, hawala operators are at risk of arrest all the time and if they are to be arrested at any time while you are performing your transaction, you will never see your money again. Just imagine losing your monthly salary of thousands of dirhams to save fifteen to twenty dirhams of transaction fee. Also, since the transaction is done in cash only, there always remains a chance of getting mugged, especially when the amount is so big. Asif or his family member in New Delhi can always get mugged after they have received the cash. Given New Delhi’s crime rate, this would be a big no. Formal Remittance Channels Moving to the formal and more secure remittance channels, there are bank transfers and transfer services like MoneyGram and XpressMoney. Banks charge a hefty transfer fee on your remittance and their exchange rate is usually much higher than the open market rate. The most viable option for expats to send remittances is transfer services like MoneyGram and Al Ghurair Exchange that operate locally in UAE. These transfer services are extremely safe and reliable. You know the money you transfer will reach your family, GUARANTEED. With some transfer companies like UAE Exchange or XpressMoney, you can even track the status of your remit to stay on top of your transaction. These transfer companies charge a small fee (15-25 dirhams) for their services. Like I said above, I would forego a few dirhams rather than a few thousand dirhams. PRO TIP: You can use Rapid Remit to compare the exact amount that will be received by your family back home through different transfer services in the UAE. Moreover, most of these exchange companies have websites and phone apps that make it extremely convenient to transfer money from the comfort of your home. When you use the hawala service, you have to pull cash from an ATM or your bank branch and coordinate with the agent in your city. Similarly your family in New Delhi has to deposit the money into a bank account after receiving it from the associate and coordinate with the associate in India. With a transfer service, you can literally send money in a few clicks from your mobile phone or website. The bottomline is: Use a transfer service and you will have peace of mind and a sense of security AND it is extremely convenient. </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection