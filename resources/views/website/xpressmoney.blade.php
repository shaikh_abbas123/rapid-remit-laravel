@extends('website.layouts.app')
@section('content')
</div>
	<div class="container-fluid bg-white pt-5" id="partner-details">
		<div class="container1">
			<div class="transfer_row-main-div">
				<div class="row transfer_row">
					<div class="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12"><img src="https://firebasestorage.googleapis.com/v0/b/rapidremit-41516.appspot.com/o/images%2FExpressMoney.png?alt=media&amp;token=84aaee2a-f6e3-4284-9003-6d136a7288e7" alt="" style="width: 200px; height: 100px; margin-bottom: 20px;">
						<h1 class="Lato_Black" style="text-transform: capitalize;">xpress money</h1></div>
					<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
						<div class="div10">
							<div class="divA"></div>
							<div class="divB">
								<a href="https://www.xpressmoney.com/" rel="noopener noreferrer" target="_blank">
									<button class="buttonGoto" style="text-transform: uppercase;">GO TO xpress money</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  <div class="container-fluid companyOverview bg-white">
      <div class="container1">
        <div class="companyOverviewContent">
          <div class="row companyOverviewContentRow1">
            <div class="col-xl-3 col-lg-4 col-md-12 col-sm-12 col-12 img-col">
              <div class="picDivb first-img"><img src="{{asset('website/media/CompanyOverviewImg1.png')}}" alt="">
                <div class="CompanyOverviewRedLine"></div>
              </div>
            </div>
            <div class="col-Text col-xl-9 col-lg-8 col-md-12 col-sm-12 col-12 contentDiv">
              <h2>Company Overview</h2>
              <p style="max-height: 500px; overflow-y: auto;">Global leader in cross-border P2P payments and money transfers MoneyGram leverages its modern, mobile, and API-driven platform and collaborates with the world's leading brands to serve millions of people each year through both its walk-in business and its direct-to-consumer digital business.</p>
              <p></p>
            </div>
          </div>
          <div class="container1">
            <div class="row companyOverviewContentRow2">
              <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                <div class="divHeading">
                  <h1>Features</h1></div>
                <ul>
                  <li>
                    <div class="span1">
                      <h6>TRANSFER FEE</h6></div>
                    <div class="span2">
                      <p>Applicable </p>
                    </div>
                  </li>
                  <li class="keyFeature">
                    <div class="span1">
                      <h6>PAYMENT TYPE</h6></div>
                    <div class="span2">
                      <ul>
                        <li>CREDIT CARD</li>
                        <li>DEBIT CARD</li>
                      </ul>
                    </div>
                  </li>
                  <li class="keyFeature">
                    <div class="span1">
                      <h6>PAYMENT TRANSFER METHOD</h6></div>
                    <div class="span2">
                      <ul>
                        <li>PHONE</li>
                        <li>ONLINE</li>
                      </ul>
                    </div>
                  </li>
                  <li>
                    <div class="span1">
                      <h6>CONTACT</h6></div>
                    <div class="span2">
                      <p>+8000178023</p>
                    </div>
                  </li>
                  <li>
                    <div class="span1">
                      <h6>COMPANY TYPE</h6></div>
                    <div class="span2">
                      <p>Remmittance Providers</p>
                    </div>
                  </li>
                  <li>
                    <div class="span1">
                      <h6>MOBILE APP</h6></div>
                    <div class="span2 d-flex j-center-480">
                      <a href="https://play.google.com/store/apps/details?id=com.gpshopper.moneygram&amp;hl=en&amp;gl=US" rel="noopener noreferrer" target="_blank"><img alt="" width="40" height="40" src="{{asset('website/media/play-playstore-icon.png')}}" style="margin-right: 20px;"></a>
                      <a href="https://apps.apple.com/us/app/moneygram/id867619606" rel="noopener noreferrer" target="_blank"><img alt="" width="40" height="40" src="{{asset('website/media/iOS-Apple-icon.png')}}"></a>
                    </div>
                  </li>
                  <li class="keyFeature">
                    <div class="span1">
                      <h6>DOCUMENTS</h6></div>
                    <div class="span2">
                      
                        <ul>
                        <li>Passport</li>
                        <li>Emirates ID</li>
                      </ul>
                      
                    </div>
                  </li>
                  <li class="documents">
                    <div class="span1">
                      <h6>KEY FEATURES</h6></div>
                    <div class="span2">
                      
                        <ul>
                            <li>Transfer with in 10-15 minutes on time.</li>
                        </ul>
                      
                    </div>
                  </li>
                </ul>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                <div class="picDivb"><img src="{{asset('website/media/CompanyOverviewImg2.png')}}" alt=""></div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
  <div class="container-fluid divBottom bg-white bg-img-for-reviews" style="padding-bottom: 0%;">
    <div class="row companyOverviewContentRow4 ml-0 mr-0">
      <div class="container1">
        <div class="divHeading">
          <h1>Reviews</h1></div>
        <div class="carousel-root" tabindex="0">
          <div class="carousel carousel-slider" style="width: 100%;">
            <button type="button" aria-label="previous slide / item" class="control-arrow control-prev control-disabled"></button>
            <div class="slider-wrapper axis-horizontal">
              <ul class="slider animated" style="transform: translate3d(0px, 0px, 0px); transition-duration: 350ms;">
                <li class="slide selected">
                  <div class="row no-gutters john-row">
                    <div class="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
                    <div class="mr-john col-xl-4 col-lg-4 col-md-2 col-sm-2 col-2"><img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADEAAAAwCAYAAAC4wJK5AAADO0lEQVRogbWaPWgVQRSFj0/jCDoKkmgaRRQHEYVoBEG0CiiCoEgsxMJCS7vYiKBNCitBWyuDaEQLJUQUBbUR/A0IKRz8AYv8IKIwNhMV5YbZsHnZnd2dmXu67FzOtyez+97MnYdQWam6rFSHgg1qqC6jFWi+A8AbACNWqodWqi0MAWozFgWYHwRwB8Dy3OXfAC4DGBRG/wq+80BGoxBWqhMArgNYXFIyAWBAGD0cdPeBjNohnPlQzUfwOYDTwuiPdf1jGLVCWKn6Adxu+A79BLBTGP2Fm1E2ZXnzfQDuAehoYE5aBmBmcOb7Y26GN7WVah2AuwBEQ/NMq6sKUjBKQ1ipOtwnxJpAc9IL32Aqhm8mLgLYHWH+DsCNipokjMIX20rVC+Cl52OuSlMAeoXRE2V1KRkLZsJKRcGuRpiTTlUESMooepyOAdgTYT4ijH5QUZOUMe9xslItBfABwIZAc1oabBNG67ICDkb7TByPMCfd9AXgYrSHGIgwJ12pUZOcMRfCStUHYHuE+bgwesxXwMXIz8TJCHPSSI0aFsZsCCsVfeUfjgQ88Q1yMrKZ2A9gZYT5PwCvKmrYGFmIvghz0ldhtKmoYWNkIfZGAj7XqGFjtKxUKwD0RAJ++Aa5GTQTmyPXMKSqR4mVQSE2RZqjxo6MlZEqhKwYZ2VQiM4EgLUV46yMVsDmvEjrK8ZZGRRiVQJAt5Wq2zPOyqAQfxIASLs8Y6yMlmtApZCve83KSBniqJVqSckYK4NCTCcCdAHoLxljZVCIqu1kE50tqWVlUIjxhIBeK9WRguusjNluh5VqOrKVmNcnAFuF0TP5i5yMbCn+NJE53BLjTMF1NkYW4llCAOmcaxbnxcbIQnj3xwHqLOjwsTFmQ7hjKW+7JUDz9tOcjHzLpqoN30TUanxdUM/CyIe4BeBvIsAFYfRUwXUWxlwIYfSkO7WJ0aRruV8q8uBitHfFewKeW1qhjrqz59H274d2cTAWnBTRTxAAHKhhPOZMqUv9rckdpWYUhaDfVLwtOaifdi/nkDD6fZMb52SUndldo9N69ydN3X33H3kkjE6ywUnJKFv/nwew0Z0vDwujvc2xQKVhAPgPvRNITbyK7CsAAAAASUVORK5CYII=" class="inverted-coma">
                      <div class="pic1">
                        <div class="pic-i"></div>
                        <div class="pic-j"><img alt="" src="https://firebasestorage.googleapis.com/v0/b/rapidremit-41516.appspot.com/o/user%2FJordan_profile.jpg?alt=media&amp;token=c7bc0231-c0a3-40c3-95ad-afe63d7897c7" class="john-img"></div>
                      </div>
                    </div>
                    <div class="mr-john john-detail col-xl-5 col-lg-5 col-md-9 col-sm-9 col-8 ">
                      <h5>Mohtashim</h5>
                      <p id="para2">MoneyGram is very quick, efficient, professional and polite. I have never ever had a money transfer declined by MoneyGram.</p>
                    </div>
                    <div class="mr-john col-xl-1 col-lg-1 col-md-1 col-sm-1 col-2"><img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADEAAAAwCAYAAAC4wJK5AAADQ0lEQVRogbWaS6iOQRjHf8dtNsYttxQRhg0RRSeyUCiKRC6JhSyUFTZKbG0szv4sDxbslEspVhbuCywmSRZuG2qyGPfm9H76Lu99nvlvvr6ZZ57f/L/53ncu70uVvDa7vDZzKgMjFMuYUFThtVnptbkD3ASeeG3WJui8CGMoJ/FU4DxwGpjcVfUd2K+cvR3V8wSMHhNem4PAZWBBQfxv4Jhy9kqr3idiDGWJlwGjwJYabf4AR5saSckY8tosAZ4BMxr0KUAOKGdv1AlOzQgX9smGyTvtxrw2m2vGJ2WEwFkNk3ekgBtem4U1YpMygomHLQFBc4HrXpvJFXFJGeNDlv1f22oDcLGibVJG5+4UbndPgfktIeG2uFE5+6QoICVjfMZWzn4AjrdMHjQRGPHaDEyeHaVk/F92KGdvZdN/Ww2H2basbSpG/4xtgJd9S4EmegesUM7+KGqTgtGzAFTOWuBqy+RBi4FDZQEpGHmr2JEIQNCZGjGijAETytnnwKsIwCqvzdayAGlG0X4i5uILOlYjRoxRZOJeJGC310ZVxIgxikw8Av5GAKYB2ypixBi5JpSzDngfAQiqui7EGIV7bOBtJGBTjRgRRpmJr5GANdleukwijDITLhIQ1jrLK2JEGGUm2i4LurW0ol6EUWZCSwAq6kUYZSbmCQBmV9SLMMpMLBIAVP1dRBi5Jrw28yN2YN2aXlQhySgaifUCyYN+ldSJMYpM7BICfCupE2MMmPDaTAL2SgHyCqUZeSOxD5B6HvG5oFyUkWfirFDyIFtQLsroMeG12QOsEwQM7N5SMP6fdnhtpgCva8yydfVFOdszmaVidI/EKcHkQfdzypIwxk1kh7XnBJMHPej+kpLRGYnhGuucpurfQydjdExME07+XDn7pq8sGaNj4jHwUxAwllOWjNE5Ff8EXBBKHo7gr/UXpmR0n4pfyo7eP0YCritnc3OkYuQ9jA/38p3ZCVv4nNQQsFY5+6IsQJpR+FAkg4X1zeEMVueVhbvK2R1NeiPBKDXRB1sdHpADRwq2leE0b112WNxKbRm1TXSZCUO/PfvldgNTsqpR5eyJtgZiGI1N9MFmAuFdjbC0PqSc/RKTrxUD+AfxcDE8MWLNeAAAAABJRU5ErkJggg==" class="inverted-coma2"></div>
                    <div class="col-xl-1 col-lg-1 col-md-0 col-sm-0 col-0"></div>
                  </div>
                </li>
              </ul>
            </div>
            <button type="button" aria-label="next slide / item" class="control-arrow control-next control-disabled"></button>
          </div>
          <div class="carousel">
            <div class="thumbs-wrapper axis-vertical">
              <button type="button" class="control-arrow control-prev control-disabled" aria-label="previous slide / item"></button>
              <ul class="thumbs animated" style="transform: translate3d(0px, 0px, 0px); transition-duration: 350ms;"></ul>
              <button type="button" class="control-arrow control-next control-disabled" aria-label="next slide / item"></button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row companyOverviewContentRow5 ml-0 mr-0">
      <div class="container1">
        <div class="row divBlack">
          <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
            <p>Share your experience with us, Get register your self and let us know yourself</p>
          </div>
          <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
            <a href="/login" class="reg-log-btn"><h3 style="cursor: pointer;">Register/Login</h3></a></div>
        </div>
      </div>
    </div>
</div>
  
	

         
@endsection
@push('custom-css')
<link href="{{asset('website/css/style-CO.css')}}" rel="stylesheet">
@endpush
