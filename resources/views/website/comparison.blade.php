@extends('website.layouts.app')

@push('custom-css')
<link href="{{asset('website/css/comparison.css')}}" rel="stylesheet">

@section('content')

 <!-- Comparision -->
 <div class="coooom">
 <div class="react-reveal bottom-div">
 <div class="d-flex justify-content-start"></div>
 <div>
     <a class="comparision-compare-text-show" href="#" type="button">Go to Comparision</a>
     <a class="comparision-compare-icon-show" href="#" type="button">Compare</a>
    </div>
 </div>
       <div class="comparision-main-div wow animate__zoomIn">
                <div class="comparision">
                <div class="main-container">
                    <div class="container3"></div>
                    <div id="comparision-list" class="container3">
                        <div class="row">
                            <div class="reFormPortionDiv">
                                <div class="redFormItems" id="redFormItem1">
                                    <input type="text" class="input1 form-control textBlack" name="UAE" placeholder="AED" disabled="" value="AED">
                                        </div>
                                        <div class="redFormItems" id="redFormItemTo">
                                            <p class="align-middle to">TO</p>
                                            </div>
                                            <div class="redFormItems margin-Top-comparision-form" id="redFormItem2">
                                                <select class="dropdownInput" id="label col-xl-2 col-lg-2 col-md-2 col-sm-5 col-9" name="to">
                                                <option value="">SELECT</option>
                                                <option value="AUD">AUD</option>
                                                <option value="EGP">EGP</option>
                                                <option value="EUR">EUR</option>
                                                <option value="GBP">GBP</option>
                                                <option value="INR">INR</option>
                                                <option value="LKR">LKR</option>
                                                <option value="NPR">NPR</option>
                                                <option value="PHP">PHP</option>
                                                <option value="PKR">PKR</option>
                                                <option value="USD">USD</option>
                                                </select>
                                                </div>
                                                <div class="redFormItems small-w-100 margin-Top-comparision-form" id="redFormItem3">
                                                    <input type="number" class="input2 form-control textBlack" name="Amount" placeholder="AMOUNT" value="">
                                                    </div>
                                                    <div class="margin-Top-comparision-form">
                                                        <a href="#">
                                                            <button class="comparision-form-table-btn">SEARCH</button>
                                                            </a>
                                                            </div>
                                                            </div> 
                                                            </div> 
                                                            <div class=" row comparision-form-row2">
                                                                <div class="col-xl-2 col-lg-3 col-md-4 col-sm-5 col-5 ml-auto">
                                                                    <form>
                                                                        <select name="result-dropdown" placeholder="Result per Page" class="dropdownsC cal-dropdown1 center-label1 bg-white ">
                                                                            <option class="opt" value="5">5</option>
                                                                            <option class="opt" value="10">10</option>
                                                                            <option class="opt" value="15">15</option>
                                                                            </select>
                                                                            </form>
                                                                            </div>
                                                                            </div>
                                                                            </div>
                                                                            <div class="container3">
                                                                                <div class="row">
                                                                                    <div class="row no-gutter">
                                                                                        <h4>No Amount Selected</h4>
                                                                                        </div>
                                                                                        <div>
                                                                                            </div>
                                                                                            </div> 
                                                                                            </div>                             
                                          
                                           </div>
                                          

                       <div>

                            <!-- Instructions  -->
                           <div class="container-fluid Instructions">
                               <div class="container1">
                                   <div class="row">
                                       <div class="instruct1 col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12">
                                           <div class="picDivi">
                                               <div class="picDivx">
                                                   </div>
                                                   <div class="picDivy">
                                                   <img src="{{ asset('website/media/Instruction.png') }}" alt="">
                                                       </div>
                                                       </div>
                                                       </div>
                                                       <div class="instruct col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12">
                                                           <h1 class="instruct-heading">Instructions</h1>
                                                           <p class="instruct-para">
                                                               <span class="instruct-span">Sending money to your hometown could cost you a lot, especially if you aren’t aware of the hidden fees. <strong>Money transfer</strong> companies and banks not only charge you a transfer fee, but also a hidden markup on their 
                                                               <strong>currency rate</strong>.
                                                               </span>
                                                               </p>
                                                               <p class="instruct-para">
                                                                   <span class="instruct-span">You could save a lot of money by comparing <strong>currency exchange rates</strong> and transfer fees in real-time with the Rapid Remit <strong>money transfer</strong> comparison tool. Our transparent and updated comparison tool lets you compare more than 20+ features of money transfer services in the UAE. Just enter your amount and desired <strong>currency rates </strong>and our comprehensive platform will help you decide the <strong>best money transfer</strong> service for your remittance needs.</span>
                                                                   </p>
                                                                   <p class="instruct-para">
                                                                       <span class="instruct-span">With just a few clicks, you can filter your results according to your need. You can find the fastest, cheapest, most reliable service in your vicinity.</span>
                                                                      <span class="instruct-span">With just a few clicks,you can filter your results according to your need. You can find the fastest, cheapest, most reliable service in your vicinity.</span>
                                                                       </p>
                                                                       </div>
                                                                       </div>
                                                                       </div>
                                                                       </div>
                                                                       </div>


                                           
                                                                        </div>


 </div>
 </div>
@endsection

@push('custom-script')
<script>
console.log('Here');
</script>