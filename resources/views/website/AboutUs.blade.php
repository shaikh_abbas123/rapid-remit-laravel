@extends('website.layouts.app')
@section('content')
<div style="background-color: white;">
    <div class="about-us-wrapper">
        <div class="container-fluid about-us">
            <div class="row m-0">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="react-reveal" style="opacity: 1;">
                        <span> </span>
                    </div>
                    <h1 class="react-reveal Lato_Black" style="opacity: 1;">
                        <span class="span-1 wow animate__flipInYMainPageTitle1">A</span>
                        <span class="span-2 wow animate__flipInYMainPageTitle2">B</span>
                        <span class="span-3 wow animate__flipInYMainPageTitle3">O</span>
                        <span class="span-4 wow animate__flipInYMainPageTitle4">U</span>
                        <span class="span-5 wow animate__flipInYMainPageTitle5">T</span>
                        <span class="span-6 wow animate__flipInYMainPageTitle6"> </span>
                        <span class="span-7 wow animate__flipInYMainPageTitle7">R</span>
                        <span class="span-8 wow animate__flipInYMainPageTitle8 !important">A</span>
                        <span class="span-9 wow animate__flipInYMainPageTitle9">P</span>
                        <span class="span-10 wow animate__flipInYMainPageTitle10">I</span>
                        <span class="span-11 wow animate__flipInYMainPageTitle11">D</span>
                        <span class="span-12 wow animate__flipInYMainPageTitle12"> </span>
                        <span class="span-13 wow animate__flipInYMainPageTitle13 !important">R</span>
                        <span class="span-14 wow animate__flipInYMainPageTitle14">E</span>
                        <span class="span-15 wow animate__flipInYMainPageTitle15">M</span>
                        <span class="span-16 wow animate__flipInYMainPageTitle16">I</span>
                        <span class="span-17 wow animate__flipInYMainPageTitle17 !important">T</span>
                    </h1>
                </div>
            </div>
        </div>
        <div class="react-reveal container1 wow slideInRight" >
            <div class="row about-second-container">
                <div class="col-xl-5 col-lg-5 col-md-10 col-sm-12 col-12">
                    <h2 class="Lato_Black">About Us</h2>
                    <h5 class="Lato_Regular">Regions first money transfer comparison service</h5>
                    <div class="horizontal-red-line col-xl-5 col-lg-6 col-md-8 col-sm-11 col-11"></div>
                </div>
                <div class="about-us-imgs col-xl-6 col-lg-7 col-md-0 col-sm-0 col-0">
                    <div class="img1"><img alt="" src="{{asset('website/media/Group-24.71e6fc97.png')}}"></div>
                    <div class="img2"><img alt="" src="{{asset('website/media/Group-22.8ff9d527.png')}}"></div>
                    <div class="img3"><img alt="" src="{{asset('website/media/Group-23.3e6b2019.png')}}"></div>
                </div>
                <div class="about-para col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                    <p class="Lato_Regular">Did you know you could save upto 100 AED per transaction on your remit to your home country. That means you can save around 1000 AED annually by choosing the right transfer service. Rapid Remit allows you to compare money transfer
                        services in UAE from the ease of your screen without any charge.</p>
                </div>
                <div class="about-para2 col-xl-8 col-lg-11 col-md-12 col-sm-12 col-12"></div>
            </div>
        </div>
    </div>
    <div class="react-reveal wow slideInLeft">
        <div>
            <div class="container-fluid about-third-container">
                <div class="row m-0">
                    <div class="red-background col-xl-5 col-lg-5 col-md-6 col-sm-7 col-10">
                        <div class="red-background-content">
                            <h6 class="react-reveal Lato_Black" style="opacity: 1;">
                            <span style="display: inline-block; white-space: nowrap; animation-fill-mode: both; animation-duration: 2000ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-877349407986392-4;">W</span>
                                <span
                                    style="display: inline-block; white-space: nowrap; animation-fill-mode: both; animation-duration: 2144ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-877349407986392-4;">H</span><span style="display: inline-block; white-space: nowrap; animation-fill-mode: both; animation-duration: 2297ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-877349407986392-4;">A</span>
                                    <span
                                        style="display: inline-block; white-space: nowrap; animation-fill-mode: both; animation-duration: 2462ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-877349407986392-4;">T</span><span style="display: inline-block; white-space: nowrap; animation-fill-mode: both; animation-duration: 2639ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-877349407986392-4;"> </span>
                                        <span
                                            style="display: inline-block; white-space: nowrap; animation-fill-mode: both; animation-duration: 2828ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-877349407986392-4;">W</span><span style="display: inline-block; white-space: nowrap; animation-fill-mode: both; animation-duration: 3031ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-877349407986392-4;">E</span>
                                            <span
                                                style="display: inline-block; white-space: nowrap; animation-fill-mode: both; animation-duration: 3249ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-877349407986392-4;">
                                            </span><span style="display: inline-block; white-space: nowrap; animation-fill-mode: both; animation-duration: 3482ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-877349407986392-4;">D</span>
                                                <span
                                                    style="display: inline-block; white-space: nowrap; animation-fill-mode: both; animation-duration: 3732ms; animation-delay: 0ms; animation-iteration-count: 1; opacity: 1; animation-name: react-reveal-877349407986392-4;">O</span>
                            </h6>
                            <h4 class="Lato_Black">Putting you in control of your money transfer</h4>
                            <p class="Lato_Regular">At Rapid Remit, we set about tackling the problem of incomplete or outdated information for people who want to send their remittance back home. We obtain updated information from our money transfer providers through real time
                                data feeds. Our aim is to collect all data that might help you choose your money transfer service. Our data is presented in a user friendly and concise manner for users to make the best decision with least hassle and allow
                                for a fair comparison of providers. Rapid Remit does not make any money from its users or money transfer providers, eliminating any conflict of interest that might exist between and we are able to provide unbiased options
                                to our users.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="react-reveal wow slideInRight">
        <div>
            <div class="container1 about-forth-row-main-div">
                <div class="row about-forth-row">
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-4 d-sm-block d-none"><img alt="" src="{{asset('website/media/group-28.2879a3b1.png')}}" class="mission"></div>
                    <div class="our-mission col-xl-5 col-lg-5 col-md-6 col-sm-7 col-12">
                        <h1 class="Lato_Black">Our Mission</h1>
                        <div class="horizontal-red-line col-xl-5 col-lg-5 col-md-5 col-sm-5 col-5"></div>
                        <p class="Lato_Regular" style="box-sizing: inherit; font-family: lato-regular; margin: 0px 0px 10px; padding: 0px; color: rgb(122, 122, 122); font-size: 14px; background-color: rgb(255, 255, 255);"><span style="font-family: calibri, sans-serif; font-size: 11pt;">Provide a one-stop solution for all your remittance needs.</span></p>
                        <p class="Lato_Regular" style="box-sizing: inherit; font-family: lato-regular; margin: 0px 0px 10px; padding: 0px; color: rgb(122, 122, 122); font-size: 14px; background-color: rgb(255, 255, 255);"><span style="font-size: 11pt; font-family: calibri, sans-serif;">Providing transparent, comprehensive and updated information of&nbsp; money transfer providers in the UAE</span></p>
                        <p style="box-sizing: inherit; font-family: lato-regular; margin: 0px 0px 10px; padding: 0px; color: rgb(122, 122, 122); font-size: 14px; background-color: rgb(255, 255, 255);"><span style="font-family: calibri, sans-serif; font-size: 11pt;">Partner with remittance companies to promote best use of their services.</span>&nbsp;</p>
                    </div>
                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-0"></div>
                </div>
            </div>
            <div class="container1">
                <div class="row about-fifth-row">
                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-0"></div>
                    <div class="our-vision col-xl-5 col-lg-5 col-md-6 col-sm-7 col-12">
                        <h1 class="Lato_Black">Our Vision</h1>
                        <div class="horizontal-red-line col-xl-5 col-lg-5 col-md-5 col-sm-5 col-4"></div>
                        <p class="MsoNormal"><span style="font-family: lato, sans-serif;">We envision a transparent comparison service for expatriates living in the Middle East and North America (MENA) region.</span></p>
                        <p><span style="font-family: lato, sans-serif; font-size: 12pt;"> We want to empower users to take control of their remittance experience and make the right choice by providing them a clear picture of their money transfer providers.</span></p>
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-5 col-sm-4 d-sm-block d-none"><img alt="" src="{{asset('website/media/group-30.88f2e451.png')}}" class="vision"></div>
                </div>
            </div>
        </div>
    </div>
    <div id="aboutus-div" class="row companyOverviewContentRow5 ml-0 mr-0">
        <div class="container1">
            <div class="row divBlack" style="padding: 10px 0px;">
                <div class="col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                    <p>Sign up now for a free Rapid Remit account.</p>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
                    <h3 style="cursor: pointer; margin: 0px; height: 44px;">Register/Login</h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
