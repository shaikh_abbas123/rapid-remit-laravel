@extends('website.layouts.app')
@section('content')
<!-- code -->

<div class="container-fluid news">
    <div class="container1">
        <div class="row newsRow1">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <h1 style="color: rgb(249, 13, 38);">NEWS</h1>
                <img src="{{asset('website/media/newsImg.png')}}" alt="">
                <div class="newsRedLine"></div>
                <h2></h2>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="News01">
                    <h4>NEWS</h4>
                    <h6 class="">01</h6>
                    <h5><time datetime="1612188093424">Mon, 2021/02/01</time></h5>
                    <h3>Nothing adverse' in overseas remittance data, says CBDT </h3>
                    <p>The Directorate of Intelligence &amp; Criminal Investigation (I&amp;CI) of the income tax department during August and September 2019 obtained LRS data from several banks in Mumbai and Delhi and verification of the top 100 cases was undertaken. However, nothing adverse was found,” said the report, which was submitted to SIT probing black money earlier this year.</p>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection
