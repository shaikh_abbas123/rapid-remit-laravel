@extends('website.layouts.app')
@section('content')
<!-- Code here -->
<div>
<div class="container-fluid FAQs">
<div class="container1">
<div class="react-reveal wow slideInRight">
<div>
<h1 class="h1Service ">Frequently Asked Questions</h1>
<img src="{{asset('website/media/FAQsImg1.png')}}" alt="">
<div>
</div>
</div>
</div>
<div class="FAQsContent">
<div class="react-reveal wow slideInLeft" >
<div>
<h2>Frequently Asked Questions</h2>
<h4>Everything you could possibly want to know about the Us.</h4>
</div>
</div>
<div class="questions">
<div data-accordion-component="Accordion" class="accordion">
<div class="question">
<div data-accordion-component="AccordionItem" class="accordion__item">
<div data-accordion-component="AccordionItemHeading" role="heading" class="accordion__heading" aria-level="3">
<div class="accordion__button" id="accordion__heading-raa-0" aria-disabled="false" aria-expanded="false" aria-controls="accordion__panel-raa-0" role="button" tabindex="0" data-accordion-component="AccordionItemButton">
<h6>Why would I choose Rapid Remit as a payment comparison site?</h6>
<span><i class="fa fa-plus"></i></span>
</div>
</div>
<div data-accordion-component="AccordionItemPanel" class="accordion__panel" role="region" aria-labelledby="accordion__heading-raa-0" id="accordion__panel-raa-0">
<p>The creators of Rapid Remit have significant industry experience and the concept came from our own life experiences. As a result of those insights we believe Rapid Remit has the most complete set of data available on the market, and presented in the most effective way to ensure users have all the information required to make an informed decision on the best provider to use. Additionally, in certain cases we have also managed to negotiate preferential terms for our users, so before making an international payment it always makes sense to make a comparison through Rapid Remit.</p>
</div>
</div>
</div>
</div>
<div data-accordion-component="Accordion" class="accordion">
<div class="question">
<div data-accordion-component="AccordionItem" class="accordion__item">
<div data-accordion-component="AccordionItemHeading" role="heading" class="accordion__heading" aria-level="3">
<div class="accordion__button" id="accordion__heading-raa-1" aria-disabled="false" aria-expanded="false" aria-controls="accordion__panel-raa-1" role="button" tabindex="0" data-accordion-component="AccordionItemButton">
<h6>Can I trust Rapid Remit and the payment service providers featured on it?</h6>
<span><i class="fa fa-plus"></i></span>
</div>
</div>
<div data-accordion-component="AccordionItemPanel" class="accordion__panel" role="region" aria-labelledby="accordion__heading-raa-1" id="accordion__panel-raa-1">
<p>Yes of course! Rapid Remit always compares information in real-time and does not provide historic or approximate rates when making comparisons. We also only list providers who are fully authorised and monitored by the regulation authorities in the countries they are registered meaning you can transact with confidence.</p>
</div>
</div>
</div>
</div>
<div data-accordion-component="Accordion" class="accordion">
<div class="question">
<div data-accordion-component="AccordionItem" class="accordion__item">
<div data-accordion-component="AccordionItemHeading" role="heading" class="accordion__heading" aria-level="3">
<div class="accordion__button" id="accordion__heading-raa-2" aria-disabled="false" aria-expanded="false" aria-controls="accordion__panel-raa-2" role="button" tabindex="0" data-accordion-component="AccordionItemButton">
<h6>What is an international money transfer?</h6>
<span><i class="fa fa-plus"></i></span>
</div>
</div>
<div data-accordion-component="AccordionItemPanel" class="accordion__panel" role="region" aria-labelledby="accordion__heading-raa-2" id="accordion__panel-raa-2">
<p>Put simply, international money transfers allow you to send money to a designated recipient whether that be friends, family or businesses overseas. It involves you sending one currency (e.g. AED) to an intermediary transfer service which then exchanges those funds into a foreign currency (e.g. GBP) to then transfer on to your recipient in another country. International payments can be initiated in person, over the phone, through a mobile app or online. The most common method for the recipient to receive the funds is directly into a bank account although certain providers also provide the facility to allow them to collect it in cash or top up a mobile wallet.</p>
</div>
</div>
</div>
</div>
<div data-accordion-component="Accordion" class="accordion">
<div class="question">
<div data-accordion-component="AccordionItem" class="accordion__item">
<div data-accordion-component="AccordionItemHeading" role="heading" class="accordion__heading" aria-level="3">
<div class="accordion__button" id="accordion__heading-raa-3" aria-disabled="false" aria-expanded="false" aria-controls="accordion__panel-raa-3" role="button" tabindex="0" data-accordion-component="AccordionItemButton">
<h6>What are the different payments types listed by providers?</h6>
<span><i class="fa fa-plus"></i></span>
</div>
</div>
<div data-accordion-component="AccordionItemPanel" class="accordion__panel" role="region" aria-labelledby="accordion__heading-raa-3" id="accordion__panel-raa-3">
<p>Payment can be made at any of the local branches of the partner remittance companies.</p>
</div>
</div>
</div>
</div>
<div data-accordion-component="Accordion" class="accordion">
<div class="question"><div data-accordion-component="AccordionItem" class="accordion__item">
<div data-accordion-component="AccordionItemHeading" role="heading" class="accordion__heading" aria-level="3">
<div class="accordion__button" id="accordion__heading-raa-4" aria-disabled="false" aria-expanded="false" aria-controls="accordion__panel-raa-4" role="button" tabindex="0" data-accordion-component="AccordionItemButton">
<h6>Is Rapid Remit a payment service provider?</h6>
<span><i class="fa fa-plus"></i></span>
</div>
</div>
<div data-accordion-component="AccordionItemPanel" class="accordion__panel" role="region" aria-labelledby="accordion__heading-raa-4" id="accordion__panel-raa-4">
<p>Rapid Remit isn’t a payment service provider itself, meaning we can’t physically send a payment on your behalf. Instead we can be used to compare payment service providers to find it the most favourable foreign exchange rates, cheapest transfer fees, shortest transfer speed and even if payments are open to further fee deductions even after it has been sent.</p>
</div>
</div>
</div>
</div>
<div data-accordion-component="Accordion" class="accordion">
<div class="question">
<div data-accordion-component="AccordionItem" class="accordion__item"><div data-accordion-component="AccordionItemHeading" role="heading" class="accordion__heading" aria-level="3">
<div class="accordion__button" id="accordion__heading-raa-5" aria-disabled="false" aria-expanded="false" aria-controls="accordion__panel-raa-5" role="button" tabindex="0" data-accordion-component="AccordionItemButton">
<h6>What is the Total Cost field in results?</h6>
<span><i class="fa fa-plus"></i></span>
</div>
</div>
<div data-accordion-component="AccordionItemPanel" class="accordion__panel" role="region" aria-labelledby="accordion__heading-raa-5" id="accordion__panel-raa-5">
<p>Total cost consists of the amount to be exchanged into foreign currency, plus any additional payment fees levied by a provider. Certain providers deduct their payment fee off the amount to be exchanged before converting into foreign currency, and these will not be added to the total cost field when applicable. The difference are displayed and the most competitive will be marked with our RR logo.</p>
</div>
</div>
</div>
</div>
<div data-accordion-component="Accordion" class="accordion">
<div class="question">
<div data-accordion-component="AccordionItem" class="accordion__item">
<div data-accordion-component="AccordionItemHeading" role="heading" class="accordion__heading" aria-level="3">
<div class="accordion__button" id="accordion__heading-raa-6" aria-disabled="false" aria-expanded="false" aria-controls="accordion__panel-raa-6" role="button" tabindex="0" data-accordion-component="AccordionItemButton">
<h6>What is the Amount Received field in results?</h6>
<span><i class="fa fa-plus"></i></span>
</div>
</div>
<div data-accordion-component="AccordionItemPanel" class="accordion__panel" role="region" aria-labelledby="accordion__heading-raa-6" id="accordion__panel-raa-6">
<p>The amount received is dictated by the rate of exchange, and will be the amount of foreign currency received by the bank of the payment recipient. Certain providers deduct their payment fees off the amount to be exchanged before converting into foreign currency, and where applicable the received amount will already have this fee considered.</p>
</div>
</div>
</div>
</div>
<div data-accordion-component="Accordion" class="accordion">
<div class="question">
<div data-accordion-component="AccordionItem" class="accordion__item">
<div data-accordion-component="AccordionItemHeading" role="heading" class="accordion__heading" aria-level="3">
<div class="accordion__button" id="accordion__heading-raa-7" aria-disabled="false" aria-expanded="false" aria-controls="accordion__panel-raa-7" role="button" tabindex="0" data-accordion-component="AccordionItemButton">
<h6>What is the Speed field in results?</h6>
<span><i class="fa fa-plus"></i></span>
</div>
</div>
<div data-accordion-component="AccordionItemPanel" class="accordion__panel" role="region" aria-labelledby="accordion__heading-raa-7" id="accordion__panel-raa-7">
<p>Speed details how long the payment should take to arrive into the recipient’s bank account following a fully executed payment instruction with a provider. The difference displayed in red is between each provider and the provider/s that are the most competitive (marked with Mooney’s cow bell).</p>
</div>
</div>
</div>
</div>
<div data-accordion-component="Accordion" class="accordion">
<div class="question">
<div data-accordion-component="AccordionItem" class="accordion__item">
<div data-accordion-component="AccordionItemHeading" role="heading" class="accordion__heading" aria-level="3">
<div class="accordion__button" id="accordion__heading-raa-8" aria-disabled="false" aria-expanded="false" aria-controls="accordion__panel-raa-8" role="button" tabindex="0" data-accordion-component="AccordionItemButton">
<h6>What is the documents filed?</h6>
<span><i class="fa fa-plus"></i></span>
</div>
</div>
<div data-accordion-component="AccordionItemPanel" class="accordion__panel" role="region" aria-labelledby="accordion__heading-raa-8" id="accordion__panel-raa-8">
<p>This contains the minimum documentation required each remittance partner requires. In addition if a larger amount is sent there maybe additional documentation required. This is in line with the Money laundering compliance regulations.</p>
</div>
</div>
</div>
</div>
<div data-accordion-component="Accordion" class="accordion">
<div class="question">
<div data-accordion-component="AccordionItem" class="accordion__item">
<div data-accordion-component="AccordionItemHeading" role="heading" class="accordion__heading" aria-level="3">
<div class="accordion__button" id="accordion__heading-raa-9" aria-disabled="false" aria-expanded="false" aria-controls="accordion__panel-raa-9" role="button" tabindex="0" data-accordion-component="AccordionItemButton">
<h6>What is the Payment Fees field in results?</h6>
<span><i class="fa fa-plus"></i></span>
</div>
</div>
<div data-accordion-component="AccordionItemPanel" class="accordion__panel" role="region" aria-labelledby="accordion__heading-raa-9" id="accordion__panel-raa-9">
<p>Payment fees details any additional payment fees levied by a provider outside of an exchange rate cost. Certain providers claim this fee by adding it as an additional charge on top of the original amount to be exchanged into foreign currency, and when applicable this will be included in the total cost amount. Other providers deduct their fees off the amount to be exchanged, and therefore these will be considered in the amount received and will not be added to the total cost field.</p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection
@push('custom-script')
<script>
    $(document).ready(function () {
        //toggle the component with class accordion_body
        $(".accordion__heading").click(function () {
            $(this).next(".accordion__panel").slideToggle(300);
            // if ($('.accordion__panel').is(':visible')) {
            //     // $(".accordion__panel").slideUp(300);
            //     $(this).next(".accordion__panel").slideDown(300);
            // }
            // if ($(this).next(".accordion__panel").is(':visible')) {
            //     // $(this).next(".accordion__panel").slideUp(300);
            //     $(this).next(".accordion__panel").slideDown(300);
            
            // } 
            // else {
            //     $(this).next(".accordion__panel").slideDown(300);
                
            // }
        });
    });

    $('.accordion__button').click(function() {
        $(this).find('i').toggleClass('fa fa-plus fa fa-minus');
    });
</script>
@endpush
@push('custom-css')
<style>
    .accordion__panel {
        display: none;
    }
</style>
@endpush