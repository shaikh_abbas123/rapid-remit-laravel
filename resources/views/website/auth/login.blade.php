@extends('website.layouts.app')
@section('content')
<div class="registration">
	<div class="container01">
		<div class="registrationDivMain">
			<div class="form">
				<form action="{{route('login.store')}}" method="post">
					<div class="row">
						<h1 class="h1Account">ACCOUNT</h1>
            @if (Session::has('message'))
              <p class="text-danger">{{Session::get('message')}}</p>
            @endif 
          </div>
					<div class="form-group row">
						<div class="col-sm-12">
							<div class="fields">
								<label for="firstName" class="col-form-label">Email Address</label>
								<input type="email" class="form-control" value="{{old('email')}}" name="email" placeholder="Email" id="email">
							</div>
						</div>
            @error('email')
              <p class="text-danger text-sm">{{$message}}</p>
            @enderror
						<div class="col-sm-12">
							<div class="fields">
								<label for="password" class=" col-form-label">Password</label>
								<input type="password" class="form-control" name="password" placeholder="Password" id="password">
							</div>
						</div>
            @error('password')
            <p class="text-danger text-sm">{{$message}}</p>
            @enderror
					</div>
					<div class="form-group row">
						<div class="col-sm-12 col-md-12 col-form-label">
							<div class="row">
								<div class="form-check col-sm-12 col-md-7">
                  @csrf
									<div class="row no-gutters align-items-center">
										<input class="" type="checkbox" id="remember" name="remember" style="width: auto;">
										<label class="" for="gridCheck4" style="width: auto; margin-left: 10px; margin-bottom: 0px; font-size: 12px;">Remain logged in to website</label>
									</div>
								</div>
								<div class="col-sm-12 col-md-5">
									<div class="row no-gutters justify-content-end align-items-center"><a href="#"><span style="" class="forget_pass">Forgot Password?</span></a></div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-12 d-flex justify-content-end">
							<button type="submit" class="btn btn-primary">LOGIN</button>
						</div>
					</div>
					<div class="row" style="justify-content: flex-end; margin-top: 50px; padding: 0px 16px;">
						<h6><span style="color: rgb(146, 146, 146); font-weight: 700;">Don't have an account?</span> <span style="" class="create_acc"><a href="/register" class="create_acc">Create Account.</a></span></h6></div>
				</form>
			</div>
		</div>
	</div>
</div>


@endsection
@push('custom-css')
<link href="{{asset('website/css/style-registration.css')}}" rel="stylesheet">
@endpush




