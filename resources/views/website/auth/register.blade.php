@extends('website.layouts.app')
@section('content')

<div class="registration">
	<div class="container01">
		<div class="registrationDivMain">
			<div class="form">
				<form action="{{route('registerUser')}}" method="post">
					<div class="">
						<div class="back-arrow-button" style="margin: 0px;"><a href="/login" class=""><i class="fa fa-arrow-left"></i></a></div>
					</div>
					<div class="row">
						<h1>REGISTRATION</h1>
                    </div>
                    @if (Session::has('message'))
                    <p class="text-danger">{{Session::get('message')}}</p>
                    @endif

                    @csrf
					<div class="form-group row">
						<div class="col-sm-6">
							<div class="fields">
								<label for="firstName" class="col-form-label">First Name</label>
								<input type="text" class="form-control" id="firstName" placeholder="First Name" name="firstname">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="fields">
								<label for="lastName" class="col-form-label">Last Name</label>
								<input type="text" class="form-control" id="lastName" placeholder="Last Name" name="lastname">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<div class="fields">
								<label for="email" class=" col-form-label">Email</label>
								<input type="email" class="form-control" id="email" placeholder="info@gmail.com" value="{{old('email')}}" name="email">
							</div>
                            @error('email')
                            <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
						</div>
						<div class="col-sm-6">
							<div class="fields">
								<label for="phone" class=" col-form-label">Phone No.</label>
								<input type="number" class="form-control" id="phone" placeholder="Phone No." name="phone">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<div class="fields">
								<label for="country" class=" col-form-label">Country</label>
								<input type="text" class="form-control" id="country" placeholder="UAE" name="country">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="fields">
								<label for="city" class=" col-form-label">City</label>
								<input type="text" class="form-control" id="city" placeholder="Dubai" name="city">
							</div>
						</div>
					</div>
					<div class="form-group row" style="">
						<div class="col-sm-6">
							<div class="fields">
								<label for="streetAddress" class=" col-form-label">State</label>
								<input type="text" class="form-control" id="streetAddress" placeholder="State" name="state">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="fields">
								<label for="address" class=" col-form-label">Postal Code</label>
								<input type="text" class="form-control" id="address" placeholder="Postal Code" name="postalcode">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6">
							<div class="fields">
								<label for="password" class=" col-form-label">Password</label>
								<input type="password" class="form-control" id="password" placeholder="XXX XXX XXX" name="password">
							</div>
                            @error('password')
                            <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
						</div>
						<div class="col-sm-6">
							<div class="fields">
								<label for="confirmPassword" class=" col-form-label">Confirm Password</label>
								<input type="password" class="form-control" id="confirmPassword" placeholder="XXX XXX XXX" name="confirm_password">
							</div>
                            @error('confirm_password')
                            <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-6 col-form-label">
							<h4>Select Your Role</h4>
							<div class="row">
								<div class="form-check col-sm-12 col-md-6 col-lg-4">
									<input type="checkbox" id="gridCheck2" value="false" style="width: auto;" name="role[]" value="Individual">
									<label class="form-check-label" for="gridCheck2">Individual </label>
								</div>
								<div class="form-check col-sm-12 col-md-6 col-lg-4 ">
									<input type="checkbox" id="gridCheck1" value="false" style="width: auto;" name="role[]" value="Business">
									<label class="form-check-label" for="gridCheck1">Business </label>
								</div>
							</div>
						</div>
						<div class="col-sm-6 d-flex justify-content-end">
							<button type="submit" class="btn btn-primary">REGISTER</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@push('custom-css')
<link href="{{asset('website/css/style-registration.css')}}" rel="stylesheet">
@endpush          
@endsection