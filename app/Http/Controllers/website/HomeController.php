<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Service;
use App\Models\News;
use App\Models\Article;
use App\Models\Blog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $service = Service::all()->sortByDesc('id');
        $news = News::all()->sortByDesc('id');
        return view('website.welcome',compact('service','news'));
    }

    public function comparison()
    {
        return view('website.comparison');
    }

    public function ournetwork()
    {
        return view('website.ournetwork');
    }

    public function SpecialOffer()
    {
        return view('website.SpecialOffer');
    }

    public function AboutUs()
    {
        return view('website.AboutUs');
    }

    public function knowledgeBase()
    {
        $data = Article::all()->sortByDesc('id');
        $newsData = News::all()->sortByDesc('id');
        $blogData = Blog::all()->sortByDesc('id');
        return view('website.knowledgeBase',compact('data','newsData','blogData'));

        
        // return view('website.knowledgeBase',compact('data'));


    }

    public function faqs()
    {
        return view('website.faqs');
    }

    public function contact()
    {
        return view('website.contact');
    }

    public function privacypolicy()
    {
        return view('website.privacypolicy');
    }

    public function termsandconditions()
    {
        return view('website.termsandconditions');
    }

    public function cookiespolicy()
    {
        return view('website.cookiespolicy');
    }
    public function News()
    {
        return view('website.News');
    }
    public function xpressmoney()
    {
        return view('website.xpressmoney');
    }
    public function forexcalculator()
    {
        return view('website.forexcalculator');
    }
    public function Articles()
    {
        return view('website.Articles');
    }
    public function blogs()
    {
        return view('website.blogs');
    }
}
