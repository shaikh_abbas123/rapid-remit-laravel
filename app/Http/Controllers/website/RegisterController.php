<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Support\Facades\Redirect;

class RegisterController extends Controller
{
    public function viewRegister()
    {
        return view('website.auth.register');
    }

    public function registerUser(Request $request)
    {
        // dd($request);
        $request->validate([
            'firstname'             => 'required|string|max:255',
            'lastname'             => 'required|string|max:255',
            'email'                 => 'required|string|email|max:255|unique:users',
            'password'              => 'min:8|required_with:confirm_password|same:confirm_password',
            'confirm_password'      => 'min:8',
        ]);

        $user               = new User;
        $user->firstname         = $request->firstname;
        $user->lastname         = $request->lastname;
        $user->email        = $request->email; 
        $user->phone        = $request->phone; 
        $user->country        = $request->country; 
        $user->state        = $request->state;
        $user->city        = $request->city; 
        $user->postalcode        = $request->postalcode;
        $user->password     = Hash::make($request->password);
        $user->save();

        return Redirect::to('/login')->with('success','Your account is registered successfully!');

    }
}
