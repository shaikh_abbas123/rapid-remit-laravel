<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FAQ;

class FaqController extends Controller
{
    public function index()
    {
        $faq = FAQ::all()->sortByDesc('id');
        return view('admin.faq.view',compact('faq'));
    }

    public function create()
    {
        return view('admin.faq.create');
    }

    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            'question'  => 'required',
            'answer'    => 'required',
        ]);

        try{
            if(isset($request->id))
            {
                $success_msg = "Data is Successfully Updated.";
                $faq = FAQ::find($request->id);
            }
            else{

                $success_msg = "Data is Successfully Added.";
                $faq = new FAQ;
                
            }
            $faq->question  = $request->question;
            $faq->answer    = $request->answer;
            $faq->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
        }

        // return \redirect()->back()->with('success',$success_msg);
        return redirect()->route('faq.view')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $faq = FAQ::find($id);
        return view('admin.faq.create',compact('faq'));
    }

    public function delete($id)
    {
        try {
            $faq = FAQ::find($id);
            if($faq)
            {
                FAQ::destroy($id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');
    }
}
