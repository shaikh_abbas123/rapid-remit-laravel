<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;

class NEWSController extends Controller
{
    public function index()
    {
        $data = News::all()->sortByDesc('id');
        return view('admin.news.view',compact('data'));
    }

    public function create()
    {
        return view('admin.news.create');
    }

    public function store(Request $request)
    {
        try{
            $request->validate([
                'heading'   => 'required',
                'news'      => 'required',
            ]);

            if(isset($request->id))
            {
                $success_msg = "Data is Successfully Updated.";
                $news = News::find($request->id);
            }
            else{
                $success_msg = "Data is Successfully Added.";
                $news = new News;
            }

            $news->heading     = $request->heading;
            $news->news        = $request->news;
            $news->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
        }

        return \redirect()->back()->with('success',$success_msg);

    }

    public function edit($id)
    {
        $news = News::find($id);
        return view('admin.news.create',compact('news'));
    }

    public function delete(Request $request)
    {
        try {
            $news = News::find($request->id);
            if($news)
            {
                News::destroy($request->id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');

    }
}
