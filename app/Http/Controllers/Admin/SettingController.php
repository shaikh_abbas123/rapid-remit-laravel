<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;


class SettingController extends Controller
{
    public function index()
    {
        
    }

    public function create()
    {
        $data = Setting::all()->first();
        return view('admin.setting.create',compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'email'             => 'required',
            'company_name'      => 'required',
            'office_address'    => 'required',
            'po_box'            => 'required',
            'city'              => 'required',
            'country'           => 'required',
            'latitude'          => 'required',
            'longitude'         => 'required',
            'facebook_link'     => 'required',
            'instagram_link'    => 'required',
            'linkedIn_link'     => 'required',
            'twitter_link'      => 'required',
            'ios_link'          => 'required',
            'android_link'      => 'required',
        ]);

        try {
            if(isset($request->id))
            {
                $success_msg = "Data is Successfully Updated.";
                $setting = Setting::find($request->id);
            }
            else{
                $success_msg = "Data is Successfully Added.";
                $setting = new Setting;
            }

            $setting->email             = $request->email;
            $setting->company_name      = $request->company_name;
            $setting->office_address    = $request->office_address;
            $setting->po_box            = $request->po_box;
            $setting->city              = $request->city;
            $setting->country           = $request->country;
            $setting->latitude          = $request->latitude;
            $setting->longitude         = $request->longitude;
            $setting->facebook_link     = $request->facebook_link;
            $setting->instagram_link    = $request->instagram_link;
            $setting->linkedIn_link     = $request->linkedIn_link;
            $setting->twitter_link      = $request->twitter_link;
            $setting->ios_link          = $request->ios_link;
            $setting->android_link      = $request->android_link;
            $setting->save();
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
            
        }
        return \redirect()->back()->with('success',$success_msg);
    }

    public function delete($id)
    {
        try {
            $setting = Setting::find($id);
            if($setting)
            {
                Setting::destroy($id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');

    }

}
