<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;

class ArticleController extends Controller
{
    public function index()
    {
        $data = Article::all()->sortByDesc('id');
        return view('admin.article.view',compact('data'));
    }

    public function create()
    {
        return view('admin.article.create');
    }

    public function store(Request $request)
    {
        try{
            if(isset($request->id))
            {
                $request->validate([
                    'banner_title'  => 'required',
                    'title'         => 'required',
                    'description'   => 'required',
                ]);
                $success_msg = "Data is Successfully Updated.";
                $article = Article::find($request->id);
            }
            else{
                
                $request->validate([
                    'banner_title'  => 'required',
                    'title'         => 'required',
                    'description'   => 'required',
                    'logo'          => 'required|image',
                ]);
                $success_msg = "Data is Successfully Added.";
                $article = new Article;
                
            }

            if($request->hasFile('logo'))
            {
                $imageName = time().'.'.$request->logo->extension();
                $request->logo->move(public_path('/website/images/article'), $imageName);
                $article->logo          = $imageName;
            }
            $article->banner_title  = $request->banner_title;
            $article->title         = $request->title;
            $article->description   = $request->description;
            $article->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
        }

        return \redirect()->back()->with('success',$success_msg);

    }

    public function edit($id)
    {
        $article = Article::find($id);
        return view('admin.article.create',compact('article'));
    }

    public function delete(Request $request)
    {
        try {
            $article = Article::find($request->id);
            if($article)
            {
                Article::destroy($request->id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');

    }

}
