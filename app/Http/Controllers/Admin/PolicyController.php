<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Policy;

class PolicyController extends Controller
{
    public function create()
    {
        return view('admin.policies.create');
    }

    public function store_privacy_policy(Request $request)
    {
        // dd($request);
        try {
            $request->validate([
                'privacy_policy' => 'required', 
            ]);
    
            $policy             = Policy::firstOrCreate(['page_name'=> 'privacy_policy']);
            $policy->contect    = $request->privacy_policy;
            $policy->save();
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');   
        }
        return \redirect()->back()->with('success','Data is Successfully Deleted!');
    }

    public function store_terms_and_conditions(Request $request)
    {
        try {
            $request->validate([
                'terms_and_conditions' => 'required', 
            ]);
    
            $policy             = Policy::firstOrCreate(['page_name'=> 'terms_and_conditions']);
            $policy->contect    = $request->terms_and_conditions;
            $policy->save();
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
        }
        return \redirect()->back()->with('success','Data is Successfully Deleted!');
    }

    public function store_cookies_policy(Request $request)
    {
        try {
            $request->validate([
                'cookies_policy' => 'required', 
            ]);
    
            $policy             = Policy::firstOrCreate(['page_name'=> 'cookies_policy']);
            $policy->contect    = $request->cookies_policy;
            $policy->save();
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
        }
        return \redirect()->back()->with('success','Data is Successfully Deleted!');
    }
}
