<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;

class ServiceController extends Controller
{
    public function index()
    {
        $service = Service::all()->sortByDesc('id');
        return view('admin.service.view',compact('service'));
    }

    public function create()
    {
        return view('admin.service.create');
    }

    public function store(Request $request)
    {
        try{
            

             
            if(isset($request->id))
            {
                $request->validate([
                    'heading'       => 'required',
                    'description'   => 'required',
                    // 'logo'          => 'required|image',
                ]);
                $success_msg = "Data is Successfully Updated.";
                $service = Service::find($request->id);
            }
            else{
                
                $request->validate([
                    'heading'       => 'required',
                    'description'   => 'required',
                    'logo'          => 'required|image',
                ]);
                $success_msg = "Data is Successfully Added.";
                $service = new Service;
                
            }

            if($request->hasFile('logo'))
            {
                $imageName = time().'.'.$request->logo->extension();
                $request->logo->move(public_path('/website/images/service'), $imageName);
                $service->logo          = $imageName;
            }
            $service->heading       = $request->heading;
            $service->description   = $request->description;
            $service->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
        }

        return \redirect()->back()->with('success',$success_msg);

    }

    public function edit($id)
    {
        $service = Service::find($id);
        return view('admin.service.create',compact('service'));
    }

    public function delete(Request $request)
    {
        try {
            $service = Service::find($request->id);
            if($service)
            {
                Service::destroy($request->id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');

    }

}
