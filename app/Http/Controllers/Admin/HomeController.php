<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Home;

class HomeController extends Controller
{
    public function index()
    {
        $data = Home::all()->sortByDesc('id');
        return view('admin.home.index',compact('data'));
    }

    public function store(Request $request)
    {
        try{
            $request->validate([
                'home_banner_text'   => 'required',
                'why_choose_us_left_text'      => 'required',
                'why_choose_us_right_text'      => 'required',
                'our_network_description' => 'required'
            ]);

            if(isset($request->id))
            {
                $success_msg = "Data is Successfully Updated.";
                $home = Home::find($request->id);
            }
            else{
                $success_msg = "Data is Successfully Added.";
                $home = new Home;
            }

            $home->heading     = $request->heading;
            $home->news        = $request->news;
            $home->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
        }

        return \redirect()->back()->with('success',$success_msg);

    }
}
