<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;

class BlogController extends Controller
{
    public function index()
    {
        $data = Blog::all()->sortByDesc('id');
        return view('admin.blog.view',compact('data'));
    }

    public function create()
    {
        return view('admin.blog.create');
    }

    public function store(Request $request)
    {
        try{
            if(isset($request->id))
            {
                $request->validate([
                    'title'  => 'required',
                    'sub_title'         => 'required',
                    'description'   => 'required',
                ]);
                $success_msg = "Data is Successfully Updated.";
                $blog = Blog::find($request->id);
            }
            else{
                
                $request->validate([
                    'title'  => 'required',
                    'sub_title'         => 'required',
                    'description'   => 'required',
                    'logo'          => 'required|image',
                ]);
                $success_msg = "Data is Successfully Added.";
                $blog = new Blog;
                
            }

            if($request->hasFile('logo'))
            {
                $imageName = time().'.'.$request->logo->extension();
                $request->logo->move(public_path('/website/images/blog'), $imageName);
                $blog->logo          = $imageName;
            }
            $blog->title            = $request->title;
            $blog->sub_title        = $request->sub_title;
            $blog->description      = $request->description;
            $blog->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
        }

        return \redirect()->back()->with('success',$success_msg);

    }

    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('admin.blog.create',compact('blog'));
    }

    public function delete(Request $request)
    {
        try {
            $blog = Blog::find($request->id);
            if($blog)
            {
                Blog::destroy($request->id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');

    }
}
