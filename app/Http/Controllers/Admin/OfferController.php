<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Offer;

class OfferController extends Controller
{
    public function index()
    {
        $data = Offer::all()->sortByDesc('id');
        return view('admin.offer.view',compact('data'));
    }

    public function create()
    {
        return view('admin.offer.create');
    }

    public function store(Request $request)
    {
        try{
            if(isset($request->id))
            {
                $request->validate([
                    'heading'       => 'required',
                    'expiry_date'   => 'required',
                    'description'   => 'required',
                ]);
                $success_msg = "Data is Successfully Updated.";
                $offer = Offer::find($request->id);
            }
            else{
                
                $request->validate([
                    'heading'       => 'required',
                    'expiry_date'   => 'required',
                    'description'   => 'required',
                    'logo'          => 'required|image',
                ]);
                $success_msg = "Data is Successfully Added.";
                $offer = new Offer;
                
            }

            if($request->hasFile('logo'))
            {
                $imageName = time().'.'.$request->logo->extension();
                $request->logo->move(public_path('/website/images/offer'), $imageName);
                $offer->logo          = $imageName;
            }
            $offer->heading             = $request->heading;
            $offer->expiry_date         = $request->expiry_date;
            $offer->description         = $request->description;
            $offer->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
        }

        return \redirect()->back()->with('success',$success_msg);

    }

    public function edit($id)
    {
        $offer = Offer::find($id);
        return view('admin.offer.create',compact('offer'));
    }

    public function delete(Request $request)
    {
        try {
            $offer = Offer::find($request->id);
            if($offer)
            {
                Offer::destroy($request->id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');

    }
}
